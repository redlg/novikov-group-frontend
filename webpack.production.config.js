const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: [
        './src/index.js',
        './src/shared/styles/global.scss'
    ],
    output: {
        path: path.resolve(__dirname, "assets"),
        publicPath: "/assets/",
        filename: 'js/bundle.js',
        chunkFilename: "js/[id].bundle.js"
    },
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.js|jsx$/,
                loader: 'babel-loader',

                query: {
                    presets: [
                        'react',
                        [
                            "es2015", {
                                "modules": false
                            }
                        ],
                        'stage-1'
                    ],
                    plugins: [
                        'transform-decorators-legacy',
                        'transform-class-properties',
                        [
                            "babel-plugin-root-import",
                            [
                                {
                                    "rootPathPrefix": "!",
                                    "rootPathSuffix": "src/shared"
                                }
                            ]
                        ]
                    ]
                }
            }, {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-raw-loader',
                        'postcss-loader',
                        'sass-loader', {
                            loader: 'sass-resources-loader',
                            options: {
                                resources: ["./src/shared/styles/mixin/*.scss"]
                            }
                        }
                    ]
                })
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            comments: false,
            mangle: {
                except: ['$super', '$', 'exports', 'require']
            }
        }),
        new webpack.LoaderOptionsPlugin({
            test: /\.scss$/,
            minimize: true,
            comments: false,
            options: {
                context: __dirname,
                postcss: [require('precss'), require('autoprefixer')]
            }
        }),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
        new ExtractTextPlugin({filename: 'css/styles.css', allChunks: true})
    ],
    resolve: {
        extensions: [".jsx", ".js", ".json", "*"]
    }
};
