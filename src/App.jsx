import React from 'react';
import { Router } from 'react-router-dom';
import { history } from './shared/app';
import Routes from './Routes';

export default class App extends React.Component {
    render() {
        return (
            <Router history={history}>
                <Routes />
            </Router>
        );
    }
}
