import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Async from 'react-code-splitting';

import Layout from './layout';
import Index from './page/index';
import Login from './page/login';
import Error404 from './page/error404';

// import RestaurantsDetail from './page/restaurants-description';
// import RestaurantsChef from './page/restaurants-chef';
// import RestaurantsAddress from './page/restaurants-address';
// import RestaurantsDeliveryMenu from './page/restaurants-delivery-order-menu';
// import RestaurantsLegalEntity from './page/restaurants-legal-entity';
// import RestaurantsTags from './page/restaurants-tags';
// import RestaurantsKitchens from './page/restaurants-kitchens';
// import DiscountCards from './page/discount-cards';
// import DiscountCardsDetail from './page/discount-cards-detail';
//
// import Certificates from './page/certificates';
// import CertificatesDetail from './page/certificates-detail';
// import CertificatesCreate from './page/certificates-create';

// const Index = () => <Async load={import('./page/index')} />;
// const Error404 = () => <Async load={import('./page/error404')} />;
// const Login = () => <Async load={import('./page/login')} />;

const Restaurants = () => <Async load={import('./page/restaurants')} />;
const RestaurantsDetail = () => <Async load={import('./page/restaurants-detail')} />;
const RestaurantsChef = () => <Async load={import('./page/restaurants-chef')} />;
const RestaurantsAddress = () => <Async load={import('./page/restaurants-address')} />;
const RestaurantsDeliveryMenu = () => <Async load={import('./page/restaurants-delivery-menu')} />;
const RestaurantsLegalEntity = () => <Async load={import('./page/restaurants-legal-entity')} />;
const RestaurantsImages = () => <Async load={import('./page/restaurants-images')} />;
const RestaurantsTags = () => <Async load={import('./page/restaurants-tags')} />;
const RestaurantsKitchens = () => <Async load={import('./page/restaurants-kitchens')} />;
const RestaurantsMenu = () => <Async load={import('./page/restaurants-menu')} />;

const DiscountCards = () => <Async load={import('./page/discount-cards')} />;
const DiscountCardsDetail = () => <Async load={import('./page/discount-cards-detail')} />;
const DiscountCardsCreate = () => <Async load={import('./page/discount-cards-create')} />;

const Certificates = () => <Async load={import('./page/certificates')} />;
const CertificatesDetail = () => <Async load={import('./page/certificates-detail')} />;
const CertificatesCreate = () => <Async load={import('./page/certificates-create')} />;

const Guests = () => <Async load={import('./page/guests')} />;
const GuestsDetail = () => <Async load={import('./page/guests-detail')} />;

const Events = () => <Async load={import('./page/events')} />;
const EventsDetail = () => <Async load={import('./page/events-detail')} />;

const Tags = () => <Async load={import('./page/tags')} />;
const TagsDetail = () => <Async load={import('./page/tags-detail')} />;


const Operations = () => <Async load={import('./page/operations')} />;
const OperationsDetail = () => <Async load={import('./page/operations-detail')} />;

const Transactions = () => <Async load={import('./page/transactions')} />;
const TransactionsDetail = () => <Async load={import('./page/transactions-detail')} />;

const Kitchens = () => <Async load={import('./page/kitchens')} />;
const KitchensDetail = () => <Async load={import('./page/kitchens-detail')} />;

const CompilationRestaurants = () => <Async load={import('./page/compilation-restaurants')} />;
const CompilationRestaurantsDetail = () => <Async load={import('./page/compilation-restaurants-detail')} />;

const CompilationDishes = () => <Async load={import('./page/compilation-dishes')} />;
const CompilationDishesDetail = () => <Async load={import('./page/compilation-dishes-detail')} />;

const DeliveryOrder = () => <Async load={import('./page/delivery-order')} />;




export default class Routes extends React.Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route exact path="/" component={Index} />
                    <Route exact path="/login" component={Login} />

                    <Route exact path="/restaurants" component={Restaurants} />
                    <Route exact path="/restaurants/:id" component={RestaurantsDetail} />
                    <Route path="/restaurants/:id/chef" component={RestaurantsChef} />
                    <Route path="/restaurants/:id/legal-entity" component={RestaurantsLegalEntity} />
                    <Route path="/restaurants/:id/address" component={RestaurantsAddress} />
                    <Route path="/restaurants/:id/images" component={RestaurantsImages} />
                    <Route path="/restaurants/:id/tags" component={RestaurantsTags} />
                    <Route path="/restaurants/:id/kitchens" component={RestaurantsKitchens} />
                    <Route exact path="/restaurants/:id/delivery-menu" component={RestaurantsDeliveryMenu} />
                    <Route
                        path="/restaurants/:id/delivery-menu/category/:category_id"
                        component={RestaurantsDeliveryMenu}
                    />
                    <Route path="/restaurants/:id/delivery-menu/dish/:dish_id" component={RestaurantsDeliveryMenu} />

                    <Route exact path="/restaurants/:id/menu" component={RestaurantsMenu} />
                    <Route path="/restaurants/:id/menu/category/:category_id" component={RestaurantsMenu} />
                    <Route path="/restaurants/:id/menu/dish/:dish_id" component={RestaurantsMenu} />

                    <Route exact path="/discount-cards" component={DiscountCards} />
                    <Route exact path="/discount-cards/create" component={DiscountCardsCreate} />
                    <Route exact path="/discount-cards/:id" component={DiscountCardsDetail} />

                    <Route exact path="/certificates" component={Certificates} />
                    <Route exact path="/certificates/create" component={CertificatesCreate} />
                    <Route exact path="/certificates/:id" component={CertificatesDetail} />

                    <Route exact path="/guests" component={Guests} />
                    <Route exact path="/guests/:id" component={GuestsDetail} />


                    <Route exact path="/events" component={Events} />
                    <Route exact path="/events/:id" component={EventsDetail} />

                    <Route exact path="/tags" component={Tags} />
                    <Route exact path="/tags/:id" component={TagsDetail} />


                    <Route exact path="/operations" component={Operations} />
                    <Route exact path="/operations/:id" component={OperationsDetail} />

                    <Route exact path="/transactions" component={Transactions} />
                    <Route exact path="/transactions/:id" component={TransactionsDetail} />

                    <Route exact path="/kitchens" component={Kitchens} />
                    <Route exact path="/kitchens/:id" component={KitchensDetail} />

                    <Route exact path="/compilation-restaurants" component={CompilationRestaurants} />
                    <Route exact path="/compilation-restaurants/:id" component={CompilationRestaurantsDetail} />

                    <Route exact path="/compilation-dishes" component={CompilationDishes} />
                    <Route exact path="/compilation-dishes/:id" component={CompilationDishesDetail} />

                    <Route exact path="/delivery-order" component={DeliveryOrder} />




                    <Route path="/error404" component={Error404} />
                    <Route component={Error404} />
                </Switch>
            </Layout>
        );
    }
}
