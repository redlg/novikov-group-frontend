import React from 'react';
import DevTools from 'mobx-react-devtools';
import AlertContainer from 'react-alert';

import { observer, observable } from '!/app';
import { Alert } from '!/utils';

import { Sidebar } from './Sidebar';
import { Header } from './Header';

@observer
export default class Layout extends React.Component {
    @observable sidebarActive = true;

    handleToggle = () => {
        this.sidebarActive = !this.sidebarActive;
    };

    render = () => {
        let style = {
            overflow: 'auto'
        };

        return (
            <div className="h-100 d-flex flex-column">
                <DevTools position={{ top: 10, right: 120 }} />
                <AlertContainer
                    ref={(alert) => Alert.init(alert)}
                    position="top right"
                    theme="light"
                    transition="fade"
                />

                <div className="d-flex flex-row" style={{ height: '100%' }}>
                    <Sidebar active={this.sidebarActive} onHide={() => (this.sidebarActive = false)} />
                    <div className="h-100 screen w-100" style={style}>
                        <Header onToggle={this.handleToggle} />
                        <div className="col">{this.props.children}</div>
                    </div>
                </div>
            </div>
        );
    };
}
