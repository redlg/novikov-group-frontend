import { React, withRouter, observer } from '!/app';
import { NavLink, Icon } from '!/uikit';

import './style.scss';

@withRouter
@observer
export class Sidebar extends React.Component {
    render = () => {
        let mini = this.props.active ? '' : 'sidebar--mini';
        return (
            <nav className={'nav flex-column sidebar nav-pills h-100 ' + mini}>
                <div className="sidebar__overlay" onClick={(e) => this.props.onHide()} />
                <div
                    className="logo text-white d-flex align-items-center justify-content-center"
                    style={{
                        background: '#4f9e4f',
                        height: '50px'
                    }}>
                    <span>{this.props.active ? 'Novikov Group' : 'N'}</span>
                </div>
                <li className="nav-item">
                    <NavLink
                        to="/restaurants"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="glass" />
                        <span>Рестораны</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/discount-cards"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="credit-card" />
                        <span>Дисконтные карты</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/certificates"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="id-card-o" />
                        <span>Сертификаты</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/operations"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="usd" />
                        <span>Операции</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/transactions"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="university" />
                        <span>Транзакции</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/guests"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="users" />
                        <span>Гости</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/events"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="calendar" />
                        <span>События</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/tags"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                    activeClassName="sidebar__item--active">
                        <Icon name="tags" />
                        <span>Теги</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/kitchens"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="cutlery" />
                        <span>Кухни</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/compilation-restaurants"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="list-alt" />
                        <span>Подборки ресторанов</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/compilation-dishes"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="list-alt" />
                        <span>Подборки блюд доставки</span>
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        to="/delivery-order"
                        className="nav-link text-white sidebar__item"
                        style={{ borderRadius: '0' }}
                        activeClassName="sidebar__item--active">
                        <Icon name="truck" />
                        <span>Доставка</span>
                    </NavLink>
                </li>
            </nav>
        );
    };
}
