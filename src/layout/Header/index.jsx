import React from 'react';
import { observer } from '!/app';
import { NavLink, Button, Select } from '!/uikit';
import { UserService } from '!/services';

@observer
export class Header extends React.Component {
    render = () => {
        return (
            <nav
                className="header navbar navbar-inverse bg-inverse d-flex flex-row p-0 align-items-center"
                style={{ height: '50px' }}>
                <button className="navbar-toggler align-self-center ml-2" type="button" onClick={this.props.onToggle}>
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="col d-flex flex-row justify-content-between pr-0 align-items-center">
                    <div className="d-flex flex-ro align-items-center">
                        {/* <Select items={[{id: 0, name: 'Пушкинский'}]} style={{width: '170px'}} value={0}/> */}
                        <NavLink className="btn btn-link text-white hidden-md-down" to="/">
                            Заказы на доставку <span className="badge badge-success">2</span>
                        </NavLink>
                        <NavLink className="btn btn-link text-white hidden-md-down" to="/">
                            Бронь <span className="badge badge-success">7</span>
                        </NavLink>
                    </div>
                    <Button success outline className="mr-2" onClick={UserService.logout}>
                        Выход
                    </Button>

                    {/* <div className="d-flex flex-row pr-3 align-items-center">
                        <Button success outline className="mr-3">Профиль</Button>
                    </div> */}
                </div>
            </nav>
        );
    };
}
