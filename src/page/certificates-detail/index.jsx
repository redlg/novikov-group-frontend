import { React, withRouter, observable, observer, moment, computed } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template } from '!/uikit';

import { CertificateStore, CertificateTypeStore } from '!/stores';

import { CertificateService, CertificateTypeService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class CertificatesDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Сертификаты',
            url: '/certificates'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable message = '';

    componentDidMount = () => {
        CertificateTypeService.getItems();
        CertificateService.getItem(this.props.match.params.id).then(() => {
            this.breadcrumb[1].name = CertificateStore.item.number;
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        CertificateService.updateItem().then(() => {
            Alert.success('Сертификат успешно сохранен');
        });
    };

    handleChange = (data) => {
        CertificateStore.item[data.name] = data.value;
    };

    handleBlock = () => {
        CertificateService.block(CertificateStore.item.id, this.message);
    };

    handleUnblock = () => {
        CertificateService.unblock(CertificateStore.item.id, this.message);
    };

    handleActivate = () => {
        CertificateService.activate(CertificateStore.item.id, this.message);
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{CertificateStore.item.number}</Title>
                            </h1>
                        </div>
                    </div>
                    <div className="row">
                        {/* <Template className="col-12" visible={CertificateStore.guest}>
                            TODO <h3>Гость</h3>
                        </Template> */}
                        {/* <div className="col-12">
                            <h3>Информация</h3>
                        </div> */}
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Номер"
                                name="number"
                                value={CertificateStore.item.number}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Статус"
                                name="state"
                                value={CertificateStore.item.status_name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Номинал"
                                name="balance"
                                value={
                                    CertificateStore.item.type.sum > 0
                                        ? CertificateStore.item.type.sum
                                        : 'Неограниченный'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Баланс"
                                name="balance"
                                value={CertificateStore.item.balance}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Тип сертификата"
                                name="balance"
                                value={
                                    CertificateStore.item.type.name +
                                    ' ' +
                                    (CertificateStore.item.type.reusable ? '(Многоразовый)' : '(Одноразовый)')
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Эмитент"
                                name="balance"
                                value="Не указан"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Ресторан"
                                name="balance"
                                value="Не указан"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Контрагент"
                                name="balance"
                                value="Не указан"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата выпуска"
                                name="balance"
                                value={
                                    CertificateStore.item.created_at
                                        ? moment(CertificateStore.item.created_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата активации"
                                name="balance"
                                value={
                                    CertificateStore.item.started_at
                                        ? moment(CertificateStore.item.started_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата окончания"
                                name="balance"
                                value={
                                    CertificateStore.item.ended_at
                                        ? moment(CertificateStore.item.ended_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата блокировки"
                                name="balance"
                                value={
                                    CertificateStore.item.blocked_at
                                        ? moment(CertificateStore.item.blocked_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Input
                                label="Комментарий к операции"
                                name="balance"
                                placeholder="Введите комментарий к операции"
                                value={this.message}
                                onChange={(e) => (this.message = e.value)}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Template
                                visible={
                                    CertificateStore.item.status === 'not_activated' ||
                                    CertificateStore.item.status === 'activated_in_office' ||
                                    CertificateStore.item.status === 'activated_in_restaurant'
                                }>
                                <Button danger className="mr-3" onClick={this.handleBlock}>
                                    Заблокировать
                                </Button>
                            </Template>
                            <Template
                                visible={
                                    CertificateStore.item.status === 'blocked_manually' ||
                                    CertificateStore.item.status === 'blocked_expired'
                                }>
                                <Button success className="mr-3" onClick={this.handleUnblock}>
                                    Разблокировать
                                </Button>
                            </Template>
                            <Template visible={CertificateStore.item.status === 'not_activated'}>
                                <Button primary onClick={this.handleActivate}>
                                    Активировать
                                </Button>
                            </Template>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
