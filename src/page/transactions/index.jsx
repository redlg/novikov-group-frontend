import { React, withRouter, observable, observer, moment, computed } from '!/app';

import {
    Autocomplete,
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Datepicker
} from '!/uikit';

import { TransactionStore, RestaurantStore,OperationTypeStore,OperationStore } from '!/stores';

import {TransactionService, RestaurantService,OperationTypeService,OperationService} from '!/services';

@withRouter
@observer
export default class Transactions extends React.Component {

    object_type = {
        Certificate: 'Сертификат',
        DiscountCard: 'Дисконтная карта'
    };

    componentDidMount = () => {
        TransactionService.getItems();
        RestaurantService.getSelectItems();
        OperationTypeService.getItems();
        TransactionService.getFilters();
        OperationService.getFilters();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        TransactionService.getItems();
    };

    handleChange = (object) => {
        TransactionStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        TransactionStore.filter.cur_page = 1;
        TransactionStore.filter.operation_type_id = 0;
        TransactionStore.filter.card_type = 0;
        TransactionStore.filter.card_input_method = 0;
        TransactionStore.filter.restaurant_id = 0;
        TransactionStore.filter.payment_method_id = 0;
        TransactionStore.filter.sum = ['', ''];
        TransactionStore.filter.discount_sum = ['', ''];
        TransactionStore.filter.created_at_from = null;
        TransactionStore.filter.created_at_to = null;
        TransactionStore.filter.sum_from = null;
        TransactionStore.filter.sum_to = null;
        TransactionService.getItems();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Транзакции</Title>
                    </h1>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={TransactionStore.filter.name}
                                placeholder="Введите название"
                                name="name"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Autocomplete
                                template={(item) =>
                                    item.first_name + ' ' + item.last_name + ' (' + item.phone + ')'
                                }
                                label="Гость"
                                placeholder="Начните писать имя или телефон гостя"
                                name="guest_id"
                                api="/guest"
                                queryfield="phone"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={TransactionStore.filter.operation_type_id}
                                items={TransactionStore.filters.transaction_types}
                                placeholder="Выберите тип транзакции"
                                name="operation_type_id"
                                label="Тип транзакции"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={TransactionStore.filter.relation}
                                items={OperationStore.filters.relations}
                                name="relation"
                                label="Тип объекта"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={TransactionStore.filter.card_input_method}
                                items={TransactionStore.filters.card_input_methods}
                                placeholder="не выбрано"
                                name="card_input_method"
                                label="Метод ввода"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={TransactionStore.filter.restaurant_id}
                                items={RestaurantStore.selectItems}
                                name="restaurant_id"
                                label="Ресторан"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={TransactionStore.filter.cashbox_number}
                                placeholder="Номер кассы"
                                name="cashbox_number"
                                label="Номер кассы"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={TransactionStore.filter.check_number}
                                placeholder="Номер чека"
                                name="check_number"
                                label="Номер чека"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={TransactionStore.filter.operation_id}
                                placeholder="Номер операции"
                                name="operation_id"
                                label="Номер операции"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={TransactionStore.filter.payment_method_id}
                                items={TransactionStore.filters.payment_methods}
                                name="payment_method_id"
                                label="Метод оплаты"
                                placeholder="Метод оплаты"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Range
                                value={TransactionStore.filter.sum}
                                label="Сумма транзакции"
                                placeholder={['от', 'до']}
                                name="sum"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Range
                                value={TransactionStore.filter.discount_sum}
                                label="Сумма cкидки"
                                placeholder={['от', 'до']}
                                name="discount_sum"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата транзакций от"
                                    name="created_at_from"
                                    start={TransactionStore.filter.created_at_from}
                                    end={TransactionStore.filter.created_at_to}
                                    value={TransactionStore.filter.created_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="created_at_to"
                                    start={TransactionStore.filter.created_at_from}
                                    end={TransactionStore.filter.created_at_to}
                                    value={TransactionStore.filter.created_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>

                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <TablePagination store={TransactionStore} onChange={TransactionService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={TransactionStore} onChange={TransactionService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="created_at" store={TransactionStore} onChange={TransactionService.getItems}>
                            Дата
                        </TableSort>
                        <TableSort name="operation_type_id" store={TransactionStore} onChange={TransactionService.getItems}>
                            Тип транзакции
                        </TableSort>
                        <TableSort name="card_input_method" store={TransactionStore} onChange={TransactionService.getItems}>
                            Метод ввода
                        </TableSort>
                        <TableSort name="check_number" store={TransactionStore} onChange={TransactionService.getItems}>
                            Номер Чека
                        </TableSort>
                        <TableSort name="cashbox_number" store={TransactionStore} onChange={TransactionService.getItems}>
                            Номер Кассы
                        </TableSort>
                        <TableSort name="restaurant_id" store={TransactionStore} onChange={TransactionService.getItems}>
                            Ресторан
                        </TableSort>
                        <TableSort name="operation_id" store={TransactionStore} onChange={TransactionService.getItems}>
                            Номер операции
                        </TableSort>
                        <TableSort name="payment_method" store={TransactionStore} onChange={TransactionService.getItems}>
                            Метод Оплаты
                        </TableSort>
                        <TableSort name="card_type" store={TransactionStore} onChange={TransactionService.getItems}>
                            Способ оплаты
                        </TableSort>
                        <TableSort name="sum" store={TransactionStore} onChange={TransactionService.getItems}>
                            Сумма транзакций
                        </TableSort>
                        <TableSort name="discount_sum" store={TransactionStore} onChange={TransactionService.getItems}>
                            Скидка
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {TransactionStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/transactions/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>
                                        {item.created_at ? moment(item.created_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>{item.type.name}</TableCell>
                                    <TableCell>{item.card_input_method}</TableCell>
                                    <TableCell>{item.check_number}</TableCell>
                                    <TableCell>{item.cashbox_number}</TableCell>
                                    <TableCell>{(RestaurantStore.itemById[item.restaurant_id]) ? RestaurantStore.itemById[item.restaurant_id].name : '...'}</TableCell>
                                    <TableCell>{item.operation_id}</TableCell>
                                    <TableCell>{item.payment_method.name_ru}</TableCell>
                                    <TableCell>{this.object_type[item.card_type.split('\\').pop()]}</TableCell>
                                    <TableCell>{item.sum}</TableCell>
                                    <TableCell>{item.discount_sum}</TableCell>

                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={TransactionStore} onChange={TransactionService.getItems} />
            </div>
        );
    };
}
