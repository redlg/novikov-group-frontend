import { React, withRouter, observable, observer, moment, computed, history } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template } from '!/uikit';

import { GuestStore } from '!/stores';

import { GuestService } from '!/services';

import { Alert, Mask } from '!/utils';
import {TableRow} from "../../shared/uikit/TableRow";

@withRouter
@observer
export default class GuestsDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Гости',
            url: '/guests'
        },
        {
            name: 'Загрузка...'
        }
    ];

    sexVariants = [
        {
            id: 0,
            name: 'Неизвестно'
        },
        {
            id: 1,
            name: 'Мужской'
        },
        {
            id: 2,
            name: 'Женский'
        },
        {
            id: 9,
            name: 'Другой'
        }
    ];


    id = 'new';

    componentDidMount = () => {
        this.getInformation();
    };

    getInformation = () => {
        GuestStore.item = GuestStore.newItem;

        this.id = this.props.match.params.id;

        if (this.id === 'new') {
            this.breadcrumb[1].name = 'Новый гость';
        } else {
            GuestService.get(this.id).then(() => {
                this.breadcrumb[1].name = GuestStore.item.last_name + ' ' + GuestStore.item.first_name;
            });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.id === 'new') {
            GuestStore.item.id = '';
            GuestService.create().then((response) => {
                Alert.success('Гость успешно создан');
                history.push('/guests/' + response.body.id);
                this.getInformation();
            });
        } else {
            GuestService.update().then(() => {
                Alert.success('Гость успешно сохранен');
            });
        }
    };

    handleChange = (data) => {
        GuestStore.item[data.name] = data.value;
    };


    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{GuestStore.item.last_name + ' ' + GuestStore.item.first_name}</Title>
                            </h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="#"
                                name="id"
                                value={GuestStore.item.id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                required
                                label="Телефон"
                                name="phone"
                                placeholder="введите телефон"
                                mask={Mask.PHONE_MOBILE}
                                value={GuestStore.item.phone}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                required
                                label="Фамилия"
                                name="last_name"
                                placeholder="введите фамилию"
                                value={GuestStore.item.last_name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                required
                                label="Имя"
                                name="first_name"
                                placeholder="введите имя"
                                value={GuestStore.item.first_name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                label="Эл. почта"
                                name="email"
                                placeholder="введите эл. почту"
                                value={GuestStore.item.email}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                label="Кодовое слово"
                                name="code"
                                placeholder="введите кодовое слово"
                                value={GuestStore.item.code}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                label="Пол"
                                name="sex"
                                items={this.sexVariants}
                                value={GuestStore.item.sex}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Datepicker
                                label="Дата рождения"
                                name="birthday_at"
                                value={GuestStore.item.birthday_at}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h2>Дисконтные карты:</h2>
                        </div>
                    </div>
                        {GuestStore.item.discount_cards.map((item) => {
                            return (
                                <div className="row">
                                    <div className="col-12">
                                        <h3>{item.number}</h3>
                                    </div>
                                    <div className="col-6 mb-3">
                                        <Input
                                            readOnly
                                            label="Номер"
                                            name="number"
                                            value={item.number}
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                    <div className="col-6 mb-3">
                                        <Input
                                            readOnly
                                            label="Тип карты"
                                            name="name"
                                            value={item.type.name}
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                    <div className="col-6 mb-3">
                                        <Input
                                            readOnly
                                            label="Статус"
                                            name="status"
                                            value={item.status_name}
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                    <div className="col-6 mb-3">
                                        <Input
                                            readOnly
                                            label="Ресторан"
                                            name="restaurant_id"
                                            value={item.restaurant_id}
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                    <div className="col-6 mb-3">
                                        <Input
                                            readOnly
                                            label="Дата активации"
                                            name="started_at"
                                            value={item.started_at}
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                    <div className="col-6 mb-3">
                                        <Input
                                            readOnly
                                            label="Дата блокировки"
                                            name="blocked_at"
                                            value={item.blocked_at}
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                            );
                        })}
                    <div className="row">
                        <div className="col-12">
                            <h2>Сертификаты:</h2>
                        </div>
                    </div>
                    {GuestStore.item.certificates.map((item) => {
                        return (
                            <div className="row">
                                <div className="col-12">
                                    <h3>{item.number}</h3>
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Номер"
                                        name="number"
                                        value={item.number}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Тип сертификаты"
                                        name="name"
                                        value={item.type.name}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Статус"
                                        name="status"
                                        value={item.status_name}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Баланс"
                                        name="balance"
                                        value={item.balance}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Номинал"
                                        name="balance"
                                        value={item.type.sum < 0 ? 'не ограничен' : item.type.sum}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Ресторан"
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Дата активации"
                                        name="started_at"
                                        value={item.started_at}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        readOnly
                                        label="Дата блокировки"
                                        name="blocked_at"
                                        value={item.blocked_at}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </div>
                        );
                    })}

                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Template visible={this.id === 'new'}>
                                <Button primary submit className="mr-3">
                                    Создать
                                </Button>
                            </Template>
                            <Template visible={this.id !== 'new'}>
                                <Button success submit className="mr-3">
                                    Обновить
                                </Button>
                            </Template>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
