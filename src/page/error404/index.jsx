import { React } from '!/app';

import { Title } from '!/uikit';

export default class Error404 extends React.Component {
    render = () => {
        return (
            <div className="pt-3">
                <h1>
                    <Title>Ошибка 404: Страница не найдена</Title>
                </h1>
            </div>
        );
    };
}
