import { React, withRouter, observable, observer, computed, lodash } from '!/app';

import {
    Image,
    Upload,
    Input,
    Checkbox,
    Textarea,
    Button,
    Breadcrumb,
    Title,
    Tabs,
    TabsButton,
    ListGroup,
    ListGroupItem
} from '!/uikit';

import { RestaurantService, RestaurantMenuService } from '!/services';

import { RestaurantMenuStore } from '!/stores';

import { Alert } from '!/utils';

import { RestaurantMenuDish } from './RestaurantMenuDish';
import { RestaurantMenuCategory } from './RestaurantMenuCategory';

@withRouter
@observer
export default class RestaurantsMenu extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        chef_id: ''
    };

    componentDidMount = () => {
        RestaurantService.getRestaurant(this.props.match.params.id).then((response) => {
            this.breadcrumb[1].name = response.body.name_ru;
            lodash.merge(this.restaurant, response.body);
        });

        RestaurantMenuService.getCategories(this.props.match.params.id).then((response) => {
            RestaurantMenuService.getDishes(this.props.match.params.id);
        });
    };

    handleChange = (data) => {
        this.item[data.name] = data.value;
    };

    handleUpload = (file) => {
        this.item.logo_id = file.id;
        this.item.logo.link = file.link;
    };

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.item.name_ru);
        RestaurantService.saveRestaurant(this.item).then((response) => {
            Alert.success('Ресторан ' + this.item.name_ru + ' успешно сохранен');
        });
    };

    @computed
    get renderCategories() {
        const categories = RestaurantMenuStore.categories.map((category) => {
            return this.renderCategory(category);
        });
        return categories;
    }

    renderCategory = (category) => {
        const childCategories = category.categories.map((item) => this.renderCategory(item));
        const dishes = category.dishes.map((item) => this.renderDish(item));

        return (
            <ListGroupItem
                folder
                expanded={category.expanded}
                onChange={(data) => (category.expanded = data.value)}
                key={'rmc' + category.id}
                edit={'/restaurants/' + this.props.match.params.id + '/menu/category/' + category.id}
                name={'(' + category.dishes_count + ') ' + category.name_ru}
                className="lead list-group-item-info">
                {dishes}
                {childCategories}
            </ListGroupItem>
        );
    };

    renderDish = (dish) => {
        return (
            <ListGroupItem
                doubleClick
                key={'dmd' + dish.id}
                edit={'/restaurants/' + this.props.match.params.id + '/menu/dish/' + dish.id}
                name={dish.name_ru}
                className="h6"
            />
        );
    };

    expandOff = () => {
        RestaurantMenuStore._categories.forEach((item) => (item.expanded = false));
    };

    expandOn = () => {
        RestaurantMenuStore._categories.forEach((item) => (item.expanded = true));
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <div className="d-flex align-items-center justify-content-between mb-3">
                    <div>
                        <Button
                            className="mr-3"
                            to={'/restaurants/' + this.props.match.params.id + '/menu/category/new'}>
                            Добавить категорию
                        </Button>
                        <Button to={'/restaurants/' + this.props.match.params.id + '/menu/dish/new'}>
                            Добавить блюдо
                        </Button>
                    </div>
                    <div style={{ width: '30%' }}>
                        <Input
                            type="text"
                            label="Поиск"
                            name="name_ru"
                            value={RestaurantMenuStore.search}
                            onChange={(data) => (RestaurantMenuStore.search = data.value)}
                        />
                    </div>
                </div>
                <div className="d-flex mb-3">
                    <Button onClick={this.expandOff} className="mr-3">
                        Свернуть все
                    </Button>
                    <Button onClick={this.expandOn}>Развернуть все</Button>
                </div>
                <ListGroup>{this.renderCategories}</ListGroup>
                {this.props.match.params.dish_id && <RestaurantMenuDish />}
                {this.props.match.params.category_id && <RestaurantMenuCategory />}
            </div>
        );
    };
}
