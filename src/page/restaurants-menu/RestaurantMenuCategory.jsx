import { React, withRouter, observable, action, computed, observer, lodash } from '!/app';

import { Modal, ModalBody, ModalHeader, ModalFooter, Icon, Button, Title, Input, Select } from '!/uikit';

import { RestaurantMenuService } from '!/services';
import { RestaurantMenuStore } from '!/stores';

@withRouter
@observer
export class RestaurantMenuCategory extends React.Component {
    @observable
    item = {
        id: '',
        name_ru: 'Загрузка...',
        name_en: '',
        restaurant_id: '',
        menu_category_id: ''
    };

    componentDidMount = () => {
        this.item.restaurant_id = this.props.match.params.id;

        if (this.props.match.params.category_id == 'new') {
            this.item.name_ru = 'Новая категория';
        } else {
            RestaurantMenuService.getCategory(this.props.match.params.category_id).then((response) => {
                lodash.merge(this.item, response.body);
            });
        }
    };

    handleChange = (e) => {
        this.item[e.name] = e.value;
    };

    handleClose = () => {
        this.props.history.push('/restaurants/' + this.props.match.params.id + '/menu');
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.item.id) {
            RestaurantMenuService.saveCategory(this.item).then(() => {
                RestaurantMenuService.getCategories(this.props.match.params.id);
            });
            this.props.history.push('/restaurants/' + this.props.match.params.id + '/menu');
        } else {
            RestaurantMenuService.createCategory(this.item);
        }
    };

    render = () => {
        return (
            <Modal active={true} close overlay onClose={this.handleClose}>
                <form onSubmit={this.handleSubmit}>
                    <ModalHeader>
                        <h5 className="modal-title">
                            {this.item.id && <Title>Редактирование категории</Title>}
                            {!this.item.id && <Title>Добавление категории</Title>}
                        </h5>
                    </ModalHeader>
                    <ModalBody>
                        <Input
                            form
                            required
                            type="text"
                            label="Название категории"
                            name="name_ru"
                            value={this.item.name_ru}
                            onChange={this.handleChange}
                        />
                        <Input
                            form
                            required
                            type="text"
                            label="Название категории(Англ)"
                            name="name_en"
                            value={this.item.name_en}
                            onChange={this.handleChange}
                        />
                        <Select
                            form
                            value={this.item.menu_category_id}
                            items={RestaurantMenuStore.selectCategories}
                            name="menu_category_id"
                            label="Категория"
                            placeholder="Корневая"
                            onChange={this.handleChange}
                        />
                    </ModalBody>
                    <ModalFooter end>
                        <Button success submit>
                            Сохранить
                        </Button>
                    </ModalFooter>
                </form>
            </Modal>
        );
    };
}
