import { React, withRouter, observable, action, computed, observer, lodash } from '!/app';

import {
    Modal,
    ModalBody,
    ModalHeader,
    ModalFooter,
    Upload,
    Icon,
    Button,
    Title,
    Input,
    Select,
    Textarea,
    Image
} from '!/uikit';

import { RestaurantMenuService } from '!/services';
import { RestaurantMenuStore } from '!/stores';

@withRouter
@observer
export class RestaurantMenuDish extends React.Component {
    @observable
    item = {
        id: '',
        name_ru: 'Загрузка...',
        name_en: '',
        price: 350,
        menu_category_id: 0
    };

    @observable tags = Array();

    componentDidMount = () => {
        this.item.restaurant_id = this.props.match.params.id;

        if (this.props.match.params.dish_id == 'new') {
            this.item.name_ru = 'Новое блюдо';
        } else {
            RestaurantMenuService.getDish(this.props.match.params.dish_id).then((response) => {
                lodash.merge(this.item, response.body);
            });
        }
    };

    handleUpload = (file) => {
        this.item.image_id = file.id;
        this.item.image.link = file.link;
    };

    handleChange = (e) => {
        this.item[e.name] = e.value;
    };

    handleClose = () => {
        this.props.history.push('/restaurants/' + this.props.match.params.id + '/menu');
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.item.id) {
            RestaurantMenuService.saveDish(this.item).then(() => {
                RestaurantMenuService.getDishes(this.props.match.params.id);
            });
            this.props.history.push('/restaurants/' + this.props.match.params.id + '/menu');
        } else {
            RestaurantMenuService.createDish(this.item);
        }
    };

    render = () => {
        return (
            <Modal active={true} close overlay onClose={this.handleClose}>
                <form onSubmit={this.handleSubmit}>
                    <ModalHeader>
                        <h5 className="modal-title">
                            {this.item.id && <Title>Редактирование блюда</Title>}
                            {!this.item.id && <Title>Добавление блюда</Title>}
                        </h5>
                    </ModalHeader>
                    <ModalBody>
                        <Input
                            form
                            required
                            type="text"
                            label="Название категории"
                            name="name_ru"
                            value={this.item.name_ru}
                            onChange={this.handleChange}
                        />
                        <Input
                            form
                            required
                            type="text"
                            label="Название категории(Англ)"
                            name="name_en"
                            value={this.item.name_en}
                            onChange={this.handleChange}
                        />
                        <Select
                            form
                            required
                            value={this.item.menu_category_id}
                            items={RestaurantMenuStore.selectCategories}
                            name="menu_category_id"
                            label="Категория"
                            placeholder="не выбрана"
                            onChange={this.handleChange}
                        />
                        <Input
                            form
                            required
                            type="text"
                            label="Цена"
                            name="price"
                            value={this.item.price}
                            onChange={this.handleChange}
                        />
                    </ModalBody>
                    <ModalFooter end>
                        <Button success submit>
                            Сохранить
                        </Button>
                    </ModalFooter>
                </form>
            </Modal>
        );
    };
}
