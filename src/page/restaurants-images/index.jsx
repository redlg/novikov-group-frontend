import { React, withRouter, observable, observer, lodash } from '!/app';

import { Image, Upload, Button, Breadcrumb, Title, Tabs, TabsButton } from '!/uikit';

import { RestaurantService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class RestaurantsImages extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        images: []
    };

    componentDidMount = () => {
        RestaurantService.getRestaurant(this.props.match.params.id).then((response) => {
            this.breadcrumb[1].name = response.body.name_ru;
            lodash.merge(this.restaurant, response.body);
        });
    };

    handleUpload = (file) => {
        this.restaurant.images.push(file);
    };

    handleDelete = (id) => {
        this.restaurant.images = this.restaurant.images.filter((item) => item.id != id);
    };

    handleSave = (e) => {
        e.preventDefault();
        const data = {
            id: this.restaurant.id,
            image_ids: this.restaurant.images.map((item) => item.id)
        };
        RestaurantService.saveRestaurant(data).then((response) => {
            Alert.success('Ресторан ' + this.item.name_ru + ' успешно сохранен');
        });
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <div className="d-flex mb-3">
                    <Upload center onSuccess={this.handleUpload} url="/image">
                        Загрузить фото
                    </Upload>
                    <Button className="ml-3" success onClick={this.handleSave}>
                        Сохранить
                    </Button>
                </div>
                <div className="row">
                    {this.restaurant.images.map((image) => {
                        return (
                            <div className="col-3 mb-3" key={image.id}>
                                <Image size="300x300" className="mb-2" src={image.link} />
                                <Button danger onClick={() => this.handleDelete(image.id)}>
                                    Удалить
                                </Button>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    };
}
