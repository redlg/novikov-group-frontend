import { React, withRouter, observable, observer, moment } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Datepicker
} from '!/uikit';
import { Mask } from '!/utils';

import { GuestStore } from '!/stores';

import { GuestService } from '!/services';

@withRouter
@observer
export default class Guests extends React.Component {
    componentDidMount = () => {
        GuestService.getItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        GuestService.getItems();
    };

    handleChange = (object) => {
        GuestStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        GuestStore.filter.cur_page = 1;
        GuestStore.filter.query = '';
        GuestStore.filter.email = '';
        GuestStore.filter.average_check = ['', ''];
        GuestStore.filter.transactions_count = ['', ''];
        GuestStore.filter.total_check = ['', ''];
        GuestStore.filter.last_operation_from = null;
        GuestStore.filter.last_operation_to = null;
        GuestStore.filter.birthday_at_from = null;
        GuestStore.filter.birthday_at_to = null;
        GuestService.getItems();
    };

    transformCards = (discount_cards) => {
            return discount_cards.map((discount_card) => discount_card.number + '(' + discount_card.type.name + ')').join(' | ');

    };
    transformCertificateCards = (certificates) => {
            return certificates.map((certificate) => certificate.number + '(' + certificate.type.name + ')').join(' | ');

    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Гости</Title>
                    </h1>
                    <Button success to="/guests/new">
                        Добавить{' '}
                        <Icon size="24" className="ml-1">
                            +
                        </Icon>
                    </Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={GuestStore.filter.query}
                                placeholder="ФИО или телефону"
                                name="query"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={GuestStore.filter.email}
                                placeholder="Эл. почта"
                                name="email"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                label="Телефон"
                                name="phone"
                                placeholder="введите телефон"
                                mask={Mask.PHONE_MOBILE}
                                value={GuestStore.item.phone}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Range
                                value={GuestStore.filter.transactions_count}
                                label="Всего операций"
                                placeholder={['от', 'до']}
                                name="transactions_count"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Range
                                value={GuestStore.filter.total_check}
                                label="Общая сумма"
                                placeholder={['от', 'до']}
                                name="total_check"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Range
                                value={GuestStore.filter.average_check}
                                label="Величина среднего чека"
                                placeholder={['от', 'до']}
                                name="average_check"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-4 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата рождение от"
                                    name="birthday_at_from"
                                    start={GuestStore.filter.birthday_at_from}
                                    end={GuestStore.filter.birthday_at_to}
                                    value={GuestStore.filter.birthday_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="birthday_at_to"
                                    start={GuestStore.filter.birthday_at_from}
                                    end={GuestStore.filter.birthday_at_to}
                                    value={GuestStore.filter.birthday_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-4 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Последняя операция от"
                                    name="last_operation_from"
                                    start={GuestStore.filter.last_operation_from}
                                    end={GuestStore.filter.last_operation_to}
                                    value={GuestStore.filter.last_operation_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="last_operation_to"
                                    start={GuestStore.filter.last_operation_from}
                                    end={GuestStore.filter.last_operation_to}
                                    value={GuestStore.filter.last_operation_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <TablePagination store={GuestStore} onChange={GuestService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={GuestStore} onChange={GuestService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="phone" store={GuestStore} onChange={GuestService.getItems}>
                            Телефон
                        </TableSort>
                        <TableSort name="first_name" store={GuestStore} onChange={GuestService.getItems}>
                            Имя
                        </TableSort>
                        <TableSort name="last_name" store={GuestStore} onChange={GuestService.getItems}>
                            Фамилия
                        </TableSort>
                        <TableSort name="gender" store={GuestStore} onChange={GuestService.getItems}>
                            Пол
                        </TableSort>
                        <TableSort name="email" store={GuestStore} onChange={GuestService.getItems}>
                            Эл. почта
                        </TableSort>
                        <TableSort name="birthday_at" store={GuestStore} onChange={GuestService.getItems}>
                            День рождение
                        </TableSort>
                        <TableSort name="last_operation_date" store={GuestStore} onChange={GuestService.getItems}>
                            Последняя операция
                        </TableSort>
                        <TableSort name="all_operation" store={GuestStore} onChange={GuestService.getItems}>
                            Всех операций
                        </TableSort>
                        <TableSort name="total_check_sum" store={GuestStore} onChange={GuestService.getItems}>
                           Сумма заказов
                        </TableSort>
                        <TableSort name="average_check_sum" store={GuestStore} onChange={GuestService.getItems}>
                            Средний чек
                        </TableSort>
                        <TableSort name="discount_cards"  store={GuestStore} onChange={GuestService.getItems}>
                            Дисконтные карты
                        </TableSort>
                        <TableSort name="certificate" store={GuestStore} onChange={GuestService.getItems}>
                           Сертификаты
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {GuestStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/guests/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.phone || 'Не указан'}</TableCell>
                                    <TableCell>{item.first_name}</TableCell>
                                    <TableCell>{item.last_name}</TableCell>
                                    <TableCell>{item.gender}</TableCell>
                                    <TableCell>{item.email || 'Не указан'}</TableCell>
                                    <TableCell>{item.birthday_at || 'Не указан'}</TableCell>
                                    <TableCell>{item.last_operation_date || 'Не указан' }</TableCell>
                                    <TableCell>{item.transactions_count || 0}</TableCell>
                                    <TableCell>{item.total_check_sum || 0}</TableCell>
                                    <TableCell>{item.average_check_sum || 0 }</TableCell>
                                    <TableCell>{this.transformCards(item.discount_cards)}</TableCell>
                                    <TableCell>{this.transformCertificateCards(item.certificates) || 'Не указан' }</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={GuestStore} onChange={GuestService.getItems} />
            </div>
        );
    };
}
