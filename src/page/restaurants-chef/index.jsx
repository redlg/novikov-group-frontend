import { React, withRouter, observable, observer, lodash } from '!/app';

import { Image, Upload, Input, Checkbox, Textarea, Button, Breadcrumb, Title, Tabs, TabsButton } from '!/uikit';

import { RestaurantService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class RestaurantsChef extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        chef_id: ''
    };

    @observable
    chef = {
        id: '',
        first_name_ru: '',
        last_name_ru: '',
        description_ru: '',
        first_name_en: '',
        last_name_en: '',
        description_en: '',
        image_id: '',
        image: {
            link: ''
        },
        restaurant_id: ''
    };

    componentDidMount = () => {
        RestaurantService.getRestaurant(this.props.match.params.id)
            .then((response) => {
                this.breadcrumb[1].name = response.body.name_ru;
                lodash.merge(this.restaurant, response.body);
            })
            .then(() => {
                if (this.restaurant.chef_id) {
                    RestaurantService.getChef(this.restaurant.chef_id).then((response) => {
                        lodash.merge(this.chef, response.body);
                    });
                }
            });
    };

    handleChange = (data) => {
        this.chef[data.name] = data.value;
    };

    handleUpload = (file) => {
        console.log(file);
        this.chef.image_id = file.id;
        this.chef.image.link = file.link;
    };

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.chef.name_ru);
        RestaurantService.saveChef(this.chef).then((response) => {
            Alert.success('Шеф ' + this.chef.first_name_ru + ' ' + this.chef.last_name_ru + ' успешно сохранен');
        });
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <form className="row" onSubmit={this.handleSubmit}>
                    <div className="col-12 col-sm-3 mb-2">
                        <Image size="300x300" className="mb-2" src={this.chef.image.link} />
                        <Upload center url="/image" onSuccess={this.handleUpload}>
                            Загрузить фото
                        </Upload>
                    </div>
                    <div className="col-12 col-sm-9">
                        <div>
                            <h4>Русская версия</h4>
                            <div className="row">
                                <div className="col">
                                    <Input
                                        required
                                        className="mb-2"
                                        label="Имя"
                                        name="first_name_ru"
                                        value={this.chef.first_name_ru}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col">
                                    <Input
                                        required
                                        className="mb-2"
                                        label="Фамилия"
                                        name="last_name_ru"
                                        value={this.chef.last_name_ru}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </div>
                            <Textarea
                                required
                                rows={10}
                                className="mb-2"
                                label="Описание"
                                name="description_ru"
                                value={this.chef.description_ru}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="pt-3">
                            <h4>Английская версия</h4>
                            <div className="row">
                                <div className="col">
                                    <Input
                                        required
                                        className="mb-2"
                                        label="Имя"
                                        name="first_name_en"
                                        value={this.chef.first_name_en}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col">
                                    <Input
                                        required
                                        className="mb-2"
                                        label="Фамилия"
                                        name="last_name_en"
                                        value={this.chef.last_name_en}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </div>
                            <Textarea
                                required
                                className="mb-2"
                                rows={10}
                                label="Описание"
                                name="description_en"
                                value={this.chef.description_en}
                                onChange={this.handleChange}
                            />
                            <div className="row mb-3">
                                <div className="col-12 d-flex justify-content-end">
                                    <Button submit success>
                                        Сохранить
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    };
}
