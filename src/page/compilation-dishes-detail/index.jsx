import { React, withRouter, observable, observer, moment, computed, history } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template, Tags } from '!/uikit';

import { CompilationDishStore, TagStore, RestaurantStore } from '!/stores';

import {CompilationDishService, TagService, RestaurantService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class CompilationDishDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Подборка блюд доставки',
            url: '/compilation-dishes'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        tag_ids: []
    };


    id = 'new';

    componentDidMount = () => {
        this.getInformation();
        TagService.getItems().then(() => {
            this.selected_tag = TagStore.items[0].id;
        });
        RestaurantService.getSelectItems();
    };

    getInformation = () => {
        CompilationDishStore.item = CompilationDishStore.newItem;

        this.id = this.props.match.params.id;

        if (this.id === 'new') {
            this.breadcrumb[1].name = 'Новая подборка';
            CompilationDishStore.item.id = 'новая';
        } else {
            CompilationDishService.get(this.id).then(() => {
                this.breadcrumb[1].name = CompilationDishStore.item.name;
            });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.id === 'new') {
            CompilationDishStore.item.id = '';
            CompilationDishService.create().then((response) => {
                Alert.success('Подборка успешно создана');
                history.push('/compilation-dishes/' + response.body.id);
                this.getInformation();
            });
        } else {
            CompilationDishService.update().then(() => {
                Alert.success('Подборка успешно сохранена');
            });
        }
    };

    handleChange = (data) => {
        CompilationDishStore.item[data.name] = data.value;
        this.restaurant[data.name] = data.value;
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{CompilationDishStore.item.name}</Title>
                            </h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 mb-3">
                            <Input
                                required
                                label="Название"
                                name="name"
                                value={CompilationDishStore.item.name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                required
                                items={RestaurantStore.selectItems}
                                placeholder="Выберите ресторан"
                                label="Название ресторана"
                                name="restaurant_id"
                                value={CompilationDishStore.item.restaurant_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 col-sm-6 col-lg-12 mb-3">
                            <Tags
                                label="Выберите тег для добавления"
                                placeholder="Выберите тег"
                                items={TagStore.selectItems}
                                name="tag_ids"
                                value={CompilationDishStore.item.tag_ids}
                                onChange={this.handleChange}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Template visible={this.id === 'new'}>
                                <Button primary submit className="mr-3">
                                    Создать
                                </Button>
                            </Template>
                            <Template visible={this.id !== 'new'}>
                                <Button success submit className="mr-3">
                                    Обновить
                                </Button>
                            </Template>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
