import { React, withRouter, observable, observer, moment, computed, history } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template, Textarea } from '!/uikit';

import { KitchenStore } from '!/stores';

import { KitchenService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class KitchensDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Кухня',
            url: '/kitchens'
        },
        {
            name: 'Загрузка...'
        }
    ];


    id = 'new';

    componentDidMount = () => {
        this.getInformation();
    };

    getInformation = () => {
        KitchenStore.item = KitchenStore.newItem;

        this.id = this.props.match.params.id;

        if (this.id === 'new') {
            this.breadcrumb[1].name = 'Новая кухня';
        } else {
            KitchenService.get(this.id).then(() => {
                this.breadcrumb[1].name = KitchenStore.item.name_ru;
            });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.id === 'new') {
            KitchenStore.item.id = '';
            KitchenService.create().then((response) => {
                Alert.success('Кухня успешно создана');
                history.push('/kitchens/' + response.body.id);
                this.getInformation();
            });
        } else {
            KitchenService.update().then(() => {
                Alert.success('Кухня успешно  сохранена');
            });
        }
    };

    handleChange = (data) => {
        KitchenStore.item[data.name] = data.value;
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col col-12 col-sm-4 col-lg-4 mb-3">

                </div>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{KitchenStore.item.name_ru }</Title>
                            </h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="#"
                                name="id"
                                value={KitchenStore.item.id}
                                onChange={this.handleChange}
                            />
                        </div>

                        <div className="col-6 mb-3">
                            <Input
                                label="Название"
                                name="name_ru"
                                value={KitchenStore.item.name_ru}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                label="Название"
                                name="name_en"
                                value={KitchenStore.item.name_en}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Template visible={this.id === 'new'}>
                                <Button primary submit className="mr-3">
                                    Создать
                                </Button>
                            </Template>
                            <Template visible={this.id !== 'new'}>
                                <Button success submit className="mr-3">
                                    Обновить
                                </Button>
                            </Template>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
