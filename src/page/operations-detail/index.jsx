import { React, withRouter, observable, observer, moment, computed } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template } from '!/uikit';

import { OperationStore } from '!/stores';

import { OperationService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class OperationsDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Операции',
            url: '/operations'
        },
        {
            name: 'Загрузка...'
        }
    ];

    componentDidMount = () => {
        OperationService.get(this.props.match.params.id).then(() => {
            this.breadcrumb[1].name = OperationStore.item.id;
        });
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{OperationStore.item.id}</Title>
                            </h1>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="#"
                                name="id"
                                value={OperationStore.item.id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Тип операции"
                                name="id"
                                value={OperationStore.item.type.name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Тип карты"
                                name="id"
                                value={OperationStore.item.card_type.split('\\').pop()}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Номер карты"
                                name="id"
                                value={OperationStore.item.card.number}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата"
                                name="id"
                                value={
                                    OperationStore.item.created_at
                                        ? moment(OperationStore.item.created_at).format('LLL')
                                        : 'не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Комментарий"
                                name="id"
                                value={OperationStore.item.comment}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
