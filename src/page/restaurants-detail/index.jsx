import { React, withRouter, observable, observer, lodash } from '!/app';

import { Image, Upload, Input, Checkbox, Textarea, Button, Breadcrumb, Title, Tabs, TabsButton } from '!/uikit';

import { RestaurantService } from '!/services';

import { Alert, Mask } from '!/utils';

@withRouter
@observer
export default class RestaurantsDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    item = {
        id: '',
        logo_id: '',
        logo: {
            link: ''
        },
        name_ru: 'Загрузка...',
        name_en: '',
        description_ru: '',
        description_en: '',
        phone: '',
        delivery_phone: '',
        delivery_mails: [],
        work_time_friday_from: '',
        work_time_friday_to: '',
        work_time_saturday_from: '',
        work_time_saturday_to: '',
        work_time_sunday_from: '',
        work_time_sunday_to: '',
        work_time_weekdays_from: '',
        work_time_weekdays_to: '',
        average_check: '',
        number_of_seats: '',
        delivery: false,
        parking: false,
        last_people: false
    };

    componentDidMount = () => {
        RestaurantService.getRestaurant(this.props.match.params.id).then((response) => {
            this.breadcrumb[1].name = response.body.name_ru;
            const times = [
                'work_time_friday_from',
                'work_time_friday_to',
                'work_time_saturday_from',
                'work_time_saturday_to',
                'work_time_sunday_from',
                'work_time_sunday_to',
                'work_time_weekdays_from',
                'work_time_weekdays_to'
            ];
            times.forEach((item) => {
                if (response.body[item]) {
                    response.body[item] = response.body[item].substring(0, 5);
                }
            });

            lodash.merge(this.item, response.body);
        });
    };

    handleChange = (data) => {
        console.log(data);
        this.item[data.name] = data.value;
    };

    handleUpload = (file) => {
        this.item.logo_id = file.id;
        this.item.logo.link = file.link;
    };

    handleSubmit = (e) => {
        e.preventDefault();

        RestaurantService.saveRestaurant(this.item).then((response) => {
            Alert.success('Ресторан ' + this.item.name_ru + ' успешно сохранен');
        });
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.item.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <form onSubmit={this.handleSubmit} className="row">
                    <div className="col-3">
                        <div className="row mb-3">
                            <div className="col">
                                <Image size="300x300" className="mb-2" src={this.item.logo.link} />
                                <Upload center onSuccess={this.handleUpload} url="/image">
                                    Загрузить лого
                                </Upload>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <Input
                            required
                            className="mb-3"
                            label="Название"
                            name="name_ru"
                            value={this.item.name_ru}
                            onChange={this.handleChange}
                        />
                        <Input
                            className="mb-3"
                            label="Название(Англ)"
                            name="name_en"
                            value={this.item.name_en}
                            onChange={this.handleChange}
                        />
                        <Textarea
                            required
                            className="mb-3"
                            label="Описание"
                            name="description_ru"
                            value={this.item.description_ru}
                            onChange={this.handleChange}
                        />
                        <Textarea
                            className="mb-3"
                            label="Описание(Англ)"
                            name="description_en"
                            value={this.item.description_en}
                            onChange={this.handleChange}
                        />
                        <Input
                            required
                            className="mb-3"
                            label="Телефон"
                            name="phone"
                            mask={this.phone_mask}
                            value={this.item.phone}
                            onChange={this.handleChange}
                        />
                        <Checkbox
                            className="mb-3"
                            label="До последнего клиента"
                            name="last_people"
                            value={this.item.last_people}
                            onChange={this.handleChange}>
                            Есть
                        </Checkbox>
                        <div className="mb-3 d-flex align-item-center">
                            <Input
                                className="mr-2"
                                label="Время работы по будням с"
                                name="work_time_weekdays_from"
                                mask={Mask.TIME}
                                value={this.item.work_time_weekdays_from}
                                onChange={this.handleChange}
                            />
                            <Input
                                label="по"
                                name="work_time_weekdays_to"
                                mask={Mask.TIME}
                                value={this.item.work_time_weekdays_to}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="mb-3 d-flex align-item-center">
                            <Input
                                className="mr-2"
                                label="Время работы в пятницу с"
                                name="work_time_friday_from"
                                mask={Mask.TIME}
                                value={this.item.work_time_friday_from}
                                onChange={this.handleChange}
                            />
                            <Input
                                label="по"
                                name="work_time_friday_to"
                                mask={Mask.TIME}
                                value={this.item.work_time_friday_to}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="mb-3 d-flex align-item-center">
                            <Input
                                className="mr-2"
                                label="Время работы в субботу с"
                                name="work_time_saturday_from"
                                mask={Mask.TIME}
                                value={this.item.work_time_saturday_from}
                                onChange={this.handleChange}
                            />
                            <Input
                                label="по"
                                name="work_time_saturday_to"
                                mask={Mask.TIME}
                                value={this.item.work_time_saturday_to}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="mb-3 d-flex align-item-center">
                            <Input
                                className="mr-2"
                                label="Время работы в воскресенье с"
                                name="work_time_sunday_from"
                                mask={Mask.TIME}
                                value={this.item.work_time_sunday_from}
                                onChange={this.handleChange}
                            />
                            <Input
                                label="по"
                                name="work_time_sunday_to"
                                mask={Mask.TIME}
                                value={this.item.work_time_sunday_to}
                                onChange={this.handleChange}
                            />
                        </div>
                        <Checkbox
                            className="mb-3"
                            label="Доставка"
                            name="delivery"
                            value={this.item.delivery}
                            onChange={this.handleChange}>
                            Есть
                        </Checkbox>
                        {this.item.delivery && (
                            <Input
                                className="mb-3"
                                label="Телефон доставки"
                                name="delivery_phone"
                                mask={Mask.PHONE_MOBILE}
                                value={this.item.delivery_phone}
                                required={this.item.delivery}
                                onChange={this.handleChange}
                            />
                        )}
                        {/* {this.item.delivery && (
                            <Input
                                className="mb-3"
                                type="email"
                                label="Элекстронная почта доставки"
                                name="delivery_mail"
                                value={this.item.delivery_mail}
                                required={this.item.delivery}
                                onChange={this.handleChange}
                            />
                        )} */}
                        <Input
                            required
                            className="mb-3"
                            label="Средний чек"
                            name="average_check"
                            value={this.item.average_check}
                            onChange={this.handleChange}
                        />
                        <Input
                            className="mb-3"
                            label="Количество мест"
                            name="number_of_seats"
                            value={this.item.number_of_seats}
                            onChange={this.handleChange}
                        />
                        <Checkbox
                            className="mb-3"
                            label="Парковка"
                            name="parking"
                            value={this.item.parking}
                            onChange={this.handleChange}>
                            Есть
                        </Checkbox>
                        <div className="row">
                            <div className="col d-flex justify-content-end">
                                <Button submit className="mb-3">
                                    Сохранить
                                </Button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    };
}
