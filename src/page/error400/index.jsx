import { React } from 'react';

import { Title, Button } from '~/uikit';

export default class Error400 extends React.Component {
    render = () => {
        return (
            <div className="pt-3">
                <Title>Ошибка 400: Что-то пошло не так</Title>
                <Button>Вернуться на предыдущую страницу</Button>
            </div>
        );
    };
}
