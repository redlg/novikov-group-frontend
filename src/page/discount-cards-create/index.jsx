import { React, withRouter, observable, observer, moment, computed } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Datepicker,
    Breadcrumb,
    Radio,
    Textarea,
    Template,
    Autocomplete
} from '!/uikit';

import { DiscountCardStore, DiscountCardTypeStore, RestaurantStore } from '!/stores';

import { DiscountCardService, DiscountCardTypeService, RestaurantService } from '!/services';

import { Alert, Api } from '!/utils';

@withRouter
@observer
export default class DiscountCardsCreate extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Дисконтные карты',
            url: '/discount-cards'
        },
        {
            name: 'Выпуск карт'
        }
    ];

    @observable issuers = Array();
    @observable contractors = Array();

    @observable
    counterparty_types = [
        {
            id: 'physical',
            name: 'Физическое лицо'
        },
        {
            id: 'legal_entity',
            name: 'Юридическое лицо'
        },
        {
            id: 'restaurant',
            name: 'Ресторан'
        }
    ];

    @observable
    data = {
        counterparty_type: 'physical',
        legal_entity_type_id: null,
        issuer_id: null,
        card_type_id: null,
        contractor_id: 0,
        restaurant_id: 0,
        balance: '',
        name: '',
        number: '',
        activate_at: null,
        count: 1000,
        number_from: '',
        discount_cards: '',
        first_name: '',
        last_name: '',
        phone: ''
    };

    @observable guest = {};

    @observable balance_readonly = true;

    @observable radio_tab = 'group';

    @observable radio_counterparty = 'exist';

    componentDidMount = () => {
        DiscountCardTypeService.getItems();
        RestaurantService.getItems(
            {
                per_page: 100
            },
            true
        );

        Api.get('/legal-entity/filters').then((response) => {
            this.issuers = response.body.issuers;
            this.contractors = response.body.contractors;
        });
    };

    handleChange = (data) => {
        this.data[data.name] = data.value;
    };

    handleChangeRadio = (data) => {
        this.radio_tab = data.value;
    };

    handleSubmit = (e) => {
        e.preventDefault();
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>Выпуск дисконтрных карт</Title>
                            </h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h3>Контрагент и эмитент</h3>
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                label="Тип контрагенра"
                                name="counterparty_type"
                                items={this.counterparty_types}
                                value={this.data.counterparty_type}
                                onChange={(data) => (this.data.counterparty_type = data.value)}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                label="Эмитент"
                                placeholder="Выберите эмитента"
                                name="issuer_id"
                                items={this.emitents}
                                value={this.data.issuer_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <Template visible={this.data.counterparty_type === 'legal_entity'}>
                            <div className="col-12 mb-3">
                                <Select
                                    label="Юр. лицо"
                                    placeholder="Выберите юр. лицо"
                                    name="contractor_id"
                                    items={this.contractors}
                                    value={this.data.contractor_id}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </Template>
                        <Template visible={this.data.counterparty_type === 'restaurant'}>
                            <div className="col-12 mb-3">
                                <Select
                                    label="Ресторан"
                                    name="restaurant_id"
                                    items={RestaurantStore.selectItems}
                                    value={this.data.restaurant_id}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </Template>
                        <Template visible={this.data.counterparty_type === 'physical'}>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.radio_counterparty}
                                    value="exist"
                                    name="radio_counterparty"
                                    onChange={() => (this.radio_counterparty = 'exist')}>
                                    Выбрать существуюшего гостя
                                </Radio>
                            </div>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.radio_counterparty}
                                    value="new"
                                    name="radio_counterparty"
                                    onChange={() => (this.radio_counterparty = 'new')}>
                                    Добавить нового гостя
                                </Radio>
                            </div>
                            <Template visible={this.radio_counterparty === 'exist'}>
                                <div className="col-12 mb-3">
                                    <Autocomplete
                                        template={(item) =>
                                            item.first_name + ' ' + item.last_name + ' (' + item.phone + ')'
                                        }
                                        label="Гость"
                                        placeholder="Начните писать имя или телефон гостя"
                                        name="guest_id"
                                        api="/guest"
                                        queryfield="phone"
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </Template>
                            <Template visible={this.radio_counterparty === 'new'}>
                                <div className="col-4 mb-3">
                                    <Input
                                        label="Имя"
                                        name="first_name"
                                        value={this.data.first_name}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-4 mb-3">
                                    <Input
                                        label="Фамилия"
                                        name="last_name"
                                        value={this.data.last_name}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-4 mb-3">
                                    <Input
                                        label="Телефон"
                                        name="phone"
                                        value={this.data.phone}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </Template>
                        </Template>
                        <div className="col-12">
                            <h3>Общая информация</h3>
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                required
                                label="Тип карты"
                                name="card_type_id"
                                items={DiscountCardTypeStore.items}
                                value={this.data.card_type_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Datepicker
                                label="Дата активации"
                                name="activate_at"
                                start={moment().format('LLL')}
                                value={this.data.activate_at}
                                onChange={this.handleChange}
                            />
                        </div>
                        <Template visible={this.data.counterparty_type === 'physical'}>
                            <div className="col-6 mb-3">
                                <Input
                                    required
                                    label="Номер карты"
                                    name="number"
                                    placeholder="Введите номер карты"
                                    value={this.data.number}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </Template>
                        <Template
                            visible={
                                this.data.counterparty_type === 'legal_entity' ||
                                this.data.counterparty_type === 'restaurant'
                            }>
                            <div className="col-12">
                                <h3>Формат выпуска</h3>
                            </div>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.radio_tab}
                                    value="group"
                                    name="radio_tab"
                                    onChange={this.handleChangeRadio}>
                                    Ввести группу вертификатов
                                </Radio>
                            </div>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.radio_tab}
                                    value="hand"
                                    name="radio_tab"
                                    onChange={this.handleChangeRadio}>
                                    Ручной ввод
                                </Radio>
                            </div>
                            <Template visible={this.radio_tab === 'group'}>
                                <div className="col-6 mb-3">
                                    <Input
                                        label="Кол-во сертификатов в группе"
                                        name="count"
                                        value={this.data.count}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        label="Начиная с номера"
                                        name="number_from"
                                        value={this.data.number_from}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </Template>
                            <Template visible={this.radio_tab === 'hand'}>
                                <div className="col-12 mb-3">
                                    <Textarea
                                        label="Сертификаты"
                                        name="DiscountCards"
                                        value={this.data.DiscountCards}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </Template>
                        </Template>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Button submit className="mr-3">
                                Выпустить
                            </Button>
                            <Button success>Сохранить шаблон</Button>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
