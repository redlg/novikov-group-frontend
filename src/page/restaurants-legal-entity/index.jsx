import { React, withRouter, observable, observer, lodash } from '!/app';

import { Image, Upload, Select, Input, Checkbox, Textarea, Button, Breadcrumb, Title, Tabs, TabsButton } from '!/uikit';

import { RestaurantService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class RestaurantsLegalEntity extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        legal_entity_id: ''
    };

    @observable
    legal_entity = {
        id: '',
        actual_address: '',
        bank: '',
        bik: '',
        brand: '',
        contact_person: '',
        correspondent_account: '',
        email: '',
        general_manager: '',
        inn: '',
        kpp: '',
        legal_address: '',
        legal_entity_type_id: 0,
        name: '',
        ogrn: '',
        payment_account: '',
        phone: ''
    };

    @observable legal_entity_types = Array();

    componentDidMount = () => {
        RestaurantService.getLegalEntityTypes().then((response) => {
            response.body.legal_entities_types.forEach((item) => {
                this.legal_entity_types.push(item);
            });
        });

        RestaurantService.getRestaurant(this.props.match.params.id)
            .then((response) => {
                this.breadcrumb[1].name = response.body.name_ru;
                lodash.merge(this.restaurant, response.body);
            })
            .then(() => {
                RestaurantService.getLegalEntity(this.restaurant.legal_entity_id).then((response) => {
                    lodash.merge(this.legal_entity, response.body);
                });
            });
    };

    handleChange = (data) => {
        this.legal_entity[data.name] = data.value;
    };

    handleSubmit = (e) => {
        e.preventDefault();
        RestaurantService.saveLegalEntity(this.chef).then((response) => {
            Alert.success('Юр. лицо ' + this.legal_entity.name + ' успешно сохранено');
        });
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <form onSubmit={this.handleSubmit}>
                    <h4>Общая информация</h4>
                    <div className="row">
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Имя"
                                name="name"
                                value={this.legal_entity.name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Select
                                required
                                items={this.legal_entity_types}
                                className="mb-2"
                                label="Тип юр. лица"
                                name="legal_entity_type_id"
                                value={this.legal_entity.legal_entity_type_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Главный менеджер"
                                name="general_manager"
                                value={this.legal_entity.general_manager}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                className="mb-2"
                                label="Бренд"
                                name="brand"
                                value={this.legal_entity.brand}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <h4>Контакты</h4>
                    <div className="row">
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Юридический адрес"
                                name="legal_address"
                                value={this.legal_entity.legal_address}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Фактический адрес"
                                name="actual_address"
                                value={this.legal_entity.actual_address}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Контактное лицо"
                                name="contact_person"
                                value={this.legal_entity.contact_person}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Телефон"
                                name="phone"
                                value={this.legal_entity.phone}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Email"
                                name="email"
                                value={this.legal_entity.email}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <h4>Реквизиты</h4>
                    <div className="row">
                        <div className="col-6">
                            <Input
                                className="mb-2"
                                label="Банк"
                                name="bank"
                                value={this.legal_entity.bank}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="БИК"
                                name="bik"
                                value={this.legal_entity.bik}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Корреспондентский счет"
                                name="correspondent_account"
                                value={this.legal_entity.correspondent_account}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="ИНН"
                                name="inn"
                                value={this.legal_entity.inn}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="КПП"
                                name="kpp"
                                value={this.legal_entity.kpp}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="ОГРН"
                                name="ogrn"
                                value={this.legal_entity.ogrn}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Рассчетный счет"
                                name="payment_account"
                                value={this.legal_entity.payment_account}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12 d-flex justify-content-end">
                            <Button submit success>
                                Сохранить
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        );
    };
}
