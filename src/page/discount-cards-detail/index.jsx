import { React, withRouter, observable, observer, moment, computed } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template } from '!/uikit';

import { DiscountCardStore, DiscountCardTypeStore } from '!/stores';

import { DiscountCardService, DiscountCardTypeService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class DiscountCardsDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Дисконтные карты',
            url: '/discount-cards'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable message = '';

    componentDidMount = () => {
        DiscountCardTypeService.getItems();
        DiscountCardService.getItem(this.props.match.params.id).then(() => {
            this.breadcrumb[1].name = DiscountCardStore.item.number;
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        DiscountCardService.updateItem().then(() => {
            Alert.success('Дисконтная карта успешно сохранена');
        });
    };

    handleChange = (data) => {
        DiscountCardStore.item[data.name] = data.value;
    };

    handleBlock = () => {
        DiscountCardService.block(DiscountCardStore.item.id, this.message);
    };

    handleUnblock = () => {
        DiscountCardService.unblock(DiscountCardStore.item.id, this.message);
    };

    handleActivate = () => {
        DiscountCardService.activate(DiscountCardStore.item.id, this.message);
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{DiscountCardStore.item.number}</Title>
                            </h1>
                        </div>
                    </div>

                    <div className="row">
                        <Template visible={DiscountCardStore.item.guest_id === null}>
                            <div className="col-12">
                                <h3>Гость не привязан</h3>
                            </div>
                        </Template>
                        <Template visible={DiscountCardStore.item.guest_id !== null}>
                            <div className="col-12">
                                <h3>Гость</h3>
                            </div>
                            <div className="col-4 mb-3">
                                <Input
                                    readOnly
                                    label="Имя"
                                    name="number"
                                    value={DiscountCardStore.item.guest ? DiscountCardStore.item.guest.first_name : ''}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-4 mb-3">
                                <Input
                                    readOnly
                                    label="Фамилия"
                                    name="number"
                                    value={DiscountCardStore.item.guest ? DiscountCardStore.item.guest.last_name : ''}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="col-4 mb-3">
                                <Input
                                    readOnly
                                    label="Телефон"
                                    name="number"
                                    value={DiscountCardStore.item.guest ? DiscountCardStore.item.guest.phone : ''}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </Template>
                        <div className="col-12">
                            <h3>Информация</h3>
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Номер"
                                name="number"
                                value={DiscountCardStore.item.number}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Статус"
                                name="status_name"
                                value={DiscountCardStore.item.status_name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Тип карты"
                                name="balance"
                                value={DiscountCardStore.item.type.name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Эмитент"
                                name="balance"
                                value="Не указан"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Ресторан"
                                name="balance"
                                value="Не указан"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Код скидки RKeeper"
                                name="balance"
                                value={DiscountCardStore.item.type.rkeeper_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Контрагент"
                                name="balance"
                                value="Не указан"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата выпуска"
                                name="balance"
                                value={
                                    DiscountCardStore.item.created_at
                                        ? moment(DiscountCardStore.item.created_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата активации"
                                name="balance"
                                value={
                                    DiscountCardStore.item.started_at
                                        ? moment(DiscountCardStore.item.started_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата окончания"
                                name="balance"
                                value={
                                    DiscountCardStore.item.ended_at
                                        ? moment(DiscountCardStore.item.ended_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата блокировки"
                                name="balance"
                                value={
                                    DiscountCardStore.item.blocked_at
                                        ? moment(DiscountCardStore.item.blocked_at).format('LL')
                                        : 'Не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Input
                                label="Комментарий к операции"
                                name="balance"
                                placeholder="Введите комментарий к операции"
                                value={this.message}
                                onChange={(e) => (this.message = e.value)}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Template
                                visible={
                                    DiscountCardStore.item.status === 'not_activated' ||
                                    DiscountCardStore.item.status === 'activated_in_office' ||
                                    DiscountCardStore.item.status === 'activated_in_restaurant'
                                }>
                                <Button danger className="mr-3" onClick={this.handleBlock}>
                                    Заблокировать
                                </Button>
                            </Template>
                            <Template
                                visible={
                                    DiscountCardStore.item.status === 'blocked_manually' ||
                                    DiscountCardStore.item.status === 'blocked_expired'
                                }>
                                <Button success className="mr-3" onClick={this.handleUnblock}>
                                    Разблокировать
                                </Button>
                            </Template>
                            <Template visible={DiscountCardStore.item.status === 'not_activated'}>
                                <Button primary onClick={this.handleActivate}>
                                    Активировать
                                </Button>
                            </Template>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
