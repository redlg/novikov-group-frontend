import { React, withRouter, observable, observer, moment, computed } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Textarea,
    Datepicker
} from '!/uikit';

import { TagStore } from '!/stores';

import { TagService } from '!/services';

@withRouter
@observer
export default class Tags extends React.Component {
    componentDidMount = () => {
        TagService.getItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        TagService.getItems();
    };

    handleChange = (object) => {
        TagStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        TagStore.filter.cur_page = 1 ;
        TagStore.filter.name_ru = '';
        TagService.getItems();
    };

    @computed get items(){
        return TagStore.items.filter(item => {
            return (item.name_ru + item.name_en).toLowerCase().indexOf(TagStore.filter.name_ru.toLowerCase()) > -1
        });
    }

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Тэги</Title>
                    </h1>
                    <Button success to="/tags/new">
                        Добавить{' '}
                        <Icon size="24" className="ml-1">
                            +
                        </Icon>
                    </Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={TagStore.filter.name_ru}
                                placeholder="Введите название"
                                name="name_ru"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                        </div>
                    </div>
                </form>
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={TagStore} onChange={TagService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="name_ru" store={TagStore} onChange={TagService.getItems}>
                            Название
                        </TableSort>
                        <TableSort name="name_en" store={TagStore} onChange={TagService.getItems}>
                            Name
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {this.items.map((item) => {
                             return (
                        <TableRow key={item.id} edit={'/tags/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.name_ru}</TableCell>
                                    <TableCell>{item.name_en}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </div>
        );
    };
}
