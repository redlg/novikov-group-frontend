import { React, withRouter, observable, observer, moment } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Datepicker
} from '!/uikit';

import { DiscountCardStore, DiscountCardTypeStore, RestaurantStore } from '!/stores';

import { DiscountCardService, DiscountCardTypeService, RestaurantService } from '!/services';

@withRouter
@observer
export default class DiscountCards extends React.Component {
    componentDidMount = () => {
        DiscountCardTypeService.getItems();
        DiscountCardService.getItems();
        RestaurantService.getSelectItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        DiscountCardService.getItems();
    };

    handleExport = (e) => {
        DiscountCardService.export();
    };

    handleChange = (object) => {
        DiscountCardStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        DiscountCardStore.filter.query = '';
        DiscountCardStore.filter.cur_page = 1;
        DiscountCardStore.filter.restaurant_id = 0;
        DiscountCardStore.filter.type = 0;
        DiscountCardStore.filter.status = 0;
        DiscountCardStore.filter.issuer_id = 0;
        DiscountCardStore.filter.contractor_id = 0;
        DiscountCardStore.filter.restaurant_id = 0;
        DiscountCardStore.filter.ended_at_from = null;
        DiscountCardStore.filter.ended_at_to = null;
        DiscountCardStore.filter.started_at_from = null;
        DiscountCardStore.filter.started_at_to = null;
        DiscountCardStore.filter.created_at_from = null;
        DiscountCardStore.filter.created_at_to = null;
        DiscountCardStore.filter.blocked_at_from = null;
        DiscountCardStore.filter.blocked_at_to = null;
        DiscountCardService.getItems();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Дискотные карты</Title>
                    </h1>
                    <Button to="/discount-cards/create">Выпуск карт</Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Input
                                value={DiscountCardStore.filter.query}
                                placeholder="Искать по номеру или ФИО гостя"
                                name="query"
                                label="Номер или ФИО"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={DiscountCardStore.filter.status}
                                items={DiscountCardStore.statuses}
                                name="status"
                                label="Статус"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={DiscountCardStore.filter.type}
                                items={DiscountCardTypeStore.items}
                                name="type"
                                label="Тип"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={DiscountCardStore.filter.issuer_id}
                                items={[]}
                                name="issuer_id"
                                label="Эмитент"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={DiscountCardStore.filter.contractor_id}
                                items={[]}
                                name="contractor_id"
                                label="Контрагент"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={DiscountCardStore.filter.restaurant_id}
                                items={RestaurantStore.selectItems}
                                name="restaurant_id"
                                label="Ресторан"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата выпуска от"
                                    name="created_at_from"
                                    start={DiscountCardStore.filter.created_at_from}
                                    end={DiscountCardStore.filter.created_at_to}
                                    value={DiscountCardStore.filter.created_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="created_at_to"
                                    start={DiscountCardStore.filter.created_at_from}
                                    end={DiscountCardStore.filter.created_at_to}
                                    value={DiscountCardStore.filter.created_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата активации от"
                                    name="started_at_from"
                                    start={DiscountCardStore.filter.started_at_from}
                                    end={DiscountCardStore.filter.started_at_to}
                                    value={DiscountCardStore.filter.started_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="started_at_to"
                                    start={DiscountCardStore.filter.started_at_from}
                                    end={DiscountCardStore.filter.started_at_to}
                                    value={DiscountCardStore.filter.started_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата окончания от"
                                    name="ended_at_from"
                                    start={DiscountCardStore.filter.ended_at_from}
                                    end={DiscountCardStore.filter.ended_at_to}
                                    value={DiscountCardStore.filter.ended_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="ended_at_to"
                                    start={DiscountCardStore.filter.ended_at_from}
                                    end={DiscountCardStore.filter.ended_at_to}
                                    value={DiscountCardStore.filter.ended_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата блокировки от"
                                    name="blocked_at_from"
                                    start={DiscountCardStore.filter.blocked_at_from}
                                    end={DiscountCardStore.filter.blocked_at_to}
                                    value={DiscountCardStore.filter.blocked_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="blocked_at_to"
                                    start={DiscountCardStore.filter.blocked_at_from}
                                    end={DiscountCardStore.filter.blocked_at_to}
                                    value={DiscountCardStore.filter.blocked_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12 d-flex justify-content-between">
                            <div>
                                <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                    Сбросить
                                </Button>
                                <Button submit>Применить</Button>
                            </div>
                            <div>
                                <Button success onClick={this.handleExport}>
                                    Экспорт в Excel
                                </Button>
                            </div>
                        </div>
                    </div>
                </form>
                <TablePagination store={DiscountCardStore} onChange={DiscountCardService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={DiscountCardStore} onChange={DiscountCardService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="number" store={DiscountCardStore} onChange={DiscountCardService.getItems}>
                            Номер
                        </TableSort>
                        <TableSort name="state" store={DiscountCardStore} onChange={DiscountCardService.getItems}>
                            Статус
                        </TableSort>
                        <TableSort
                            name="card_type_id"
                            store={DiscountCardStore}
                            onChange={DiscountCardService.getItems}>
                            Тип
                        </TableSort>
                        <TableSort>Гость</TableSort>
                        <TableSort>Эмитент</TableSort>
                        <TableSort>Контрагент</TableSort>
                        <TableSort>Ресторан</TableSort>
                        <TableSort name="created_at" store={DiscountCardStore} onChange={DiscountCardService.getItems}>
                            Дата выпуска
                        </TableSort>
                        <TableSort name="started_at" store={DiscountCardStore} onChange={DiscountCardService.getItems}>
                            Дата активации
                        </TableSort>
                        <TableSort name="ended_at" store={DiscountCardStore} onChange={DiscountCardService.getItems}>
                            Дата окончания
                        </TableSort>
                        <TableSort name="blocked_at" store={DiscountCardStore} onChange={DiscountCardService.getItems}>
                            Дата блокировки
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {DiscountCardStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/discount-cards/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.number}</TableCell>
                                    <TableCell>{DiscountCardStore.statusById[item.status]['name']}</TableCell>
                                    <TableCell>{item.type.name}</TableCell>
                                    <TableCell>
                                        {item.guest ? item.guest.first_name + ' ' + item.guest.last_name : 'не указан'}
                                    </TableCell>
                                    <TableCell>не указан</TableCell>
                                    <TableCell>не указан</TableCell>
                                    <TableCell>не указан</TableCell>
                                    <TableCell>
                                        {item.created_at ? moment(item.created_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>
                                        {item.started_at ? moment(item.started_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>
                                        {item.ended_at ? moment(item.ended_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>
                                        {item.blocked_at ? moment(item.blocked_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={DiscountCardStore} onChange={DiscountCardService.getItems} />
            </div>
        );
    };
}
