import { React, withRouter, observable, observer, moment } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Textarea,
    Datepicker
} from '!/uikit';

import { EventStore, EventTypeStore } from '!/stores';

import { EventService, EventTypeService } from '!/services';

@withRouter
@observer
export default class Events extends React.Component {
    componentDidMount = () => {
        EventTypeService.getItems();
        EventService.getItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        EventService.getItems();
    };

    handleChange = (object) => {
        EventStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        EventStore.filter.cur_page = 1;
        EventStore.filter.query = '';
        EventStore.filter.email = '';
        EventService.getItems();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>События</Title>
                    </h1>
                    <Button success to="/events/new">
                        Добавить{' '}
                        <Icon size="24" className="ml-1">
                            +
                        </Icon>
                    </Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={EventStore.filter.name}
                                placeholder="Введите название"
                                name="name"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={EventStore.item.event_type_id}
                                items={EventTypeStore.items}
                                template={(item) => item.name_ru}
                                placeholder="Не выбран"
                                name="event_type_id"
                                label="Тип события"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <TablePagination store={EventStore} onChange={EventService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={EventStore} onChange={EventService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="name_ru" store={EventStore} onChange={EventService.getItems}>
                            Название
                        </TableSort>
                        <TableSort name="announce_ru" store={EventStore} onChange={EventService.getItems}>
                            Анонс
                        </TableSort>
                        <TableSort name="event_type_id" store={EventStore} onChange={EventService.getItems}>
                            Тип
                        </TableSort>
                        <TableSort name="phone" store={EventStore} onChange={EventService.getItems}>
                            Телефон
                        </TableSort>
                        <TableSort name="started_at" store={EventStore} onChange={EventService.getItems}>
                            Начинается
                        </TableSort>
                        <TableSort name="ended_at" store={EventStore} onChange={EventService.getItems}>
                            Заканчивается
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {EventStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/events/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.name_ru}</TableCell>
                                    <TableCell>{item.announce_ru}</TableCell>
                                    <TableCell>{item.type.name_ru}</TableCell>
                                    <TableCell>{item.phone || 'Не указан'}</TableCell>
                                    <TableCell>
                                        {item.started_at ? moment(item.started_at).format('LLL') : 'не указано'}
                                    </TableCell>
                                    <TableCell>
                                        {item.ended_at ? moment(item.ended_at).format('LLL') : 'не указано'}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={EventStore} onChange={EventService.getItems} />
            </div>
        );
    };
}
