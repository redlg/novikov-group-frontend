import { React, withRouter, observable, action, computed, observer, lodash } from '!/app';

import {
    Modal,
    ModalBody,
    ModalHeader,
    ModalFooter,
    Upload,
    Icon,
    Button,
    Title,
    Tags,
    Input,
    Select,
    Textarea,
    Image
} from '!/uikit';

import { DeliveryMenuService, TagService } from '!/services';

import { DeliveryMenuStore, TagStore } from '!/stores';

@withRouter
@observer
export class DeliveryMenuDish extends React.Component {
    @observable
    item = {
        id: '',
        name_ru: 'Загрузка...',
        name_en: '',
        composition_ru: '',
        composition_en: '',
        description_ru: '',
        description_en: '',
        image_id: '',
        image: {
            link: ''
        },
        price: 350,
        volume: '',
        weight: '',
        delivery_category_id: '',
        main_tag: '',
        tag_ids: [],
        tags: []
    };

    componentDidMount = () => {
        TagService.getItems();

        this.item.restaurant_id = this.props.match.params.id;

        if (this.props.match.params.dish_id == 'new') {
            this.item.name_ru = 'Новое блюдо';
        } else {
            DeliveryMenuService.getDish(this.props.match.params.dish_id).then((response) => {
                lodash.merge(this.item, response.body);
                this.item.tag_ids = this.item.tags.map((item) => item.id);
            });
        }
    };

    handleUpload = (file) => {
        this.item.image_id = file.id;
        this.item.image.link = file.link;
    };

    handleChange = (e) => {
        this.item[e.name] = e.value;
    };

    handleClose = () => {
        this.props.history.push('/restaurants/' + this.props.match.params.id + '/delivery-order-menu');
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.item.id) {
            DeliveryMenuService.saveDish(this.item).then(() => {
                DeliveryMenuService.getDishes();
            });
            this.props.history.push('/restaurants/' + this.props.match.params.id + '/delivery-order-menu');
        } else {
            DeliveryMenuService.createDish(this.item);
        }
    };

    render = () => {
        return (
            <Modal active={true} close overlay onClose={this.handleClose}>
                <form onSubmit={this.handleSubmit}>
                    <ModalHeader>
                        <h5 className="modal-title">
                            {this.item.id && <Title>Редактирование блюда</Title>}
                            {!this.item.id && <Title>Добавление блюда</Title>}
                        </h5>
                    </ModalHeader>
                    <ModalBody>
                        <Input
                            form
                            required
                            type="text"
                            label="Название категории"
                            name="name_ru"
                            value={this.item.name_ru}
                            onChange={this.handleChange}
                        />
                        <Input
                            form
                            required
                            type="text"
                            label="Название категории(Англ)"
                            name="name_en"
                            value={this.item.name_en}
                            onChange={this.handleChange}
                        />
                        <Select
                            form
                            required
                            value={this.item.delivery_category_id}
                            items={DeliveryMenuStore.selectCategories}
                            name="delivery_category_id"
                            label="Категория"
                            placeholder="не выбрана"
                            onChange={this.handleChange}
                        />
                        <Select
                            form
                            required
                            value={this.item.delivery_category_id}
                            items={TagStore.items}
                            name="main_tag_id"
                            label="Главный тег блюда"
                            placeholder="не выбрана"
                            onChange={this.handleChange}
                        />
                        <Tags
                            label="Список тегов"
                            placeholder="Выберите тег"
                            items={TagStore.selectItems}
                            name="tag_ids"
                            value={this.item.tag_ids}
                            onChange={this.handleChange}
                        />
                        <Image size="300x300" className="mb-2" src={this.item.image.link} />
                        <Upload center onSuccess={this.handleUpload} url="/image">
                            Загрузить фото
                        </Upload>
                        <Textarea
                            form
                            required
                            label="Описание"
                            name="description_ru"
                            value={this.item.description_ru}
                            onChange={this.handleChange}
                        />
                        <Textarea
                            form
                            required
                            label="Описание(англ)"
                            name="description_en"
                            value={this.item.description_en}
                            onChange={this.handleChange}
                        />
                        <Input
                            form
                            required
                            type="text"
                            label="Цена"
                            name="price"
                            value={this.item.price}
                            onChange={this.handleChange}
                        />
                        <Input
                            form
                            required
                            type="text"
                            label="Объем"
                            name="volume"
                            value={this.item.volume}
                            onChange={this.handleChange}
                        />
                    </ModalBody>
                    <ModalFooter end>
                        <Button success submit>
                            Сохранить
                        </Button>
                    </ModalFooter>
                </form>
            </Modal>
        );
    };
}
