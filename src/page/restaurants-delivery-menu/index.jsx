import { React, withRouter, observable, observer, lodash } from '!/app';

import {
    Image,
    Upload,
    Input,
    Checkbox,
    Textarea,
    Button,
    Breadcrumb,
    Title,
    Tabs,
    TabsButton,
    ListGroup,
    ListGroupItem
} from '!/uikit';

import { RestaurantService, DeliveryMenuService } from '!/services';

import { DeliveryMenuStore } from '!/stores';

import { Alert } from '!/utils';

import { DeliveryMenuDish } from './DeliveryMenuDish';
import { DeliveryMenuCategory } from './DeliveryMenuCategory';

@withRouter
@observer
export default class RestaurantsDeliveryMenu extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        chef_id: ''
    };

    componentDidMount = () => {
        RestaurantService.getRestaurant(this.props.match.params.id).then((response) => {
            this.breadcrumb[1].name = response.body.name_ru;
            lodash.merge(this.restaurant, response.body);
        });

        DeliveryMenuService.getCategories(this.props.match.params.id).then((response) => {
            DeliveryMenuService.getDishes(this.props.match.params.id);
        });
    };

    expandOff = () => {
        DeliveryMenuStore.categories.forEach((item) => (item.expanded = false));
    };

    expandOn = () => {
        DeliveryMenuStore.categories.forEach((item) => (item.expanded = true));
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <div className="d-flex align-items-center justify-content-between mb-3">
                    <div>
                        <Button
                            className="mr-3"
                            to={'/restaurants/' + this.props.match.params.id + '/delivery-menu/category/new'}>
                            Добавить категорию
                        </Button>
                        <Button to={'/restaurants/' + this.props.match.params.id + '/delivery-menu/dish/new'}>
                            Добавить блюдо
                        </Button>
                    </div>
                    <div style={{ width: '30%' }}>
                        <Input
                            form
                            type="text"
                            label="Поиск"
                            name="name_ru"
                            value={DeliveryMenuStore.search}
                            onChange={(data) => (DeliveryMenuStore.search = data.value)}
                        />
                    </div>
                </div>
                <div className="d-flex mb-3">
                    <Button onClick={this.expandOff} className="mr-3">
                        Свернуть все
                    </Button>
                    <Button onClick={this.expandOn}>Развернуть все</Button>
                </div>
                <ListGroup>
                    {DeliveryMenuStore.categories.map((category) => {
                        let dishes = category.dishes.map((dish) => {
                            return (
                                <ListGroupItem
                                    key={'dmd' + dish.id}
                                    doubleClick
                                    edit={
                                        '/restaurants/' + this.props.match.params.id + '/delivery-menu/dish/' + dish.id
                                    }
                                    name={dish.name_ru}
                                    className="h6"
                                />
                            );
                        });
                        return (
                            <ListGroupItem
                                key={'dmc' + category.id}
                                edit={
                                    '/restaurants/' +
                                    this.props.match.params.id +
                                    '/delivery-menu/category/' +
                                    category.id
                                }
                                folder
                                expanded={category.expanded}
                                onChange={(data) => (category.expanded = data.value)}
                                name={'(' + category.dishes_count + ')' + category.name_ru}
                                className="lead list-group-item-info">
                                {dishes}
                            </ListGroupItem>
                        );
                    })}
                </ListGroup>
                {this.props.match.params.dish_id && <DeliveryMenuDish />}
                {this.props.match.params.category_id && <DeliveryMenuCategory />}
            </div>
        );
    };
}
