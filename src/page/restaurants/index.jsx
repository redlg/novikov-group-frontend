import { React, withRouter, observable, observer } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination
} from '!/uikit';

import { RestaurantStore } from '!/stores';

import { RestaurantService } from '!/services';

@withRouter
@observer
export default class Restaurants extends React.Component {
    componentDidMount = () => {
        RestaurantService.getFilters();
        RestaurantService.getItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        RestaurantService.getItems();
    };

    handleChange = (object) => {
        RestaurantStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        RestaurantStore.filter.name = '';
        RestaurantStore.filter.seats = ['', ''];
        RestaurantStore.filter.city = 0;
        RestaurantStore.filter.cur_page = 1;
        RestaurantService.getItems();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Рестораны</Title>
                    </h1>
                    {/* <Button success>
                        Добавить{' '}
                        <Icon size="24" className="ml-1">
                            +
                        </Icon>
                    </Button> */}
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={RestaurantStore.filter.name}
                                placeholder="Искать по названию"
                                name="name"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={RestaurantStore.filter.city}
                                items={RestaurantStore.filters.cities}
                                name="city"
                                label="Город"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Range
                                value={RestaurantStore.filter.seats}
                                label="Количество мест"
                                placeholder={['от', 'до']}
                                name="seats"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <TablePagination store={RestaurantStore} onChange={RestaurantService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={RestaurantStore} onChange={RestaurantService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="name_ru" store={RestaurantStore} onChange={RestaurantService.getItems}>
                            Наименование
                        </TableSort>
                        <TableSort
                            name="legal_entity.name_ru"
                            store={RestaurantStore}
                            onChange={RestaurantService.getItems}>
                            Юр. лицо
                        </TableSort>
                        <TableSort name="address.city" store={RestaurantStore} onChange={RestaurantService.getItems}>
                            Город
                        </TableSort>
                        <TableSort name="address.text_ru" store={RestaurantStore} onChange={RestaurantService.getItems}>
                            Адрес
                        </TableSort>
                        <TableSort name="announce_ru" store={RestaurantStore} onChange={RestaurantService.getItems}>
                            Краткое описание
                        </TableSort>
                        <TableSort>Кухня</TableSort>
                        <TableSort name="delivery_phone" store={RestaurantStore} onChange={RestaurantService.getItems}>
                            Телефон доставки
                        </TableSort>
                        <TableSort name="number_of_seats" store={RestaurantStore} onChange={RestaurantService.getItems}>
                            Количество мест
                        </TableSort>
                        {/* <TableSort>Опции</TableSort> */}
                    </TableHead>
                    <TableBody>
                        {RestaurantStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/restaurants/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.name_ru}</TableCell>
                                    <TableCell>{item.legal_entity.name}</TableCell>
                                    <TableCell>{item.address.city}</TableCell>
                                    <TableCell>{item.address.text_ru || 'Не указан'}</TableCell>
                                    <TableCell>{item.announce_ru || 'Не указан'}</TableCell>
                                    <TableCell>{item.kitchens.map((item) => item.name_ru).join(', ')}</TableCell>
                                    <TableCell>{item.delivery_phone || 'Не указан'}</TableCell>
                                    <TableCell>{item.number_of_seats}</TableCell>
                                    {/* <TableCell /> */}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={RestaurantStore} onChange={RestaurantService.getItems} />
            </div>
        );
    };
}
