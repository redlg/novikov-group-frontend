import { React, withRouter, observable, observer, moment, computed } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Textarea,
    Datepicker,
    Tags
} from '!/uikit';

import { CompilationDishStore, RestaurantStore, TagStore } from '!/stores';

import { CompilationDishService, RestaurantService, TagService } from '!/services';

@withRouter
@observer
export default class CompilationDishes extends React.Component {
    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        tag_ids: []
    };

    componentDidMount = () => {
        RestaurantService.getSelectItems();
        CompilationDishService.getItems();
        TagService.getItems().then(() => {
            this.selected_tag = TagStore.items[0].id;
        });
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        CompilationDishService.getItems();
    };

    handleChange = (object) => {
        CompilationDishStore.filter[object.name] = object.value;
        this.restaurant[object.name] = object.value;
    };

    handleReset = (object) => {
        CompilationDishStore.filter.cur_page = 1 ;
        CompilationDishStore.filter.restaurant_id = 0 ;
        CompilationDishStore.filter.tag_id = 0 ;
        CompilationDishStore.filter.name = '';
        CompilationDishService.getItems();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Подборки блюд доставки</Title>
                    </h1>
                    <Button success to="/compilation-dishes/new">
                        Добавить{' '}
                        <Icon size="24" className="ml-1">
                            +
                        </Icon>
                    </Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={CompilationDishStore.filter.name}
                                placeholder="Введите название"
                                name="name"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={CompilationDishStore.filter.restaurant_id}
                                items={RestaurantStore.selectItems}
                                name="restaurant_id"
                                label="Ресторан"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                label="Выберите тег"
                                placeholder="Выберите тег"
                                items={TagStore.selectItems}
                                value={CompilationDishStore.filter.tag_id}
                                name="tag_id"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <TablePagination store={CompilationDishStore} onChange={CompilationDishService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={CompilationDishStore} onChange={CompilationDishService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="name" store={CompilationDishStore} onChange={CompilationDishService.getItems}>
                            Название
                        </TableSort>
                        <TableSort name="restaurant.name_ru" store={CompilationDishStore} onChange={CompilationDishService.getItems}>
                            Название Ресторана
                        </TableSort>
                        <TableSort store={CompilationDishStore} onChange={CompilationDishService.getItems}>
                            Тег
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {CompilationDishStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/compilation-dishes/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.name}</TableCell>
                                    <TableCell>{(RestaurantStore.itemById[item.restaurant_id]) ? RestaurantStore.itemById[item.restaurant_id].name : '...'}</TableCell>
                                    <TableCell>{item.tags.map((item) => item.name_ru).join(', ')}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={CompilationDishStore} onChange={CompilationDishService.getItems} />
            </div>
        );
    };
}
