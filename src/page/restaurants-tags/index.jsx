import { React, withRouter, observable, observer, lodash } from '!/app';

import { Select, Button, Tags, Breadcrumb, Title, Tabs, TabsButton } from '!/uikit';

import { RestaurantService, TagService } from '!/services';

import { TagStore } from '!/stores';

import { Alert } from '!/utils';

@withRouter
@observer
export default class RestaurantsTags extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        tag_ids: []
    };

    @observable selected_tag = 0;

    componentDidMount = () => {
        TagService.getItems().then(() => {
            this.selected_tag = TagStore.items[0].id;
        });

        RestaurantService.getRestaurant(this.props.match.params.id).then((response) => {
            this.breadcrumb[1].name = response.body.name_ru;
            lodash.merge(this.restaurant, response.body);
            this.restaurant.tag_ids = this.restaurant.tags.map((item) => item.id);
        });
    };

    handleChangeMainTag = (data) => {
        this.restaurant[data.name] = data.value;
    };

    handleChange = (data) => {
        this.restaurant[data.name] = data.value;
    };

    handleSave = (e) => {
        e.preventDefault();
        const data = {
            id: this.restaurant.id,
            main_tag_id: this.restaurant.main_tag_id,
            tag_ids: this.restaurant.tag_ids
        };
        RestaurantService.saveRestaurant(data).then((response) => {
            Alert.success('Ресторан ' + this.restaurant.name_ru + ' успешно сохранен');
        });
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <Button success onClick={this.handleSave} className="mb-3">
                    Сохранить
                </Button>
                <h4>Главный тег</h4>
                <div className="row">
                    <div className="col-6">
                        <Select
                            items={TagStore.selectItems}
                            className="mb-2"
                            label="Выберите главный тег"
                            name="main_tag_id"
                            value={this.restaurant.main_tag_id}
                            onChange={this.handleChangeMainTag}
                        />
                    </div>
                </div>
                <h4>Все теги</h4>
                <Tags
                    label="Выберите тег для добавления"
                    placeholder="Выберите тег"
                    items={TagStore.selectItems}
                    name="tag_ids"
                    value={this.restaurant.tag_ids}
                    onChange={this.handleChange}
                />
            </div>
        );
    };
}
