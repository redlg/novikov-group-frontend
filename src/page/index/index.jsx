import { React, observable, observer } from '!/app';

import { Redirect } from '!/uikit';

@observer
export default class Index extends React.Component {
    componentDidMount = () => {};

    render() {
        return (
            <div>
                <Redirect to="/restaurants" />
                {/* <Role.Administrator>
                    <Redirect to="/restaurants"/>
                </Role.Administrator> */}
            </div>
        );
    }
}
