import { React, withRouter, observable, observer, moment, computed } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Textarea,
    Datepicker,
    Autocomplete
} from '!/uikit';

import { DeliveryOrderStore,RestaurantStore } from '!/stores';

import { DeliveryOrderService,RestaurantService } from '!/services';

@withRouter
@observer
export default class DeliveryOrder extends React.Component {
    componentDidMount = () => {
            DeliveryOrderService.getItems();
            RestaurantService.getSelectItems();
            DeliveryOrderService.getFilters();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        DeliveryOrderService.getItems();
    };

    handleChange = (object) => {
        DeliveryOrderStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        DeliveryOrderStore.filter.cur_page = 1 ;
        DeliveryOrderStore.filter.name_ru = '';
        DeliveryOrderStore.filter.sum = ['', ''];
        DeliveryOrderStore.filter.created_at_from = null;
        DeliveryOrderStore.filter.created_at_to = null;
        DeliveryOrderStore.filter.canceled_at_from = null;
        DeliveryOrderStore.filter.canceled_at_to = null;
        DeliveryOrderStore.filter.completed_at_from = null;
        DeliveryOrderStore.filter.completed_at_to = null;
        DeliveryOrderStore.filter.restaurant_id = 0;
        DeliveryOrderStore.filter.delivery_payment_method = 0;
        DeliveryOrderStore.filter.delivery_method_id = 0;
        DeliveryOrderStore.filter.status = 0;

        DeliveryOrderService.getItems();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Доставка</Title>
                    </h1>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Autocomplete
                                template={(item) =>
                                    item.first_name + ' ' + item.last_name + ' (' + item.phone + ')'
                                }
                                label="Гость"
                                placeholder="Начните писать имя или телефон гостя"
                                name="guest_id"
                                api="/guest"
                                queryfield="phone"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={DeliveryOrderStore.filter.restaurant_id}
                                items={RestaurantStore.selectItems}
                                name="restaurant_id"
                                label="Ресторан"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-4 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Заказ создан от"
                                    name="created_at_from"
                                    start={DeliveryOrderStore.filter.created_at_from}
                                    end={DeliveryOrderStore.filter.created_at_to}
                                    value={DeliveryOrderStore.filter.created_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="created_at_to"
                                    start={DeliveryOrderStore.filter.created_at_from}
                                    end={DeliveryOrderStore.filter.created_at_to}
                                    value={DeliveryOrderStore.filter.created_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-4 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Заказ отменен от"
                                    name="canceled_at_from"
                                    start={DeliveryOrderStore.filter.canceled_at_from}
                                    end={DeliveryOrderStore.filter.canceled_at_to}
                                    value={DeliveryOrderStore.filter.canceled_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="canceled_at_to"
                                    start={DeliveryOrderStore.filter.canceled_at_from}
                                    end={DeliveryOrderStore.filter.canceled_at_to}
                                    value={DeliveryOrderStore.filter.canceled_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-4 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Заказ выполнен от"
                                    name="completed_at_from"
                                    start={DeliveryOrderStore.filter.completed_at_from}
                                    end={DeliveryOrderStore.filter.completed_at_to}
                                    value={DeliveryOrderStore.filter.completed_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="completed_at_to"
                                    start={DeliveryOrderStore.filter.completed_at_from}
                                    end={DeliveryOrderStore.filter.completed_at_to}
                                    value={DeliveryOrderStore.filter.completed_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Range
                                value={DeliveryOrderStore.filter.sum}
                                label="Сумма транзакции"
                                placeholder={['от', 'до']}
                                name="sum"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={DeliveryOrderStore.filter.delivery_payment_method}
                                items={DeliveryOrderStore.filters.delivery_payment_methods}
                                name="delivery_payment_method"
                                label="Спососб оплаты"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={DeliveryOrderStore.filter.delivery_method_id}
                                items={DeliveryOrderStore.filters.delivery_methods}
                                name="delivery_method_id"
                                label="Спососб доставки"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                value={DeliveryOrderStore.filter.status}
                                items={DeliveryOrderStore.filters.statuses}
                                name="status"
                                label="Статус"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            #
                        </TableSort>
                        <TableSort name="guest" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Гость
                        </TableSort>
                        <TableSort name="dishes" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Блюда
                        </TableSort>
                        <TableSort name="restaurant_id" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Ресторан
                        </TableSort>
                        <TableSort name="address" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Адрес доставки
                        </TableSort>
                        <TableSort name="number_of_persons" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Количество человек
                        </TableSort>
                        <TableSort name="created_at" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                           Заказ создан
                        </TableSort>
                        <TableSort name="canceled_at" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                           Заказ отменен
                        </TableSort>
                        <TableSort name="completed_at" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                           Заказ выполнен
                        </TableSort>
                        <TableSort name="delivered_at" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Заказ доставлен
                        </TableSort>
                        <TableSort name="delivery_method" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Способ доставки
                        </TableSort>
                        <TableSort name="delivery_payment_method" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Способ оплаты
                        </TableSort>
                        <TableSort name="status_name" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Статус
                        </TableSort>
                        <TableSort name="sum" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Сумма
                        </TableSort>
                        <TableSort name="comment" store={DeliveryOrderStore} onChange={DeliveryOrderStore.getItems}>
                            Коментарий
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {DeliveryOrderStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/delivery-order/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.guest.first_name || 'Не указана'}</TableCell>
                                    <TableCell>{item.dishes.map((item) => item.name_ru).join(', ')}</TableCell>
                                    <TableCell>{(RestaurantStore.itemById[item.restaurant_id]) ? RestaurantStore.itemById[item.restaurant_id].name : '...'}</TableCell>
                                    <TableCell>{item.address.street + ',' + item.address.city }</TableCell>
                                    <TableCell>{item.number_of_persons}</TableCell>
                                    <TableCell>{item.created_at ? moment(item.created_at).format('LLL') : 'не указана'}</TableCell>
                                    <TableCell>{item.canceled_at ? moment(item.created_at).format('LLL') : 'не указана'}</TableCell>
                                    <TableCell>{item.completed_at ? moment(item.created_at).format('LLL') : 'не указана'}</TableCell>
                                    <TableCell>{item.delivered_at ? moment(item.created_at).format('LLL') : 'не указана'}</TableCell>
                                    <TableCell>{item.delivery_method.name}</TableCell>
                                    <TableCell>{item.delivery_payment_method.name}</TableCell>
                                    <TableCell>{item.status_name}</TableCell>
                                    <TableCell>{item.sum}</TableCell>
                                    <TableCell>{item.comment || 'Нет комментария'}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </div>
        );
    };
}
