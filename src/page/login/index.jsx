import { React, observable, observer, withRouter } from '!/app';

import { Modal, ModalBody, ModalFooter, ModalHeader, Title, Input, Button } from '!/uikit';

import { UserService } from '!/services';

@withRouter
@observer
export default class Login extends React.Component {
    @observable
    form = {
        email: 'info@novikov.ru',
        password: 'Administrator'
    };

    handleSubmit = (e) => {
        e.preventDefault();
        UserService.login(this.form);
    };

    handleChange = (input) => {
        this.form[input.type] = input.value;
    };

    render = () => {
        return (
            <Modal active={true} close={false} overlay={false}>
                <form onSubmit={this.handleSubmit}>
                    <ModalHeader>
                        <Title className="h5 modal-title">Авторизация</Title>
                    </ModalHeader>
                    <ModalBody>
                        <Input
                            form
                            required
                            type="email"
                            placeholder="Введите вашу почту"
                            label="Электронная почта"
                            value={this.form.email}
                            onChange={this.handleChange}
                        />
                        <Input
                            form
                            required
                            type="password"
                            placeholder="Введите ваш пароль"
                            label="Пароль"
                            value={this.form.password}
                            onChange={this.handleChange}
                        />
                    </ModalBody>
                    <ModalFooter center>
                        <Button submit>Войти</Button>
                    </ModalFooter>
                </form>
            </Modal>
        );
    };
}
