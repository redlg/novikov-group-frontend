import { React, withRouter, observable, observer, moment } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Datepicker
} from '!/uikit';

import { CertificateStore, CertificateTypeStore, RestaurantStore } from '!/stores';

import { CertificateService, CertificateTypeService, RestaurantService } from '!/services';

@withRouter
@observer
export default class Certificates extends React.Component {
    componentDidMount = () => {
        CertificateService.getItems();
        CertificateTypeService.getItems();
        RestaurantService.getSelectItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        CertificateService.getItems();
    };

    handleChange = (object) => {
        CertificateStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        CertificateStore.filter.cur_page = 1;
        CertificateStore.filter.query = '';
        CertificateStore.filter.status = 0;
        CertificateStore.filter.type = 0;
        CertificateStore.filter.issuer_id = 0;
        CertificateStore.filter.contractor_id = 0;
        CertificateStore.filter.restaurant_id = 0;
        CertificateStore.filter.ended_at_from = null;
        CertificateStore.filter.ended_at_to = null;
        CertificateStore.filter.started_at_from = null;
        CertificateStore.filter.started_at_to = null;
        CertificateStore.filter.created_at_from = null;
        CertificateStore.filter.created_at_to = null;
        CertificateStore.filter.blocked_at_from = null;
        CertificateStore.filter.blocked_at_to = null;
        CertificateService.getItems();
    };

    handleExport = (e) => {
        CertificateService.export();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Сертификаты</Title>
                    </h1>
                    <Button to="/certificates/create">Выпуск сертификатов</Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Input
                                value={CertificateStore.filter.query}
                                placeholder="Искать по номеру карты или ФИО"
                                name="query"
                                label="Номер или ФИО"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={CertificateStore.filter.status}
                                items={CertificateStore.statuses}
                                name="status"
                                label="Статус"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={CertificateStore.filter.type}
                                items={CertificateTypeStore.items}
                                name="type"
                                label="Тип"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={CertificateStore.filter.issuer_id}
                                items={[]}
                                name="issuer_id"
                                label="Эмитент"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={CertificateStore.filter.contractor_id}
                                items={[]}
                                name="contractor_id"
                                label="Контрагент"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={CertificateStore.filter.restaurant_id}
                                items={RestaurantStore.selectItems}
                                name="restaurant_id"
                                label="Ресторан"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата выпуска от"
                                    name="created_at_from"
                                    start={CertificateStore.filter.created_at_from}
                                    end={CertificateStore.filter.created_at_to}
                                    value={CertificateStore.filter.created_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="created_at_to"
                                    start={CertificateStore.filter.created_at_from}
                                    end={CertificateStore.filter.created_at_to}
                                    value={CertificateStore.filter.created_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата активации от"
                                    name="started_at_from"
                                    start={CertificateStore.filter.started_at_from}
                                    end={CertificateStore.filter.started_at_to}
                                    value={CertificateStore.filter.started_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="started_at_to"
                                    start={CertificateStore.filter.started_at_from}
                                    end={CertificateStore.filter.started_at_to}
                                    value={CertificateStore.filter.started_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата окончания от"
                                    name="ended_at_from"
                                    start={CertificateStore.filter.ended_at_from}
                                    end={CertificateStore.filter.ended_at_to}
                                    value={CertificateStore.filter.ended_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="ended_at_to"
                                    start={CertificateStore.filter.ended_at_from}
                                    end={CertificateStore.filter.ended_at_to}
                                    value={CertificateStore.filter.ended_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-3 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата блокировки от"
                                    name="blocked_at_from"
                                    start={CertificateStore.filter.blocked_at_from}
                                    end={CertificateStore.filter.blocked_at_to}
                                    value={CertificateStore.filter.blocked_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="blocked_at_to"
                                    start={CertificateStore.filter.blocked_at_from}
                                    end={CertificateStore.filter.blocked_at_to}
                                    value={CertificateStore.filter.blocked_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12 d-flex justify-content-between">
                            <div>
                                <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                    Сбросить
                                </Button>
                                <Button submit>Применить</Button>
                            </div>
                            <div>
                                <Button success onClick={this.handleExport}>
                                    Экспорт в Excel
                                </Button>
                            </div>
                        </div>
                    </div>
                </form>
                <TablePagination store={CertificateStore} onChange={CertificateService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={CertificateStore} onChange={CertificateService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="number" store={CertificateStore} onChange={CertificateService.getItems}>
                            Номер
                        </TableSort>
                        <TableSort name="status" store={CertificateStore} onChange={CertificateService.getItems}>
                            Статус
                        </TableSort>
                        <TableSort
                            name="card_type_id"
                            store={CertificateStore}
                            onChange={CertificateService.getItems}>
                            Тип
                        </TableSort>
                        <TableSort>Номинал</TableSort>
                        <TableSort name="balance" store={CertificateStore} onChange={CertificateService.getItems}>
                            Баланс
                        </TableSort>
                        <TableSort name="issuer_id" store={CertificateStore} onChange={CertificateService.getItems}>
                            Эмитент
                        </TableSort>
                        <TableSort name="contractor_id" store={CertificateStore} onChange={CertificateService.getItems}>
                            Контрагент
                        </TableSort>
                        <TableSort name="restaurant_id" store={CertificateStore} onChange={CertificateService.getItems}>
                            Ресторан
                        </TableSort>
                        <TableSort>Гость</TableSort>
                        <TableSort name="created_at" store={CertificateStore} onChange={CertificateService.getItems}>
                            Дата выпуска
                        </TableSort>
                        <TableSort name="started_at" store={CertificateStore} onChange={CertificateService.getItems}>
                            Дата активации
                        </TableSort>
                        <TableSort name="ended_at" store={CertificateStore} onChange={CertificateService.getItems}>
                            Дата окончания
                        </TableSort>
                        <TableSort name="blocked_at" store={CertificateStore} onChange={CertificateService.getItems}>
                            Дата блокировки
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {CertificateStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/certificates/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.number}</TableCell>
                                    <TableCell>{CertificateStore.statusById[item.status]['name']}</TableCell>
                                    <TableCell>{item.type.name}</TableCell>
                                    <TableCell>{item.type.sum < 0 ? 'не ограничен' : item.type.sum}</TableCell>
                                    <TableCell>{item.balance}</TableCell>
                                    <TableCell>не указан</TableCell>
                                    <TableCell>не указан</TableCell>
                                    <TableCell>не указан</TableCell>
                                    <TableCell>
                                        {item.guest ? item.guest.first_name + ' ' + item.guest.last_name : 'не указан'}
                                    </TableCell>
                                    <TableCell>
                                        {item.created_at ? moment(item.created_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>
                                        {item.started_at ? moment(item.started_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>
                                        {item.ended_at ? moment(item.ended_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>
                                        {item.blocked_at ? moment(item.blocked_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={CertificateStore} onChange={CertificateService.getItems} />
            </div>
        );
    };
}
