import { React, withRouter, observable, observer, moment, computed, action } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Datepicker,
    Breadcrumb,
    Radio,
    Textarea,
    Template,
    Autocomplete
} from '!/uikit';

import { CertificateStore, CertificateTypeStore, RestaurantStore } from '!/stores';

import { CertificateService, CertificateTypeService, RestaurantService } from '!/services';

import { Alert, Api } from '!/utils';

@withRouter
@observer
export default class CertificatesCreate extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Сертификаты',
            url: '/certificates'
        },
        {
            name: 'Выпуск сертификатов'
        }
    ];

    @observable issuers = Array();
    @observable contractors = Array();

    @observable
    counterparty_types = [
        {
            id: 'physical',
            name: 'Физическое лицо'
        },
        {
            id: 'legal_entity',
            name: 'Юридическое лицо'
        },
        {
            id: 'restaurant',
            name: 'Ресторан'
        }
    ];

    @observable
    data = {
        counterparty_type: 'physical',
        legal_entity_type_id: null,
        issuer_id: null,
        card_type_id: null,
        contractor_id: 0,
        restaurant_id: 0,
        last_name: '',
        first_name: '',
        phone: '',
        balance: '',
        name: '',
        number: '',
        started_at: null,
        count: 1000,
        number_from: '',
        guest_id: null,
        release_format: 'group',
        guest_type: 'exist',
        certificates: ['', '', '']
    };

    @observable balance_readonly = true;

    @observable radio_tab = 'group';

    componentDidMount = () => {
        CertificateTypeService.getItems().then(() => {
            this.data.card_type_id = CertificateTypeStore.items[0].id;
            this.updateBalance(CertificateTypeStore.items[0]);
        });

        RestaurantService.getSelectItems();

        Api.get('/legal-entity/filters').then((response) => {
            this.issuers = response.body.issuers;
            this.contractors = response.body.contractors;
        });
    };

    handleChange = (data) => {
        this.data[data.name] = data.value;

        if (data.name === 'guest_type') {
            this.data.guest_id = null;
        }
    };

    handleChangeType = (data) => {
        const item = CertificateTypeStore.itemById[data.value];
        this.updateBalance(item);
        this.data[data.name] = data.value;
    };

    updateBalance = (item) => {
        this.balance_readonly = !item.reusable;
        if (item.reusable) {
            this.data['balance'] = 0;
        } else {
            this.data['balance'] = item.sum;
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let data = {};
        if (this.data.counterparty_type == 'physical') {
            if (this.data.guest_type == 'exist') {
                data = {
                    card_type_id: this.data.card_type_id,
                    issuer_id: this.data.issuer_id,
                    guest_id: this.data.guest_id,
                    number: this.data.number,
                    started_at: this.data.started_at
                };
            } else {
                data = {
                    card_type_id: this.data.card_type_id,
                    issuer_id: this.data.issuer_id,
                    phone: this.data.phone,
                    last_name: this.data.last_name,
                    first_name: this.data.first_name,
                    number: this.data.number,
                    started_at: this.data.started_at
                };
            }
        }
        if (this.data.counterparty_type == 'legal_entity') {
            if (this.data.guest_id) {
                data = {
                    card_type_id: this.data.card_type_id,
                    issuer_id: this.data.issuer_id,
                    guest_id: this.data.guest_id,
                    number: this.data.number,
                    started_at: this.data.started_at
                };
            } else {
                data = {
                    card_type_id: this.data.card_type_id,
                    issuer_id: this.data.issuer_id,
                    phone: this.data.phone,
                    last_name: this.data.last_name,
                    first_name: this.data.first_name,
                    number: this.data.number,
                    started_at: this.data.started_at
                };
            }
        }
        if (this.data.counterparty_type == 'restaurant') {
        }
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>Выпуск сертификатов</Title>
                            </h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <h3>Контрагент и эмитент</h3>
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                label="Тип контрагенра"
                                name="counterparty_type"
                                items={this.counterparty_types}
                                value={this.data.counterparty_type}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                label="Эмитент"
                                placeholder="Выберите эмитента"
                                name="issuer_id"
                                items={this.emitents}
                                value={this.data.issuer_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <Template visible={this.data.counterparty_type === 'legal_entity'}>
                            <div className="col-12 mb-3">
                                <Select
                                    label="Юр. лицо"
                                    placeholder="Выберите юр. лицо"
                                    name="contractor_id"
                                    items={this.contractors}
                                    value={this.data.contractor_id}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </Template>
                        <Template visible={this.data.counterparty_type === 'restaurant'}>
                            <div className="col-12 mb-3">
                                <Select
                                    label="Ресторан"
                                    placeholder="Выберите ресторан"
                                    name="restaurant_id"
                                    items={RestaurantStore.selectItems}
                                    value={this.data.restaurant_id}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </Template>
                        <Template visible={this.data.counterparty_type === 'physical'}>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.data.guest_type}
                                    value="exist"
                                    name="guest_type"
                                    onChange={this.handleChangeRadio}>
                                    Выбрать существуюшего гостя
                                </Radio>
                            </div>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.data.guest_type}
                                    value="new"
                                    name="guest_type"
                                    onChange={this.handleChangeRadio}>
                                    Добавить нового гостя
                                </Radio>
                            </div>
                            <Template visible={this.data.guest_type === 'exist'}>
                                <div className="col-12 mb-3">
                                    <Autocomplete
                                        required
                                        template={(item) =>
                                            item.first_name + ' ' + item.last_name + ' (' + item.phone + ')'
                                        }
                                        label="Гость"
                                        placeholder="Начните писать имя или телефон гостя"
                                        name="guest_id"
                                        api="/guest"
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </Template>
                            <Template visible={this.data.guest_type === 'new'}>
                                <div className="col-4 mb-3">
                                    <Input
                                        label="Имя"
                                        name="first_name"
                                        value={this.data.first_name}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-4 mb-3">
                                    <Input
                                        label="Фамилия"
                                        name="last_name"
                                        value={this.data.last_name}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-4 mb-3">
                                    <Input
                                        label="Телефон"
                                        name="phone"
                                        value={this.data.phone}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </Template>
                        </Template>
                        <div className="col-12">
                            <h3>Общая информация</h3>
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                required
                                label="Тип сертификата"
                                name="card_type_id"
                                items={CertificateTypeStore.selectItems}
                                value={this.data.card_type_id}
                                onChange={this.handleChangeType}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                required
                                readOnly={this.balance_readonly}
                                label="Номинал(Баланс)"
                                name="balance"
                                value={this.data.balance}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Datepicker
                                label="Дата активации"
                                name="started_at"
                                start={moment().format('YYYY-MM-DD hh:mm:ss')}
                                value={this.data.started_at}
                                onChange={this.handleChange}
                            />
                        </div>
                        <Template visible={this.data.counterparty_type === 'physical'}>
                            <div className="col-6 mb-3">
                                <Input
                                    required
                                    label="Номер карты"
                                    name="number"
                                    mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                                    placeholder="Введите номер карты"
                                    value={this.data.number}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </Template>
                        <Template
                            visible={
                                this.data.counterparty_type === 'legal_entity' ||
                                this.data.counterparty_type === 'restaurant'
                            }>
                            <div className="col-12">
                                <h3>Формат выпуска</h3>
                            </div>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.data.release_format}
                                    value="group"
                                    name="release_format"
                                    onChange={this.handleChange}>
                                    Ввести группу вертификатов
                                </Radio>
                            </div>
                            <div className="col-6 mb-3">
                                <Radio
                                    checked={this.data.release_format}
                                    value="hand"
                                    name="release_format"
                                    onChange={this.handleChange}>
                                    Ручной ввод
                                </Radio>
                            </div>
                            <Template visible={this.data.release_format === 'group'}>
                                <div className="col-6 mb-3">
                                    <Input
                                        label="Кол-во сертификатов в группе"
                                        name="count"
                                        value={this.data.count}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <Input
                                        label="Начиная с номера"
                                        name="number_from"
                                        mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                                        value={this.data.number_from}
                                        onChange={this.handleChange}
                                    />
                                </div>
                            </Template>
                            <Template visible={this.data.release_format === 'hand'}>
                                <div className="col-12 mb-2">
                                    <label>Список сертификатов</label>
                                    {this.data.certificates.map((item, index) => {
                                        return (
                                            <div className="mb-1 row" key={index}>
                                                <div className="col-10">
                                                    <Input
                                                        required
                                                        key={index}
                                                        mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                                                        name={'certificates' + index}
                                                        value={this.data.certificates[index]}
                                                        onChange={(data) =>
                                                            (this.data.certificates[index] = data.value)
                                                        }
                                                    />
                                                </div>
                                                <div className="col-2">
                                                    <Button
                                                        className="w-100"
                                                        danger
                                                        onClick={() => this.data.certificates.splice(index, 1)}>
                                                        Удалить
                                                    </Button>
                                                </div>
                                            </div>
                                        );
                                    })}
                                </div>
                                <div className="col-2 offset-10">
                                    <Button
                                        className="w-100"
                                        outline
                                        success
                                        onClick={() => this.data.certificates.push('')}>
                                        Добавить
                                    </Button>
                                </div>
                            </Template>
                        </Template>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Button submit className="mr-3">
                                Выпустить
                            </Button>
                            {/* <Button success>Сохранить шаблон</Button> */}
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
