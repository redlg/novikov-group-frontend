import { React, withRouter, observable, observer, moment, computed } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template } from '!/uikit';

import { TransactionStore, RestaurantStore, PaymentMethodStore } from '!/stores';

import { TransactionService, RestaurantService, PaymentMethodService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class TransactionsDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Транзакции',
            url: '/transactions'
        },
        {
            name: 'Загрузка...'
        }
    ];

    componentDidMount = () => {
        TransactionService.get(this.props.match.params.id).then(() => {
            this.breadcrumb[1].name = TransactionStore.item.id;
        });
        RestaurantService.getSelectItems();
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{TransactionStore.item.id}</Title>
                            </h1>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="#"
                                name="id"
                                value={TransactionStore.item.id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Тип операции"
                                name="id"
                                value={TransactionStore.item.type.name}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Тип карты"
                                name="id"
                                value={TransactionStore.item.card_type.split('\\').pop()}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                readOnly
                                items={RestaurantStore.selectItems}
                                label="Название ресторана"
                                name="restaurant_id"
                                value={TransactionStore.item.restaurant_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Метод оплаты"
                                name="payment_method_id"
                                value={TransactionStore.item.payment_method_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Сумма к оплате"
                                name="sum"
                                value={TransactionStore.item.sum}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Скидка"
                                name="discount_sum"
                                value={TransactionStore.item.discount_sum}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Чек"
                                name="check_number"
                                value={TransactionStore.item.check_number}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Касса"
                                name="cashbox_number"
                                value={TransactionStore.item.cashbox_number}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Выбор карт"
                                name="card_input_method"
                                value={TransactionStore.item.card_input_method}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="Дата"
                                name="id"
                                value={
                                    TransactionStore.item.created_at
                                        ? moment(TransactionStore.item.created_at).format('LLL')
                                        : 'не указана'
                                }
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
