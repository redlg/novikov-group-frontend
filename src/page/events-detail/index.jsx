import { React, withRouter, observable, observer, moment, computed, history } from '!/app';

import { Title, Button, Icon, Input, Select, Datepicker, Breadcrumb, Template, Textarea } from '!/uikit';

import { EventStore, EventTypeStore } from '!/stores';

import { EventService, EventTypeService } from '!/services';

import { Alert, Mask } from '!/utils';

@withRouter
@observer
export default class EventsDetail extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'События',
            url: '/events'
        },
        {
            name: 'Загрузка...'
        }
    ];


    id = 'new';

    componentDidMount = () => {
        this.getInformation();
    };

    getInformation = () => {
        EventStore.item = EventStore.newItem;

        this.id = this.props.match.params.id;

        if (this.id === 'new') {
            this.breadcrumb[1].name = 'Новое событие';
        } else {
            EventService.get(this.id).then(() => {
                this.breadcrumb[1].name = EventStore.item.name_ru;
            });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.id === 'new') {
            EventStore.item.id = '';
            EventService.create().then((response) => {
                Alert.success('Событие успешно создано');
                history.push('/events/' + response.body.id);
                this.getInformation();
            });
        } else {
            EventService.update().then(() => {
                Alert.success('Событие успешно сохранено');
            });
        }
    };

    handleChange = (data) => {
        EventStore.item[data.name] = data.value;
    };

    render = () => {
        return (
            <form className="row pt-3" onSubmit={this.handleSubmit}>
                <div className="col col-12 col-sm-4 col-lg-4 mb-3">

                </div>
                <div className="col-12 mb-3">
                    <Breadcrumb items={this.breadcrumb} />
                </div>
                <div className="col-6 offset-3">
                    <div className="row">
                        <div className="col-12">
                            <h1>
                                <Title>{EventStore.item.name_ru }</Title>
                            </h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 mb-3">
                            <Input
                                readOnly
                                label="#"
                                name="id"
                                value={EventStore.item.id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                required
                                label="Телефон"
                                name="phone"
                                placeholder="введите телефон"
                                mask={Mask.PHONE_MOBILE}
                                value={EventStore.item.phone}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Input
                                label="Название"
                                name="name_ru"
                                value={EventStore.item.name_ru}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Textarea
                                label="Анонс"
                                name="announce_ru"
                                value={EventStore.item.announce_ru}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Select
                                value={EventStore.filter.event_type_id}
                                items={EventTypeStore.items}
                                template={(item) => item.name_ru}
                                placeholder="Не выбран"
                                name="event_type_id"
                                label="Тип события"
                                onChange={this.handleChange}
                            />
                        </div>

                        <div className="col-6 mb-3">
                            <Datepicker
                                label="Начинается"
                                name="started_at"
                                value={EventStore.item.started_at}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6 mb-3">
                            <Datepicker
                                label="Заканчивается"
                                name="ended_at"
                                value={EventStore.item.ended_at}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 d-flex justify-content-center">
                            <Template visible={this.id === 'new'}>
                                <Button primary submit className="mr-3">
                                    Создать
                                </Button>
                            </Template>
                            <Template visible={this.id !== 'new'}>
                                <Button success submit className="mr-3">
                                    Обновить
                                </Button>
                            </Template>
                        </div>
                    </div>
                </div>
            </form>
        );
    };
}
