import { React, withRouter, observable, observer, lodash } from '!/app';

import {
    Image,
    Upload,
    GoogleMap,
    Select,
    Input,
    Checkbox,
    Textarea,
    Button,
    Breadcrumb,
    Title,
    Tabs,
    TabsButton
} from '!/uikit';

import { RestaurantService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class RestaurantsAddress extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        address_id: ''
    };

    @observable
    address = {
        id: false,
        address_type_id: 0,
        apartment: '',
        building: '',
        city: '',
        comment: '',
        country: '',
        floor: '',
        intercom: '',
        lat: false,
        lng: false,
        street: '',
        text_en: '',
        text_ru: '',
        timezone: ''
    };

    @observable address_types = Array();

    componentDidMount = () => {
        RestaurantService.getAddressTypes().then((response) => {
            response.body.address_types.forEach((item) => {
                this.address_types.push(item);
            });
        });

        RestaurantService.getRestaurant(this.props.match.params.id)
            .then((response) => {
                this.breadcrumb[1].name = response.body.name_ru;
                lodash.merge(this.restaurant, response.body);
            })
            .then(() => {
                RestaurantService.getAddress(this.restaurant.address_id).then((response) => {
                    lodash.merge(this.address, response.body);
                });
            });
    };

    handleChange = (data) => {
        this.chef[data.name] = data.value;
        if (e.name === 'lat' || e.name === 'lng') {
            this.map.marker.setPosition({
                lat: parseFloat(this.address.lat),
                lng: parseFloat(this.address.lng)
            });
            this.map.setPosition({
                lat: parseFloat(this.address.lat),
                lng: parseFloat(this.address.lng)
            });
        }
    };

    handleUpload = (file) => {
        console.log(file);
        this.chef.image_id = file.id;
        this.chef.image.link = file.link;
    };

    handleChangeMarker = (e) => {
        this.address.lat = e.value[0];
        this.address.lng = e.value[1];
    };

    handleSubmit = (e) => {
        e.preventDefault();
        RestaurantService.saveAddress(this.address).then((response) => {
            Alert.success('Адрес успешно сохранен');
        });
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <form onSubmit={this.handleSubmit}>
                    <h4>Общая информация</h4>
                    <div className="row">
                        <div className="col-6">
                            <Select
                                required
                                items={this.address_types}
                                className="mb-2"
                                label="Тип адреса"
                                name="address_type_id"
                                value={this.address.address_type_id}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Страна"
                                name="country"
                                value={this.address.country}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Город"
                                name="city"
                                value={this.address.city}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Улица"
                                name="street"
                                value={this.address.street}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Строение"
                                name="building"
                                value={this.address.building}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Квартира"
                                name="apartment"
                                value={this.address.apartment}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                className="mb-2"
                                label="Этаж"
                                name="floor"
                                value={this.address.floor}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                className="mb-2"
                                label="Домофон"
                                name="intercom"
                                value={this.address.intercom}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <h4>Текстовый вид</h4>
                    <div className="row">
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Русский язык"
                                name="text_ru"
                                value={this.address.text_ru}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Английский язык"
                                name="text_en"
                                value={this.address.text_en}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <h4>Карта</h4>
                    <div className="row">
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Широта"
                                name="lat"
                                value={this.address.lat}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col-6">
                            <Input
                                required
                                className="mb-2"
                                label="Долгота"
                                name="lng"
                                value={this.address.lng}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    {this.address.lng !== false &&
                        this.address.lat !== false && (
                            <GoogleMap
                                className="mb-2"
                                onChangeMarker={this.handleChangeMarker}
                                ref={(map) => (this.map = map)}
                                zoom={15}
                                height="300px"
                                center={[this.address.lat, this.address.lng]}
                                marker={[this.address.lat, this.address.lng]}
                            />
                        )}

                    <div className="row mb-3">
                        <div className="col-12 d-flex justify-content-end">
                            <Button submit success>
                                Сохранить
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        );
    };
}
