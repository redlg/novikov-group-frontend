import { React, withRouter, observable, observer, lodash } from '!/app';

import { Button, Tags, Breadcrumb, Title, Tabs, TabsButton } from '!/uikit';

import { KitchenStore } from '!/stores';
import { RestaurantService, KitchenService } from '!/services';

import { Alert } from '!/utils';

@withRouter
@observer
export default class Restaurants extends React.Component {
    @observable
    breadcrumb = [
        {
            name: 'Рестораны',
            url: '/restaurants'
        },
        {
            name: 'Загрузка...'
        }
    ];

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        kitchen_ids: []
    };

    @observable selected_tag = 0;

    componentDidMount = () => {
        KitchenService.getItems().then((response) => {
            this.selected_tag = KitchenStore.items[0].id;
        });

        RestaurantService.getRestaurant(this.props.match.params.id).then((response) => {
            this.breadcrumb[1].name = response.body.name_ru;
            lodash.merge(this.restaurant, response.body);
            this.restaurant.kitchen_ids = this.restaurant.kitchens.map((item) => item.id);
        });
    };

    handleChange = (data) => {
        this.restaurant[data.name] = data.value;
    };

    handleSave = (e) => {
        e.preventDefault();
        const data = {
            id: this.restaurant.id,
            kitchen_ids: this.restaurant.kitchen_ids
        };
        RestaurantService.saveRestaurant(data).then((response) => {
            Alert.success('Ресторан ' + this.restaurant.name_ru + ' успешно сохранен');
        });
    };

    render = () => {
        return (
            <div className="pt-3">
                <Breadcrumb items={this.breadcrumb} />
                <div className="d-flex align-items-center mb-3">
                    <h1>
                        <Title className="mr-3">{this.restaurant.name_ru}</Title>
                    </h1>
                </div>
                <Tabs>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id}>
                        Описание
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/images'}>
                        Фотографии
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/chef'}>
                        Шеф-повар
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/delivery-menu'}>
                        Меню доставки
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/menu'}>
                        Меню
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/tags'}>
                        Теги
                    </TabsButton>
                    <TabsButton active={true} to={'/restaurants/' + this.props.match.params.id + '/kitchens'}>
                        Кухни
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/legal-entity'}>
                        Юр. Лицо
                    </TabsButton>
                    <TabsButton active={false} to={'/restaurants/' + this.props.match.params.id + '/address'}>
                        Адрес
                    </TabsButton>
                </Tabs>
                <Button success onClick={this.handleSave} className="mb-3">
                    Сохранить
                </Button>
                <Tags
                    label="Выберите кухню для добавления"
                    placeholder="Выберите кухню"
                    items={KitchenStore.selectItems}
                    name="kitchen_ids"
                    value={this.restaurant.kitchen_ids}
                    onChange={this.handleChange}
                />
            </div>
        );
    };
}
