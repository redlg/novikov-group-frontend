import { React, withRouter, observable, observer, moment, computed } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Textarea,
    Datepicker,
    Tags
} from '!/uikit';

import { CompilationRestaurantStore, TagStore, RestaurantStore } from '!/stores';

import { CompilationRestaurantService, TagService, RestaurantService } from '!/services';

@withRouter
@observer
export default class CompilationRestaurants extends React.Component {


    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        tag_ids: []
    };

    componentDidMount = () => {
        CompilationRestaurantService.getItems();
        TagService.getItems().then(() => {
            this.selected_tag = TagStore.items[0].id;
        });
        RestaurantService.getSelectItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        CompilationRestaurantService.getItems();
    };

    handleChange = (object) => {
        CompilationRestaurantStore.filter[object.name] = object.value;
        this.restaurant[object.name] = object.value;
    };

    handleReset = (object) => {
        CompilationRestaurantStore.filter.cur_page = 1 ;
        CompilationRestaurantStore.filter.tag_id = 0 ;
        CompilationRestaurantStore.filter.name = '';
        CompilationRestaurantService.getItems();
    };


    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Подборки ресторанов</Title>
                    </h1>
                    <Button success to="/compilation-restaurants/new">
                        Добавить{' '}
                        <Icon size="24" className="ml-1">
                            +
                        </Icon>
                    </Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={CompilationRestaurantStore.filter.name}
                                placeholder="Введите название"
                                name="name"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Select
                                label="Выберите тег"
                                placeholder="Выберите тег"
                                items={TagStore.selectItems}
                                name="tag_id"
                                value={CompilationRestaurantStore.filter.tag_id}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <TablePagination store={CompilationRestaurantStore} onChange={CompilationRestaurantService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={CompilationRestaurantStore} onChange={CompilationRestaurantService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="name" store={CompilationRestaurantStore} onChange={CompilationRestaurantService.getItems}>
                            Название
                        </TableSort>
                        <TableSort store={CompilationRestaurantStore} onChange={CompilationRestaurantService.getItems}>
                            Тег
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {CompilationRestaurantStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/compilation-restaurants/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.name}</TableCell>
                                    <TableCell>{item.tags.map((item) => item.name_ru).join(', ')}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={CompilationRestaurantStore} onChange={CompilationRestaurantService.getItems} />
            </div>
        );
    };
}
