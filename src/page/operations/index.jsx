import { React, withRouter, observable, observer, moment } from '!/app';

import {
    Autocomplete,
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Datepicker,
    Checkbox,
} from '!/uikit';

import { OperationStore,OperationTypeStore,RestaurantStore,DiscountCardTypeStore,CertificateTypeStore } from '!/stores';

import { OperationService,OperationTypeService,RestaurantService, DiscountCardTypeService,CertificateTypeService  } from '!/services';


@withRouter
@observer
export default class Operations extends React.Component {

    @observable
    restaurant = {
        id: '',
        name_ru: 'Загрузка...',
        tag_ids: []
    };

    @observable
        card_types = Array();

   object_type = {
     Certificate: 'Сертификат',
     DiscountCard: 'Дисконтная карта'
   };


    componentDidMount = () => {
        OperationTypeService.getItems();
        OperationService.getFilters();
        OperationService.getItems();
        RestaurantService.getSelectItems();
        DiscountCardTypeService.getItems();
        CertificateTypeService.getItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        OperationService.getItems();
    };

    handleChange = (object) => {
        if(object.name === 'relation') {
           if( object.value == 0  ) {
               OperationStore.filter.card_type_id = 0;
           }
           if(object.value === 'certificate'){
               this.card_types = CertificateTypeStore.items;

           }
           if(object.value === 'discount_card'){
               this.card_types = DiscountCardTypeStore.items;

           }
        }

        OperationStore.filter[object.name] = object.value;
        this.restaurant[object.name] = object.value;
    };

    handleChangeQuickFilter = (object) => {
        if(object.type === 'checkbox'){
            OperationStore.filter.reusable = false;
            OperationStore.filter.discount_cards = false;
            OperationStore.filter.disposable = false;
        }
        OperationStore.filter[object.name] = object.value;
    };

    transactionsSum = (transactions) => {
        let sum = 0;
        transactions.forEach((item) => {
            sum = sum + item.sum
        });
        return sum;
    };

    handleReset = (object) => {
        OperationStore.filter.cur_page = 1;
        OperationStore.filter.operation_type_id = 0;
        OperationStore.filter.initiator = 0;
        OperationStore.filter.issuer_id = 0;
        OperationStore.filter.restaurant_id = 0;
        OperationStore.filter.relation = 0;
        OperationStore.filter.sum = ['', ''];
        OperationStore.filter.created_at_from = null;
        OperationStore.filter.created_at_to = null;
        OperationStore.filter.query = '';
        OperationStore.filter.email = '';
        OperationService.getItems();
    };

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Операции</Title>
                    </h1>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Input
                                value={OperationStore.filter.number}
                                placeholder="Введите номер карты"
                                name="number"
                                label="Номер карты"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Autocomplete
                                template={(item) =>
                                    item.first_name + ' ' + item.last_name + ' (' + item.phone + ')'
                                }
                                label="Гость"
                                placeholder="Начните писать имя или телефон гостя"
                                name="guest_id"
                                api="/guest"
                                queryfield="phone"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={OperationStore.filter.operation_type_id}
                                items={OperationTypeStore.items.operation_types}
                                placeholder="Выберите тип операции"
                                name="operation_type_id"
                                label="Тип операции"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={OperationStore.filter.initiator}
                                items={OperationStore.filters.initiators}
                                placeholder="не выбран"
                                name="initiator"
                                label="Инициатор"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-6 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={OperationStore.filter.issuer_id}
                                placeholder="не выбран"
                                name="issuer_id"
                                label="Эмитент"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={OperationStore.filter.restaurant_id}
                                items={RestaurantStore.selectItems}
                                name="restaurant_id"
                                label="Ресторан"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                value={OperationStore.filter.relation}
                                items={OperationStore.filters.relations}
                                name="relation"
                                label="Тип объекта"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Select
                                readOnly={OperationStore.filter.relation == 0 }
                                value={OperationStore.filter.card_type_id}
                                items={this.card_types}
                                name="card_type_id"
                                label="Тип карты"
                                placeholder="не выбран"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Range
                                value={OperationStore.filter.sum}
                                label="Сумма"
                                placeholder={['от', 'до']}
                                name="sum"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3">
                            <Input
                                label="Номер сертификата"
                                name="number"
                                value={OperationStore.filter.number_certificate}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3" >
                            <Checkbox
                                className="mb-3"
                                label="Быстрый фильтр"
                                name="disposable"
                                value={OperationStore.filter.disposable}
                                onChange={this.handleChangeQuickFilter}>
                                Одноразовые сертификаты
                            </Checkbox>
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3" >
                            <Checkbox
                                className="mb-3"
                                name="reusable"
                                label="Быстрый фильтр"
                                value={OperationStore.filter.reusable}
                                onChange={this.handleChangeQuickFilter}>
                                Многоразовые сертификаты
                            </Checkbox>
                        </div>
                        <div className="col col-12 col-sm-4 col-lg-2 mb-3" >
                            <Checkbox
                                className="mb-3"
                                name="discount_cards"
                                label="Быстрый фильтр"
                                value={OperationStore.filter.discount_cards}
                                onChange={this.handleChangeQuickFilter}>
                                Дисконтные карты
                            </Checkbox>
                        </div>
                        <div className="col col-12 col-sm-3 col-lg-2 mb-3">
                            <div className="d-flex">
                                <Datepicker
                                    className="mr-1"
                                    label="Дата от"
                                    name="created_at_from"
                                    start={OperationStore.filter.created_at_from}
                                    end={OperationStore.filter.created_at_to}
                                    value={OperationStore.filter.created_at_from}
                                    onChange={this.handleChange}
                                />
                                <Datepicker
                                    label="до"
                                    name="created_at_to"
                                    start={OperationStore.filter.created_at_from}
                                    end={OperationStore.filter.created_at_to}
                                    value={OperationStore.filter.created_at_to}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                            <Button submit>Применить</Button>
                        </div>
                    </div>
                </form>
                <TablePagination store={OperationStore} onChange={OperationService.getItems} />
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={OperationStore} onChange={OperationService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="created_at" store={OperationStore} onChange={OperationService.getItems}>
                            Дата
                        </TableSort>
                        <TableSort name="guest" store={OperationStore} onChange={OperationService.getItems}>
                            Гость
                        </TableSort>
                        <TableSort name="operation_type_id" store={OperationStore} onChange={OperationService.getItems}>
                            Тип операции
                        </TableSort>
                        <TableSort name="initiator" store={OperationStore} onChange={OperationService.getItems}>
                            Инициатор
                        </TableSort>
                        <TableSort name="issuer_id" store={OperationStore} onChange={OperationService.getItems}>
                            Эмитент
                        </TableSort>
                        <TableSort name="restaurant_id.name_ru" store={OperationStore} onChange={OperationService.getItems}>
                            Ресторан
                        </TableSort>
                        <TableSort name="card_type" store={OperationStore} onChange={OperationService.getItems}>
                            Тип объекта
                        </TableSort>
                        <TableSort name="operation_type_id" store={OperationStore} onChange={OperationService.getItems}>
                            Тип карты
                        </TableSort>
                        <TableSort name="number" store={OperationStore} onChange={OperationService.getItems}>
                            Номер карты/Cертификата
                        </TableSort>
                        <TableSort name="transaction.discount_sum" store={OperationStore} onChange={OperationService.getItems}>
                            Номинал
                        </TableSort>
                        <TableSort name="card.balance" store={OperationStore} onChange={OperationService.getItems}>
                            Баланс
                        </TableSort>
                        <TableSort name="transaction.sum" store={OperationStore} onChange={OperationService.getItems}>
                            Сумма транзакций
                        </TableSort>
                        <TableSort name="comment" store={OperationStore} onChange={OperationService.getItems}>
                            Комментарий
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {OperationStore.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/operations/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>
                                        {item.created_at ? moment(item.created_at).format('LLL') : 'не указана'}
                                    </TableCell>
                                    <TableCell>{item.guest || 'не указан'}</TableCell>
                                    <TableCell>{item.type.name}</TableCell>
                                    <TableCell>{item.initiator}</TableCell>
                                    <TableCell>{item.card.issuer_id || 'не указан'}</TableCell>
                                    <TableCell>
                                        {item.restaurant_id == null ? 'не указан' : RestaurantStore.itemById[item.restaurant_id] ? RestaurantStore.itemById[item.restaurant_id].name : 'Загрузка...'}
                                    </TableCell>
                                    <TableCell>{this.object_type[item.card_type.split('\\').pop()]}</TableCell>
                                    <TableCell>{item.card.type.name}</TableCell>
                                    <TableCell>{item.card.number}</TableCell>
                                    <TableCell>{item.card.type.sum}</TableCell>
                                    <TableCell>{item.card.balance}</TableCell>
                                    <TableCell>{this.transactionsSum(item.transactions)}</TableCell>
                                    <TableCell>{item.comment}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <TablePagination store={OperationStore} onChange={OperationService.getItems} />
            </div>
        );
    };
}
