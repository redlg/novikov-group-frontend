import { React, withRouter, observable, observer, moment, computed } from '!/app';

import {
    Title,
    Button,
    Icon,
    Input,
    Select,
    Range,
    Table,
    TableHead,
    TableBody,
    TableCell,
    TableSort,
    TableRow,
    TablePagination,
    Textarea,
    Datepicker
} from '!/uikit';

import { KitchenStore } from '!/stores';

import { KitchenService } from '!/services';

@withRouter
@observer
export default class Kitchens extends React.Component {
    componentDidMount = () => {
        KitchenService.getItems();
    };

    handleFilterSubmit = (e) => {
        e.preventDefault();
        KitchenService.getItems();
    };

    handleChange = (object) => {
        KitchenStore.filter[object.name] = object.value;
    };

    handleReset = (object) => {
        KitchenStore.filter.name_ru = '';
    };

    @computed get items(){
        return KitchenStore.items.filter(item => {
            return (item.name_ru + item.name_en).toLowerCase().indexOf(KitchenStore.filter.name_ru.toLowerCase()) > -1
        });
    }

    render = () => {
        return (
            <div className="pt-3">
                <div className="d-flex align-items-center mb-3">
                    <h1 className="mr-3">
                        <Title>Кухня</Title>
                    </h1>
                    <Button success to="/kitchens/new">
                        Добавить{' '}
                        <Icon size="24" className="ml-1">
                            +
                        </Icon>
                    </Button>
                </div>
                <form onSubmit={this.handleFilterSubmit}>
                    <div className="row">
                        <div className="col col-12 col-sm-4 col-lg-4 mb-3">
                            <Input
                                value={KitchenStore.filter.name_ru}
                                placeholder="Введите название"
                                name="name_ru"
                                label="Название"
                                onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="row mb-3">
                        <div className="col-12">
                            <Button reset outline secondary className="mr-2" onClick={this.handleReset}>
                                Сбросить
                            </Button>
                        </div>
                    </div>
                </form>
                <Table>
                    <TableHead>
                        <TableSort  width="60" name="id" store={KitchenStore} onChange={KitchenService.getItems}>
                            #
                        </TableSort>
                        <TableSort name="name_ru" store={KitchenStore} onChange={KitchenService.getItems}>
                            Название
                        </TableSort>
                        <TableSort name="name_en" store={KitchenStore} onChange={KitchenService.getItems}>
                            Name
                        </TableSort>
                    </TableHead>
                    <TableBody>
                        {this.items.map((item) => {
                            return (
                                <TableRow key={item.id} edit={'/kitchens/' + item.id}>
                                    <TableCell>{item.id}</TableCell>
                                    <TableCell>{item.name_ru}</TableCell>
                                    <TableCell>{item.name_en}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </div>
        );
    };
}
