import { React, PropTypes } from '!/app';

export class Image extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        src: PropTypes.string,
        size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        height: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    };

    render = () => {
        let style = {},
            className = this.props.className ? this.props.className : '',
            src = this.props.src ? this.props.src : 'http://placehold.it/' + this.props.size;

        if (this.props.width) style.width = this.props.width;
        else style.width = '100%';

        if (this.props.height) style.minHeight = this.props.height;

        return <img src={src} style={style} className={className} />;
    };
}
