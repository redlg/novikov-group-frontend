import { React, history } from '!/app';

export class TableRow extends React.Component {
    render = () => {
        return <tr onDoubleClick={() => history.push(this.props.edit)}>{this.props.children}</tr>;
    };
}
