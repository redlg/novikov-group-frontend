import { React, observer } from '!/app';

import { NavLink } from '!/uikit';

@observer
export class Breadcrumb extends React.Component {
    render = () => {
        return (
            <nav className="breadcrumb">
                {this.props.items.map((item) => {
                    if (item.url) {
                        return (
                            <NavLink
                                key={'bc' + item.name}
                                activeClassName=""
                                className="breadcrumb-item"
                                to={item.url}>
                                {item.name}
                            </NavLink>
                        );
                    }
                    return (
                        <span key={'bc' + item.name} className="breadcrumb-item active">
                            {item.name}
                        </span>
                    );
                })}
            </nav>
        );
    };
}
