import { React, PropTypes, observer } from '!/app';

@observer
export class Range extends React.Component {
    static propTypes = {
        onChange: PropTypes.func,
        onEnter: PropTypes.func,
        onBlur: PropTypes.func,
        name: PropTypes.string,
        value: PropTypes.any,
        label: PropTypes.string,
        placeholder: PropTypes.any,
        required: PropTypes.bool
    };

    transformNumber = (value) => {
        return value.replace(/[^0-9.]/g, '');
    };

    handleChangeLeft = (e) => {
        if (this.props.onChange)
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: 'range',
                value: [this.transformNumber(e.target.value), this.props.value[1]]
            });
    };
    handleChangeRight = (e) => {
        if (this.props.onChange)
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: 'range',
                value: [this.props.value[0], this.transformNumber(e.target.value)]
            });
    };

    handleKeyUp = (e) => {
        if (e.keyCode == 13)
            if (this.props.onEnter) {
                this.props.onEnter();
            }
    };

    render = () => {
        return (
            <div>
                {this.props.label && <label className="d-block w-100">{this.props.label}</label>}
                <div className="d-flex align-items-center justify-content-between">
                    <input
                        className="form-control"
                        value={this.props.value[0]}
                        placeholder={this.props.placeholder[0]}
                        onKeyUp={this.handleKeyUp}
                        onBlur={this.props.onBlur}
                        required={this.props.required}
                        onChange={this.handleChangeLeft}
                    />
                    <span className="px-2">-</span>
                    <input
                        className="form-control"
                        value={this.props.value[1]}
                        placeholder={this.props.placeholder[1]}
                        onKeyUp={this.handleKeyUp}
                        onBlur={this.props.onBlur}
                        required={this.props.required}
                        onChange={this.handleChangeRight}
                    />
                </div>
            </div>
        );
    };
}
