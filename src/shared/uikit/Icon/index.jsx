import { React, PropTypes } from '!/app';

export class Icon extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        name: PropTypes.string,
        disabled: PropTypes.bool,
        width: PropTypes.any,
        size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    };

    render = () => {
        let style = {},
            className = this.props.className ? this.props.className : '';

        if (this.props.disabled) style.opacity = '0.5';

        if (this.props.fontSize) style.fontSize = this.props.fontSize + 'px';

        if (this.props.width) style.width = this.props.width + 'px';

        if (!this.props.name) {
            return (
                <span
                    className={this.props.className}
                    style={{
                        fontSize: this.props.size + 'px',
                        lineHeight: '0px'
                    }}>
                    {this.props.children}
                </span>
            );
        }
        return (
            <i className={'fa fa-' + this.props.name + ' ' + className} style={style} onClick={this.props.onClick} />
        );
    };
}
