import { React, observer } from '!/app';

@observer
export class Table extends React.Component {
    render = () => {
        let style = {};
        return (
            <div
                style={{
                    maxWidth: '100%',
                    overflowX: 'auto'
                }}>
                <table className="table table-bordered table-hover" style={style}>
                    {this.props.children}
                </table>
            </div>
        );
    };
}
