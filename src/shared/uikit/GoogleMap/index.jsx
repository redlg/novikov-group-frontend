import { React, observer, PropTypes } from '!/app';

@observer
export class GoogleMap extends React.Component {
    map = null;
    marker = null;

    static propTypes = {
        marker: PropTypes.any,
        center: PropTypes.any,
        height: PropTypes.any,
        zoom: PropTypes.number,
        onChangeMarker: PropTypes.func
    };

    componentDidMount = () => {
        let mapPosition = {
            lat: parseFloat(this.props.center[0]),
            lng: parseFloat(this.props.center[1])
        };

        let markerPostion = {
            lat: parseFloat(this.props.marker[0]),
            lng: parseFloat(this.props.marker[1])
        };

        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: this.props.zoom ? this.props.zoom : 14,
            center: mapPosition
        });

        this.marker = new google.maps.Marker({
            position: markerPostion,
            map: this.map,
            draggable: true
        });

        this.marker.addListener('drag', this.handleChangeMarker);
    };

    handleChangeMarker = (e) => {
        if (this.props.onChangeMarker)
            this.props.onChangeMarker({
                name: this.props.name ? this.props.name : '',
                type: this.props.type ? this.props.type : 'marker',
                value: [e.latLng.lat(), e.latLng.lng()]
            });
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        return false;
    };

    setPosition = (e) => {
        this.map.setCenter(e);
    };

    render = () => {
        const height = this.props.height ? this.props.height : '200px';
        return (
            <div className={this.props.className ? this.props.className : ''} style={{ width: '100%', height: height }}>
                <div id="map" style={{ width: '100%', height: '100%' }} />
            </div>
        );
    };
}
