import { React } from '!/app';

export class TableHead extends React.Component {
    render = () => {
        return (
            <thead>
                <tr>{this.props.children}</tr>
            </thead>
        );
    };
}
