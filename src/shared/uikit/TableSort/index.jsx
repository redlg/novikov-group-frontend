import { React, PropTypes, observer } from '!/app';

import { Icon } from '!/uikit';

import './style.scss';

@observer
export class TableSort extends React.Component {
    static propTypes = {
        onChange: PropTypes.func,
        store: PropTypes.any,
        name: PropTypes.string,
        width: PropTypes.string

    };

    handleSort = (e) => {
        e.preventDefault();
        this.props.store.filter.sort_type = this.props.store.filter.sort_type === 'asc' ? 'desc' : 'asc';
        this.props.store.filter.sort_by = this.props.name;
        this.props.onChange();
    };

    render = () => {
        if (!this.props.store) {
            return (
                <th width={this.props.width} className="table-sort">
                    <div className="d-flex align-items-center justify-content-between">{this.props.children}</div>
                </th>
            );
        }

        let data = this.props.store.filter;
        let arrow_up = <Icon name="arrow-up" disabled />;
        let arrow_down = <Icon name="arrow-down" disabled />;
        if (this.props.name === data.sort_by) {
            if (data.sort_type === 'desc') {
                arrow_down = <Icon name="arrow-down" />;
            }
            if (data.sort_type === 'asc') {
                arrow_up = <Icon name="arrow-up" />;
            }
        }
        return (
            <th width={this.props.width}  className="table-sort" onClick={this.handleSort}>
                <div className="d-flex align-items-center justify-content-between">
                    <span>{this.props.children}</span>
                    <div className="text-nowrap ml-2">
                        {arrow_down}
                        {arrow_up}
                    </div>
                </div>
            </th>
        );
    };
}
