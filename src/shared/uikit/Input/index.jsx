import { React, PropTypes, observer, computed, classnames } from '!/app';

import MaskedTextInput from 'react-text-mask';

import './style.scss';

@observer
export class Input extends React.Component {
    static propTypes = {
        type: PropTypes.string,
        value: PropTypes.any,
        name: PropTypes.string,
        placeholder: PropTypes.string,
        form: PropTypes.bool,
        mask: PropTypes.array,
        label: PropTypes.string,
        required: PropTypes.bool,
        readOnly: PropTypes.bool,
        disabled: PropTypes.bool,
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
        onEnter: PropTypes.func,
        onClick: PropTypes.func
    };

    checkValidationMask = (newValue) => {
        if (this.props.mask && this.props.required && this.maskedInput) {
            if (!newValue) {
                this.maskedInput.inputElement.setCustomValidity('Заполните это поле');
            } else {
                const value = newValue.split('_').join('');
                if (value.length !== this.props.mask.length) {
                    this.maskedInput.inputElement.setCustomValidity('Заполните это поле');
                } else {
                    this.maskedInput.inputElement.setCustomValidity('');
                }
            }
        }
    };

    handleBlur = (e) => {
        if (this.props.onBlur) {
            this.props.onBlur(e);
        }
    };

    handleKeyUp = (e) => {
        if (e.keyCode == 13) {
            if (this.props.onEnter) {
                this.props.onEnter();
            }
        }
    };

    handleClick = (e) => {
        if (this.props.onClick) {
            this.props.onClick();
        }
    };

    handleChange = (e) => {
        e.preventDefault();

        if (this.props.onChange) {
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: this.props.type ? this.props.type : 'text',
                value: e.target.value
            });
        }
    };

    @computed
    get input() {
        if (this.props.mask) {
            return (
                <MaskedTextInput
                    ref={(input) => (this.maskedInput = input)}
                    value={this.props.value === null ? '' : this.props.value}
                    showMask
                    onChange={this.handleChange}
                    mask={this.props.mask}
                    className="form-control"
                />
            );
        }
        return (
            <input
                type={this.props.type ? this.props.type : 'text'}
                name={this.props.name ? this.props.name : ''}
                value={this.props.value === null ? '' : this.props.value}
                onKeyUp={this.handleKeyUp}
                onBlur={this.handleBlur}
                onClick={this.handleClick}
                className="form-control"
                readOnly={this.props.readOnly ? true : false}
                required={this.props.required ? true : false}
                disabled={this.props.disabled ? true : false}
                placeholder={this.props.placeholder ? this.props.placeholder : ''}
                onChange={this.handleChange}
            />
        );
    }

    render = () => {
        this.checkValidationMask(this.props.value);

        let className = classnames('uikit-input', 'w-100', this.props.className, {
            'form-group': this.props.form
        });

        return (
            <div className={className}>
                {this.props.required && this.props.label && <div className="uikit-input__required">*</div>}
                {this.props.label && <label>{this.props.label}</label>}
                {this.input}
            </div>
        );
    };
}
