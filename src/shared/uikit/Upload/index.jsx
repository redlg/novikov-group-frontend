import { React, PropTypes, observer, observable, computed } from '!/app';

import { Dropzone } from '!/uikit';
import { Api } from '!/utils';

@observer
export class Upload extends React.Component {
    static propTypes = {
        onSuccess: PropTypes.func,
        url: PropTypes.string.isRequired
    };

    @observable inProgress = false;

    handleDrop = (files) => {
        this.inProgress = true;
        Api.file(this.props.url, files[0]).then((response) => {
            this.inProgress = false;

            if (this.props.onSuccess) this.props.onSuccess(response.body);
        });
    };

    @computed
    get button() {
        let className = this.props.className ? this.props.className : '';
        if (this.inProgress) className += ' progress-bar progress-bar-striped progress-bar-animated';
        return (
            <Dropzone
                onDrop={this.handleDrop}
                className={'btn btn-outline-success ' + className}
                style={{ height: 'auto' }}>
                {this.props.children}
                <i className="fa fa-upload ml-2" />
            </Dropzone>
        );
    }

    render = () => {
        if (this.props.center) {
            return <div className="d-flex justify-content-center">{this.button}</div>;
        }
        return this.button;
    };
}
