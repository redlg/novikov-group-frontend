import React from 'react';

export class Tabs extends React.Component {
    render = () => {
        return <ul className="nav nav-tabs mb-3">{this.props.children}</ul>;
    };
}
