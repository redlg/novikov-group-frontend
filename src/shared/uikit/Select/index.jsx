import { React, PropTypes, observer, computed, classnames } from '!/app';

import './style.scss';

@observer
export class Select extends React.Component {
    static propTypes = {
        value: PropTypes.any,
        name: PropTypes.string,
        placeholder: PropTypes.string,
        form: PropTypes.bool,
        label: PropTypes.string,
        required: PropTypes.bool,
        readOnly: PropTypes.bool,
        onChange: PropTypes.func,
        items: PropTypes.any,
        template: PropTypes.func
    };

    template = (item) => {
        const value = item.name ? item.name : item.value;
        return this.props.template ? this.props.template(item) : value;
    };

    handleChange = (e) => {
        e.preventDefault();
        if (this.props.onChange)
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: 'select',
                value: e.target.value
            });
    };

    render = () => {
        let className = classnames('uikit-select', 'w-100', this.props.className, {
            'form-group': this.props.form
        });

        const items = this.props.items ? this.props.items : [];

        return (
            <div className={className}>
                {this.props.required && <div className="uikit-select__required">*</div>}
                {this.props.label && <label>{this.props.label}</label>}
                <select
                    name={this.props.name ? this.props.name : ''}
                    value={this.props.value === null ? '' : this.props.value}
                    onChange={this.handleChange}
                    readOnly={this.props.readOnly ? true : false}
                    className="form-control"
                    required={this.props.required ? true : false}>
                    {this.props.placeholder && <option value="">{this.props.placeholder}</option>}
                    {items.map((item) => (
                        <option value={item.id} key={item.id}>
                            {this.template(item)}
                        </option>
                    ))}
                </select>
            </div>
        );
    };
}
