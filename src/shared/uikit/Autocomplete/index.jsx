import { React, PropTypes, observer, observable, computed, classnames } from '!/app';

import { ClickOutside } from '!/uikit';

import { Api } from '!/utils';

import './style.scss';

@observer
export class Autocomplete extends React.Component {
    static propTypes = {
        api: PropTypes.string,
        queryfield: PropTypes.string,
        name: PropTypes.string,
        placeholder: PropTypes.string,
        form: PropTypes.bool,
        label: PropTypes.string,
        template: PropTypes.func,
        onChange: PropTypes.func
    };

    focused = false;
    _id = null;
    @observable _value = '';
    @observable value = '';
    @observable items = Array();

    handleFocus = () => {
        this.focused = true;
        this.refs.inputElement.select();
    };

    handleBlur = (e) => {
        this.focused = false;
        this.value = this._value;
        this.items = Array();

        this.checkValidation(this.props.value);
    };

    handleSelect = (item) => {
        this._id = item.id;

        const value = this.template(item);

        this._value = value;
        this.value = value;

        this.items = Array();

        if (this.props.onChange) {
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: 'autocomplete',
                value: item.id
            });
        }
    };

    handleChange = (e) => {
        e.preventDefault();
        const queryfield = this.props.queryfield ? this.props.queryfield : 'query';

        this.value = e.target.value;
        if (this.value !== '') {
            const data = {
                per_page: 20
            };
            data[queryfield] = this.value;
            Api.get(this.props.api, data).then((response) => {
                if (this.focused) {
                    this.items = response.body.items;
                }
            });
        } else {
            this.items = Array();
        }
    };

    template = (item) => {
        return this.props.template ? this.props.template(item) : item.name;
    };

    checkValidation = (newValue) => {
        if (this.props.required && this.refs.inputElement) {
            if (this.focused) {
                this.refs.inputElement.setCustomValidity('');
            } else {
                if (!this._id) {
                    this.refs.inputElement.setCustomValidity('Заполните это поле');
                } else {
                    this.refs.inputElement.setCustomValidity('');
                }
            }
        }
    };

    render = () => {
        this.checkValidation(this.props.value);

        let className = classnames('uikit-autocomplete', 'w-100', this.props.className, {
            'form-group': this.props.form
        });

        return (
            <ClickOutside onClickOutside={this.handleBlur}>
                <div className={className}>
                    {this.props.required && <div className="uikit-autocomplete__required">*</div>}
                    {this.props.label && <label>{this.props.label}</label>}
                    <input
                        ref="inputElement"
                        type="text"
                        name={this.props.name ? this.props.name : ''}
                        value={this.value}
                        className="form-control"
                        placeholder={this.props.placeholder ? this.props.placeholder : ''}
                        onFocus={this.handleFocus}
                        autoComplete="off"
                        onChange={this.handleChange}
                    />
                    {this.items.length !== 0 && (
                        <ul className="uikit-autocomplete__list">
                            {this.items.map((item) => (
                                <li
                                    key={item.id}
                                    className="uikit-autocomplete__item"
                                    onClick={() => this.handleSelect(item)}>
                                    {this.template(item)}
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
            </ClickOutside>
        );
    };
}
