import { React, PropTypes } from '!/app';

export class Modal extends React.Component {
    static propTypes = {
        onClose: PropTypes.func,
        active: PropTypes.bool,
        overlay: PropTypes.bool,
        close: PropTypes.bool
    };

    handleClose = (e) => {
        e.preventDefault();
        if (this.props.onClose) this.props.onClose(e);
    };

    render = () => {
        if (!this.props.active) return null;

        return (
            <div
                className={this.props.overlay !== false ? 'modal show' : 'modal show bg-inverse'}
                style={
                    this.props.overlay !== false
                        ? {
                              backgroundColor: 'rgba(0,0,0,0.5)',
                              overflow: 'auto'
                          }
                        : {
                              overflow: 'auto'
                          }
                }>
                <div
                    style={{
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0
                    }}
                    onClick={this.handleClose}
                />
                <div className="modal-dialog">
                    {this.props.close !== false && (
                        <button
                            type="button"
                            className="close"
                            onClick={this.handleClose}
                            style={{
                                position: 'absolute',
                                right: '16px',
                                top: '19px',
                                zIndex: '1'
                            }}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    )}
                    <div className="modal-content">{this.props.children}</div>
                </div>
            </div>
        );
    };
}
