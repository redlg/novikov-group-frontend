import { React, observer, observable, computed, withRouter } from '!/app';

import { Icon, Button } from '!/uikit';

@withRouter
@observer
export class ListGroupItem extends React.Component {
    handleDoubleClick = () => {
        if (this.props.folder) {
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: this.props.type ? this.props.type : 'listgroupitem',
                value: !this.props.expanded
            });
        }
        if (this.props.doubleClick && this.props.edit) {
            this.props.history.push(this.props.edit);
        }
    };

    render = () => {
        return (
            <div>
                <div onDoubleClick={this.handleDoubleClick} className={'list-group-item ' + this.props.className}>
                    <div className="w-100 d-flex align-items-center justify-content-between">
                        <div className="d-flex align-items-center">
                            {this.props.folder === true && (
                                <Icon
                                    name={this.props.expanded ? 'folder-open' : 'folder'}
                                    className="mr-2"
                                    width={21}
                                />
                            )}
                            <span>{this.props.name}</span>
                        </div>
                        <div className="d-flex align-items-center">
                            <Button primary style={{ fontSize: '18px', padding: '6px 9px' }} to={this.props.edit}>
                                <Icon name="pencil" />
                            </Button>
                        </div>
                    </div>
                </div>
                {this.props.expanded && <div style={{ marginLeft: '20px' }}>{this.props.children}</div>}
            </div>
        );
    };
}
