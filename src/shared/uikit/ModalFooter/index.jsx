import { React, PropTypes } from '!/app';

export class ModalFooter extends React.Component {
    static propTypes = {
        center: PropTypes.bool,
        right: PropTypes.bool,
        left: PropTypes.bool,
        between: PropTypes.bool,
        around: PropTypes.bool
    };

    render = () => {
        let subClassName = '';
        if (this.props.center) subClassName = 'd-flex align-items-center justify-content-center';

        if (this.props.end || this.props.right) subClassName = 'd-flex align-items-center justify-content-end';

        if (this.props.start || this.props.left) subClassName = 'd-flex align-items-center justify-content-start';

        if (this.props.between) subClassName = 'd-flex align-items-center justify-content-between';

        if (this.props.around) subClassName = 'd-flex align-items-center justify-content-around';

        return <div className={'modal-footer ' + subClassName}>{this.props.children}</div>;
    };
}
