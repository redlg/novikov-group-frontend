import { React, observable, observer, computed, PropTypes } from '!/app';

import { Select, Icon, Button } from '!/uikit';

@observer
export class Tags extends React.Component {
    static propTypes = {
        value: PropTypes.any,
        onChange: PropTypes.func,
        items: PropTypes.any,
        name: PropTypes.string
    };

    @observable selected_tag = '';

    @computed
    get tags() {
        return this.props.items.filter((item) => {
            return this.props.value.indexOf(item.id) !== -1;
        });
    }

    @computed
    get selectItems () {
        return this.props.items.filter((item) => {
            return this.props.value.indexOf(item.id) === -1;
        });
    }

    handleChangeTag = (data) => {
        this.selected_tag = data.value;
    };

    handleTagAdd = () => {
        if (this.props.value.filter((id) => id == this.selected_tag).length === 0 && this.selected_tag !== '') {
            const items = this.props.value.map((item) => item);

            const tag_id = this.props.items.filter((item) => item.id == this.selected_tag)[0]['id'];

            items.push(tag_id);

            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: 'tags',
                value: items
            });
        }
    };

    handleTagDelete = (id) => {
        const items = this.props.value.filter((item) => item != id);

        this.props.onChange({
            name: this.props.name ? this.props.name : '',
            type: 'tags',
            value: items
        });
    };

    render = () => {
        return (
            <div>
                <div className="row mb-2">
                    <div className="col-6">
                        <Select
                            placeholder={this.props.placeholder}
                            items={this.selectItems}
                            label={this.props.label}
                            name="selected_tag"
                            value={this.selected_tag}
                            onChange={this.handleChangeTag}
                        />
                    </div>
                    <div className="col-6 d-flex align-items-end">
                        <Button success outline onClick={this.handleTagAdd}>
                            Добавить тег
                        </Button>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 d-flex flex-wrap">
                        {this.tags.map((item) => {
                            return (
                                <div className="btn btn-secondary mr-2 mb-2" key={item.id}>
                                    <span className="mr-2">{item.name}</span>
                                    <Icon name="times" size="16" onClick={() => this.handleTagDelete(item.id)} />
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    };
}
