import { React, PropTypes, computed, observer, classnames } from '!/app';

import './style.scss';

@observer
export class Textarea extends React.Component {
    static propTypes = {
        type: PropTypes.string,
        value: PropTypes.any,
        name: PropTypes.string,
        placeholder: PropTypes.string,
        form: PropTypes.bool,
        label: PropTypes.string,
        required: PropTypes.bool,
        disabled: PropTypes.bool,
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
        onEnter: PropTypes.func,
        rows: PropTypes.number
    };

    handleBlur = (e) => {
        if (this.props.onBlur) this.props.onBlur(e);
    };

    handleKeyUp = (e) => {
        if (e.keyCode == 13)
            if (this.props.onEnter) {
                this.props.onEnter();
            }
    };

    handleChange = (e) => {
        e.preventDefault();
        if (this.props.onChange)
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: this.props.type ? this.props.type : 'text',
                value: e.target.value
            });
    };

    @computed
    get textarea() {
        let className = this.props.className ? this.props.className : '';
        return (
            <textarea
                rows={this.props.rows ? this.props.rows : 5}
                type={this.props.type ? this.props.type : 'textarea'}
                name={this.props.name ? this.props.name : ''}
                value={this.props.value === null ? '' : this.props.value}
                onKeyUp={this.handleKeyUp}
                onBlur={this.handleBlur}
                className={'form-control ' + className}
                required={this.props.required ? true : false}
                placeholder={this.props.placeholder ? this.props.placeholder : ''}
                onChange={this.handleChange}
            />
        );
    }

    render = () => {
        let className = classnames('uikit-input', 'w-100', this.props.className, {
            'form-group': this.props.form
        });

        return (
            <div className={className}>
                {this.props.required && <div className="uikit-input__required">*</div>}
                {this.props.label && <label>{this.props.label}</label>}
                {this.textarea}
            </div>
        );
    };
}
