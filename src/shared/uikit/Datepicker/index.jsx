import { React, PropTypes, observer, computed, classnames, moment } from '!/app';
import DatePicker from 'react-datepicker';

import './style.scss';

@observer
export class Datepicker extends React.Component {
    static propTypes = {
        value: PropTypes.any,
        name: PropTypes.string,
        placeholder: PropTypes.string,
        form: PropTypes.bool,
        label: PropTypes.string,
        required: PropTypes.bool,
        readOnly: PropTypes.bool,
        disabled: PropTypes.bool,
        onChange: PropTypes.func,
        start: PropTypes.any,
        end: PropTypes.any
    };

    @computed
    get value() {
        if (!this.props.value) {
            return null;
        }
        return moment(this.props.value);
    }

    @computed
    get minDate() {
        if (!this.props.start) {
            return null;
        }
        return moment(this.props.start);
    }

    @computed
    get maxDate() {
        if (!this.props.end) {
            return null;
        }
        return moment(this.props.end);
    }

    handleChange = (date) => {
        if (this.props.onChange) {
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: 'date',
                value: date ? date.format('YYYY-MM-DD hh:mm:ss') : null
            });
        }
    };

    render = () => {
        let className = classnames('uikit-input', 'w-100', this.props.className, {
            'form-group': this.props.form
        });

        return (
            <div className={className}>
                {this.props.required && <div className="uikit-datepicker__required">*</div>}
                {this.props.label && <label>{this.props.label}</label>}
                <DatePicker
                    popperPlacement="bottom-end"
                    dateFormat="LL"
                    locale="ru"
                    className="form-control"
                    isClearable={true}
                    selected={this.value}
                    readOnly={this.props.readOnly ? this.props.readOnly : false}
                    required={this.props.required ? this.props.required : false}
                    disabled={this.props.disabled ? this.props.disabled : false}
                    placeholderText={this.props.placeholder ? this.props.placeholder : 'Выберите дату'}
                    onChange={this.handleChange}
                    minDate={this.minDate}
                    maxDate={this.maxDate}
                    startDate={this.minDate}
                    endDate={this.maxDate}
                    dropdownMode="select"
                    showYearDropdown
                />
            </div>
        );
    };
}
