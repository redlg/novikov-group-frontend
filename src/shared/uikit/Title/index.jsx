import { React, PropTypes } from '!/app';

export class Title extends React.Component {
    static propTypes = {
        text: PropTypes.string,
        hidden: PropTypes.bool
    };

    render = () => {
        document.title = this.props.text ? this.props.text : this.props.children;

        if (this.props.hidden) {
            return null;
        }

        return <span>{this.props.children}</span>;
    };
}
