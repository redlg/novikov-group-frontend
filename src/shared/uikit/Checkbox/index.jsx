import { React, PropTypes, computed, observer } from '!/app';

@observer
export class Checkbox extends React.Component {
    static propTypes = {
        onChange: PropTypes.func,
        value: PropTypes.bool,
        label: PropTypes.string,
        name: PropTypes.string
    };

    handleChange = () => {
        if (this.props.onChange)
            this.props.onChange({
                name: this.props.name ? this.props.name : '',
                type: 'checkbox',
                value: !this.value
            });
    };
    @computed
    get value() {
        return this.props.value === null ? false : this.props.value;
    }
    render = () => {
        let className = this.props.className ? this.props.className : '';
        return (
            <div>
                {this.props.label && <label>{this.props.label}</label>}
                <div className={'form-check-label form-control pl-3 ' + className} onClick={this.handleChange}>
                    <div style={{ position: 'relative' }} className="ml-3">
                        <input type="checkbox" className="form-check-input" checked={this.value} />
                        <div>{this.props.children} </div>
                    </div>
                </div>
            </div>
        );
    };
}
