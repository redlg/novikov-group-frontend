import { React, PropTypes, classnames } from '!/app';

import { NavLink } from 'react-router-dom';

import './style.scss';

export class Button extends React.Component {
    static propTypes = {
        to: PropTypes.string,
        success: PropTypes.bool,
        danger: PropTypes.bool,
        secondary: PropTypes.bool,
        info: PropTypes.bool,
        warning: PropTypes.bool,
        link: PropTypes.bool,
        outline: PropTypes.bool,
        onClick: PropTypes.func
    };

    handleClick = (e) => {
        if (this.props.onClick) {
            this.props.onClick(e);
        }
    };
    render = () => {
        let className = classnames(
            this.props.className,
            'btn',
            'btn-primary',
            'd-inline-flex',
            'align-items-center',
            'justify-content-center',
            {
                'btn-success': this.props.success && !this.props.outline,
                'btn-danger': this.props.danger && !this.props.outline,
                'btn-secondary': this.props.secondary && !this.props.outline,
                'btn-info': this.props.info && !this.props.outline,
                'btn-warning': this.props.warning && !this.props.outline,
                'btn-link': this.props.link && !this.props.outline,
                'btn-outline-success': this.props.success && this.props.outline,
                'btn-outline-danger': this.props.danger && this.props.outline,
                'btn-outline-secondary': this.props.secondary && this.props.outline,
                'btn-outline-info': this.props.info && this.props.outline,
                'btn-outline-warning': this.props.warning && this.props.outline,
                'btn-outline-link': this.props.link && this.props.outline
            }
        );

        if (this.props.to) {
            return (
                <NavLink className={className} to={this.props.to} activeClassName="" onClick={this.handleClick}>
                    {this.props.children}
                </NavLink>
            );
        }

        let type = this.props.submit ? 'submit' : 'button';
        type = this.props.reset ? 'reset' : type;

        return (
            <button onClick={this.handleClick} type={type} className={className}>
                {this.props.children}
            </button>
        );
    };
}
