import { React, PropTypes, observer } from '!/app';

import { NavLink } from 'react-router-dom';

@observer
export class TabsButton extends React.Component {
    static propTypes = {
        active: PropTypes.bool,
        name: PropTypes.string
    };

    handleClick = () => {
        if (this.props.onChange) {
            this.props.onChange(this.props.name);
        }
    };

    render = () => {
        let className = this.props.active === true ? 'active' : false;

        if (this.props.to && this.props.active === false) {
            return (
                <li className="nav-item">
                    <NavLink
                        to={this.props.to}
                        className={'nav-link ' + className}
                        activeClassName=""
                        onClick={this.handleClick}
                        style={{ cursor: 'pointer' }}>
                        {this.props.children}
                    </NavLink>
                </li>
            );
        }

        return (
            <li className="nav-item">
                <div className={'nav-link ' + className} onClick={this.handleClick} style={{ cursor: 'pointer' }}>
                    {this.props.children}
                </div>
            </li>
        );
    };
}
