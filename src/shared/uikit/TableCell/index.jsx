import { React, PropTypes } from '!/app';

export class TableCell extends React.Component {
    static propTypes = {
        width: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    };

    render = () => {
        return <td width={this.props.width}>{this.props.children}</td>;
    };
}
