import { React } from '!/app';

export class ModalBody extends React.Component {
    render = () => {
        return <div className="modal-body">{this.props.children}</div>;
    };
}
