import { React, PropTypes, observer } from '!/app';

@observer
export class Template extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        hidden: PropTypes.bool,
        visible: PropTypes.bool
    };

    render = () => {
        if (this.props.visible === false) {
            return null;
        }
        if (this.props.hidden === true) {
            return null;
        }
        if (this.props.className) {
            return <div className={this.props.className}>{this.props.children}</div>;
        }
        return this.props.children;
    };
}
