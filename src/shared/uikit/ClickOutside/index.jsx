import { React, PropTypes, observer, observable, computed, classnames } from '!/app';
import onClickOutside from 'react-onclickoutside-decorator';

@onClickOutside
export class ClickOutside extends React.Component {
    static propTypes = {
        onClick: PropTypes.func,
        escape: PropTypes.bool
    };

    componentDidMount = () => {
        if (this.props.escape !== false) window.addEventListener('keyup', this.handleKeyUp);
    };

    componentWillUnmount = () => {
        window.removeEventListener('keyup', this.handleKeyUp);
    };

    handleKeyUp = (e) => {
        if (e.keyCode === 27) {
            this.props.onClick();
        }
    };

    handleClickOutside = (e) => {
        if (this.props.onClick()) this.props.onClick();
    };

    render = () => {
        if (!this.props.children) {
            return null;
        }
        return this.props.children;
    };
}
