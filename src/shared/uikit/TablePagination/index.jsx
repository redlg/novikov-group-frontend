import { React, PropTypes, computed, observer, lodash } from '!/app';

@observer
export class TablePagination extends React.Component {
    static propTypes = {
        store: PropTypes.any
    };

    handleChangePerPage = (e) => {
        this.props.store.filter['cur_page'] = 1;
        this.props.store.filter['per_page'] = e.target.value;
        this.props.onChange();
    };

    handlePageNext = (e) => {
        e.preventDefault();

        this.props.store.filter['cur_page'] = this.props.store.filter['cur_page'] + 1;
        this.props.onChange();
    };

    handlePagePrevious = (e) => {
        e.preventDefault();

        this.props.store.filter['cur_page'] = this.props.store.filter['cur_page'] - 1;
        this.props.onChange();
    };

    handlePageChange = (e, page) => {
        e.preventDefault();

        if (page !== '...') {
            this.props.store.filter['cur_page'] = page;
            this.props.onChange();
        }
    };

    createPage = (number, index) => {
        return (
            <li
                className={this.props.store.filter['cur_page'] == number ? 'page-item active' : 'page-item'}
                key={'pagination' + index}>
                <a className="page-link" href="" onClick={(e) => this.handlePageChange(e, number)}>
                    {number}
                </a>
            </li>
        );
    };

    @computed
    get pages() {
        let result = Array();
        result.push(1);
        result.push(this.props.store.pagination.last_page);
        result.push(this.props.store.filter.cur_page - 1);
        result.push(this.props.store.filter.cur_page);
        result.push(this.props.store.filter.cur_page + 1);

        result = lodash
            .uniq(result)
            .sort((a, b) => parseInt(a) > parseInt(b))
            .filter((x) => x >= 1 && x <= this.props.store.pagination.last_page);

        if (this.props.store.pagination.last_page != 1) {
            if (result[0] + 2 == result[1]) {
                result.splice(1, 0, 2);
            } else {
                if (result[0] + 1 !== result[1]) {
                    result.splice(1, 0, '...');
                }
            }

            if (result[result.length - 2] + 2 == result[result.length - 1]) {
                result.splice(result.length - 1, 0, result[result.length - 2] + 1);
            } else {
                if (result[result.length - 2] + 1 !== result[result.length - 1]) {
                    result.splice(result.length - 1, 0, '...');
                }
            }
        }

        return result.map((number, index) => this.createPage(number, index));
    }

    render = () => {
        // if (this.pages.length == 1) {
        //     return null;
        // }
        return (
            <div className="d-flex justify-content-between flex-wrap table-pagination">
                <div className="table-pagination_navigation mb-2">
                    <label>
                        Показано {this.props.store.pagination.from}-{this.props.store.pagination.to} из
                        {' ' + this.props.store.pagination.total}
                    </label>
                    <ul className="pagination mb-0">
                        {this.props.store.filter.cur_page !== 1 && (
                            <li className="page-item">
                                <a className="page-link" href="" onClick={this.handlePagePrevious}>
                                    Предыдущая
                                </a>
                            </li>
                        )}
                        {this.props.store.filter.cur_page == 1 && (
                            <li className="page-item disabled">
                                <a className="page-link" href="">
                                    Предыдущая
                                </a>
                            </li>
                        )}
                        {this.pages}
                        {this.props.store.filter.cur_page !== this.props.store.pagination.last_page && (
                            <li className="page-item">
                                <a className="page-link" href="" onClick={this.handlePageNext}>
                                    Следующая
                                </a>
                            </li>
                        )}
                        {this.props.store.filter.cur_page == this.props.store.pagination.last_page && (
                            <li className="page-item disabled">
                                <a className="page-link" href="">
                                    Следующая
                                </a>
                            </li>
                        )}
                    </ul>
                </div>
                <div className="table-pagination_perpage mb-2">
                    <label>На странице</label>
                    <select
                        className="form-control"
                        onChange={this.handleChangePerPage}
                        value={this.props.store.filter.per_page}>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
            </div>
        );
    };
}
