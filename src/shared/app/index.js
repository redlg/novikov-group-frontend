export { observable, action, computed, toJS } from 'mobx';
export { observer, inject } from 'mobx-react';
export { withRouter } from 'react-router';
export React from 'react';
export PropTypes from 'prop-types';
export classnames from 'classnames';

import { createBrowserHistory } from 'history';
export const history = createBrowserHistory();

export * as lodash from '../modules/lodash';

export moment from 'moment';
import 'moment/locale/ru';
