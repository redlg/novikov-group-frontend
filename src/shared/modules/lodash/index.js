export keyBy from './parts/keyBy';
export uniq from './parts/uniq';
export merge from './parts/merge';

//
// import mergeWith from './parts/mergeWith';
//
// //merge fix
// export const merge = (object, other) => {
//     const customizer = (objValue, srcValue) =>  {
//         if(srcValue === null){
//             return objValue;
//         }
//         return srcValue;
//     }
//     return mergeWith(object, other, customizer);
// }
