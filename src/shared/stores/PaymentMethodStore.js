import { observable, computed, lodash } from '!/app';

export default new class PaymentMethodStore {
    @observable
    filters = {
        payment_methods: [],
    };


    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',


    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();

    @observable selectItems = Array();

    @computed get itemById(){
        return lodash.keyBy(this.selectItems, 'id');
    }
}();
