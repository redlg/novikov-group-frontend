import { observable, computed, lodash } from '!/app';

export default new class EventStore {
    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        name_ru: '',
        event_type_id: 0
    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        name_ru: '',
        started_at: null,
        ended_at: null,
        phone: '',
        event_type_id: 0,
        announce_ru: ''
    };

    newItem = null;

    constructor() {
        this.newItem = Object.assign({}, this.item);
    }
}();
