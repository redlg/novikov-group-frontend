import { observable, computed, lodash } from '!/app';

export default new class KitchenStore {
    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        name_ru: ''
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        name_ru: '',
        name_en: ''

    }

    @computed
    get selectItems() {
        return this.items.map((item) => {
            return {
                id: item.id,
                name: item.name_ru
            };
        });
    }

    newItem = null;

    constructor() {
        this.newItem = Object.assign({}, this.item);
    }
}();
