import { observable, computed, lodash } from '!/app';

export default new class OperationStore {
    @observable
    filters = {
        events: [],
        initiators: [],
        quick_filter: [],
        relations: [],
    };


    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        number: '',
        operation_type_id: '',
        created_at_from: null,
        created_at_to: null,
        initiator: '',
        restaurant_id: '',
        card_type: '',
        sum: ['', ''],
        number_certificate: '',
        reusable: false,
        discount_cards: false,
        disposable: false,
        relation: '',
        comment: '',
        type: {
            name: '',
            id: ''
        },
        card_type_id: '',
        sum_from: null,
        sum_to: null,

    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        card_type: '',
        created_at: '',
        comment: '',
        type: {
            name: '',
            id: ''
        },
        issuer_id: '',
        card_type_id:'',
        initiator: '',
        restaurant_id: '',
        card_id:'',
        card: {
            balance: 0,
            type: {
                id:'',
                name: '',
                discount: '',
                rkeeper_id: '',
                life_time:'',
                sum:''

            }
        },
        transactions: [],

    };
}();
