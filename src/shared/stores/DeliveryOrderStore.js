import { observable, computed, lodash } from '!/app';

export default new class DeliveryOrderStore {
    @observable
    filters = {
        delivery_methods: [],
        delivery_payment_methods: [],
        statuses: []
    };

    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        name_ru: '',
        guest_id:'',
        restaurant_id: '',
        created_at_to: null,
        created_at_from: null,
        canceled_at_from:null,
        canceled_at_to: null,
        completed_at_from: null,
        completed_at_to:null,
        sum: ['', ''],
        delivery_method: {
            id:'',
            name:''
        },
        delivery_payment_method: {
            id:'',
            name:''
        },
        status: {
            id:'',
            name:''
        }


    };

    @observable items = Array();

    @observable
    item = {
        guest: {
            id: '',
            first_name:'',
            last_name:'',
        },
        address: {
            street:'',
            city:''

        },
        created_at: null,
        completed_at: null,
        delivered_at: null,
        canceled_at: null,
        delivery_method: {
            id:'',
            name: '',
            price:''
        },
        delivery_payment_method: {
            id:'',
            name:''
        },
        status_name:'',
        dishes: [],
        sum:'',

    };


}();
