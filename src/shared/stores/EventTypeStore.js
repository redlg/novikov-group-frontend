import { observable, computed, lodash } from '!/app';

export default new class EventTypeStore {
    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        query: '',
        email: ''
    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        first_name: '',
        last_name: '',
        birthday_at: null,
        code: '',
        email: '',
        phone: '',
        sex: 0
    };

    newItem = null;

    constructor() {
        this.newItem = Object.assign({}, this.item);
    }
}();
