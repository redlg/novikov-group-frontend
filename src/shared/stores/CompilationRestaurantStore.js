import { observable, computed, lodash } from '!/app';

export default new class CompilationRestaurantStore {
    @observable
    filter = {
        cur_page: 1,
        per_page: 25,
        sort_by: 'id',
        sort_type: 'asc',
        name: '',
        tag_id: '',

    };

    @observable
    pagination = {
        from: 1,
        last_page: 2,
        to: 2,
        total: 2,
        cur_page: 1
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        name: '',
        tag_ids: []

    };

    newItem = null;

    constructor() {
        this.newItem = Object.assign({}, this.item);
    }
}();
