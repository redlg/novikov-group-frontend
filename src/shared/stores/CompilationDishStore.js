import { observable, computed, lodash } from '!/app';

export default new class CompilationDishStore {
    @observable
    filter = {
        cur_page: 1,
        per_page: 25,
        sort_by: 'id',
        sort_type: 'asc',
        name: '',
        tag_id: '',
        restaurant_id:''

    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        name: '',
        tag_ids: [],
        restaurant_id:''


    };

    newItem = null;

    constructor() {
        this.newItem = Object.assign({}, this.item);
    }
}();
