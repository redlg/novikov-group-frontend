import { observable, computed, lodash } from '!/app';

export default new class GuestStore {
    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        query: '',
        email: '',
        birthday_at: null,
        birthday_at_from: null,
        birthday_at_to: null,
        last_operation_from: null,
        last_operation_to: null,
        all_operation: ['', ''],
        sum: ['', ''],
        average_check: ['', ''],
        transactions_count: ['', ''],
        total_check: ['', ''],
    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();


    @observable
    item = {
        id: '',
        first_name: '',
        last_name: '',
        birthday_at: null,
        code: '',
        email: '',
        phone: '',
        sex: 0,
        last_operation_date: '',
        sum:'',
        average_check_sum: '',
        discount_cards: [],
        certificates:[],
        total_check_sum:'',
        transactions_count: ''

    };

    newItem = null;

    constructor() {
        this.newItem = Object.assign({}, this.item);
    }
}();
