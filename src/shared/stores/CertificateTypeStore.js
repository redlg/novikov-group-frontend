import { observable, computed, lodash } from '!/app';

export default new class CertificateTypeStore {
    @observable items = Array();

    @computed
    get selectItems() {
        return this.items.map((item) => {
            const reusable = item.reusable ? '(Многоразовый)' : '(Одноразовый)';
            return {
                id: item.id,
                name: item.name + ' ' + reusable
            };
        });
    }

    @computed
    get itemById() {
        return lodash.keyBy(this.items, 'id');
    }
}();
