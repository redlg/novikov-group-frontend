import { observable, computed } from '!/app';

export default new class RestaurantMenuStore {
    @observable search = '';

    @observable _categories = Array();
    @computed
    get categories() {
        return this._categories.filter((category) => category.menu_category_id === null);
    }

    @computed
    get selectCategories() {
        return this._categories.map((category) => {
            return {
                id: category.id,
                name: category.name_ru
            };
        });
    }

    @observable dishes = Array();
}();
