import { observable, computed } from '!/app';

export default new class DeliveryMenuStore {
    @observable dishes = Array();

    @observable categories = Array();

    @observable search = '';

    @computed
    get selectCategories() {
        return this.categories.map((category) => {
            return {
                id: category.id,
                name: category.name_ru
            };
        });
    }
}();
