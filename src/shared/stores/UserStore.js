import { observable } from '!/app';

import Cache from '!/utils/Cache';

export default new class UserStore {
    @observable token = false; //false;
    @observable
    permissions = [
        // "RESTAURANTS",
        // "GUESTS",
        // "PARTNERS",
        // "EVENTS",
        // "CARDS",
        // "OPERATIONS",
        // "RESERVATIONS",
        // "ORDERS"
    ]; // Array();

    @observable role = 'administrator';

    cache = null;

    constructor() {
        this.cache = new Cache('UserStore');
        this.token = this.cache.get('token');
    }
}();
