import { observable, computed, lodash } from '!/app';

export default new class TransactionStore {
    @observable
    filters = {
        card_input_methods: [],
        payment_methods: [],
        transaction_types: []
    };

    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        number: '',
        card_type: '',
        operation_type_id: '',
        created_at_from: null,
        created_at_to: null,
        name:'',
        card_input_method: {
            id:'',
            name: ''
        },
        cashbox_number:'',
        operation_id:'',
        check_number:'',
        sum: ['', ''],
        discount_sum: ['', ''],
        payment_method: {
            id: '',
            name_ru:''
        },
        payment_method_id: 0,
        transaction_type: '',
        relation:'',
    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        card_type: '',
        operation_type_id: '',
        payment_method: {
            id: '',
            name_ru:''
        },
        sum:'',
        discount_sum:'',
        check_number:'',
        cashbox_number:'',
        type: {
            name: ''
        },
        card: {
            number: ''
        },
        operation_id: ''
    };
}();
