import { observable, computed, lodash } from '!/app';

export default new class CertificateStore {
    @observable
    statuses = [
        {
            id: 'not_activated',
            name: 'Не активирована'
        },
        {
            id: 'activated_in_office',
            name: 'Активирована в офисе'
        },
        {
            id: 'activated_in_restaurant',
            name: 'Активирована в ресторане'
        },
        {
            id: 'blocked_manually',
            name: 'Заблокирована вручную'
        },
        {
            id: 'blocked_expired',
            name: 'Истекла'
        },
        {
            id: 'blocked_service',
            name: 'Использован'
        }
    ];

    @computed
    get statusById() {
        return lodash.keyBy(this.statuses, 'id');
    }

    @observable
    filters = {
        cities: []
    };

    @observable
    filter = {
        cur_page: 1,
        per_page: 15,
        sort_by: 'id',
        sort_type: 'asc',
        query: '',
        status: 0,
        type: 0,
        restaurant_id: '',
        issuer_id: '',
        contractor_id: '',
        created_at_from: null,
        created_at_to: null,
        ended_at_from: null,
        ended_at_to: null,
        started_at_from: null,
        started_at_to: null,
        blocked_at_from: null,
        blocked_at_to: null
    };

    @observable
    pagination = {
        from: 1,
        last_page: 1,
        to: 1,
        total: 1,
        cur_page: 1
    };

    @observable items = Array();

    @observable
    item = {
        id: '',
        balance: '',
        number: '',
        card_type_id: '',
        status: '',
        status_name: '',
        started_at: '',
        ended_at: '',
        blocked_at: '',
        type: {
            name: '',
            sum: '',
            reusable: ''
        }
    };
}();
