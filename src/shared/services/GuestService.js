import { action, history, lodash } from '!/app';
import { GuestStore } from '!/stores';
import { Api } from '!/utils';

export default new class GuestService {
    @action
    getItems = (options, clear = false) => {
        let data = {};
        if (!clear) {
            data = Object.assign({}, GuestStore.filter);
            data['average_check_from'] = data.average_check[0];
            data['average_check_to'] = data.average_check[1];
            data['transactions_count_from'] = data.transactions_count[0];
            data['transactions_count_to'] = data.transactions_count[1];
            data['total_check_from'] = data.total_check[0];
            data['total_check_to'] = data.total_check[1];
        }
        lodash.merge(data, options);
        return Api.get('/guest', data).then((response) => {
            GuestStore.items = response.body.items;
            GuestStore.pagination = response.body.pagination;
        });
    };

    @action
    get = (id) => {
        return Api.get('/guest/' + id).then((response) => {
            lodash.merge(GuestStore.item, response.body);
        });
    };

    @action
    create = () => {
        return Api.post('/guest/', GuestStore.item);
    };

    @action
    update = () => {
        return Api.put('/guest/' + GuestStore.item.id, GuestStore.item);
    };
}();
