import { action, history, lodash } from '!/app';
import { CompilationDishStore } from '!/stores';
import { Api } from '!/utils';

export default new class CompilationDishService {

    @action
    getItems = () => {
        return Api.get('/compilation/dishes', CompilationDishStore.filter  ).then((response) => {
            CompilationDishStore.items = response.body.items;
            CompilationDishStore.pagination = response.body.pagination;
        });
    };

    @action
    get = (id) => {
        return Api.get('/compilation/dishes/' + id).then((response) => {
            lodash.merge(CompilationDishStore.item, response.body);
            CompilationDishStore.item.tag_ids = response.body.tags.map(item => item.id);

        });
    };

    @action
    create = ( ) => {
        return Api.post('/compilation/dishes/', CompilationDishStore.item);
    };

    @action
    update = () => {
        return Api.put('/compilation/dishes/' + CompilationDishStore.item.id,CompilationDishStore.item);
    };
}();
