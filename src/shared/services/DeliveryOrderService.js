import { action, history, lodash } from '!/app';
import { DeliveryOrderStore } from '!/stores';
import { Api } from '!/utils';

export default new class DeliveryOrderService {
    @action
    getItems = (options, clear = false) => {
        let data = {};
        if (!clear) {
            data = Object.assign({}, DeliveryOrderStore.filter);
            data['sum_from'] = data.sum[0];
            data['sum_to'] = data.sum[1];

        }
        lodash.merge(data, options);
        return Api.get('/delivery-order', data).then((response) => {
            DeliveryOrderStore.items = response.body.items;
            DeliveryOrderStore.pagination = response.body.pagination;
        });
    };

    @action
    getFilters = () => {
        return Api.get('/delivery-order/filters').then((response) => {
            DeliveryOrderStore.filters.delivery_payment_methods = response.body.delivery_payment_methods;
            DeliveryOrderStore.filters.delivery_methods = response.body.delivery_methods;
            DeliveryOrderStore.filters.statuses = response.body.statuses;
        });
    };

    @action
    get = (id) => {
        return Api.get('/delivery-order/' + id).then((response) => {
            lodash.merge(DeliveryOrderStore.item, response.body);
        });
    };

    @action
    create = () => {
        return Api.post('/delivery-order/', DeliveryOrderStore.item);
    };

    @action
    update = () => {
        return Api.put('/delivery-order/' + DeliveryOrderStore.item.id, DeliveryOrderStore.item);
    };
}();
