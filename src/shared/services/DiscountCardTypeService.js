import { action, history } from '!/app';
import { DiscountCardTypeStore } from '!/stores';
import { Api } from '!/utils';

export default new class DiscountCardTypeService {
    @action
    getItems = () => {
        return Api.get('/discount-card-type').then((response) => {
            const items = Array();
            response.body.forEach((item) => {
                items.push(item);
            });
            DiscountCardTypeStore.items = items;
        });
    };
}();
