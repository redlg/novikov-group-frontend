import { action, history, lodash } from '!/app';
import { OperationStore } from '!/stores';
import { Api } from '!/utils';

export default new class OperationService {
    @action
    getFilters = () => {
        return Api.get('/operation/filters').then((response) => {
            OperationStore.filters.initiators = response.body.initiators;
            OperationStore.filters.relations = response.body.relations;
        });
    };

    @action
    getItems = (options, clear = false) => {
        let data = {};
        if (!clear) {
            data = Object.assign({}, OperationStore.filter);
            data['sum_from'] = data.sum[0];
            data['sum_to'] = data.sum[1];
        }
        lodash.merge(data, options);
        return Api.get('/operation',data ).then((response) => {
            OperationStore.items = response.body.items;
            OperationStore.pagination = response.body.pagination;
        });
    };


    @action
    get = (id) => {
        return Api.get('/operation/' + id).then((response) => {
            lodash.merge(OperationStore.item, response.body);
        });
    };

    @action
    create = () => {
        return Api.post('/operation/', OperationStore.item);
    };

    @action
    update = () => {
        return Api.put('/operation/' + OperationStore.item.id, OperationStore.item);
    };
}();
