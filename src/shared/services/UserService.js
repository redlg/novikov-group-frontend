import { action, history } from '!/app';
import { UserStore } from '!/stores';
import { Api, Alert } from '!/utils';

export default new class UserService {
    @action
    login = (data) => {
        Api.post('/auth/login', data).then((response) => {
            if (response.body.error) {
                Alert.error(response.body.error);
            } else {
                UserStore.token = response.body.token;
                UserStore.cache.set('token', UserStore.token);
                history.push('/');
            }
        });
    };

    @action
    logout = () => {
        UserStore.token = false;
        UserStore.cache.remove('token');
        history.push('/login');
    };
}();
