import { action, history, lodash } from '!/app';
import { EventTypeStore } from '!/stores';
import { Api } from '!/utils';

export default new class EventTypeService {
    @action
    getItems = () => {
        return Api.get('/event-type', EventTypeStore.filter).then((response) => {
            EventTypeStore.items = response.body;
            // EventTypeStore.pagination = response.body.pagination;
        });
    };

    @action
    get = (id) => {
        return Api.get('/event-type/' + id).then((response) => {
            lodash.merge(EventTypeStore.item, response.body);
        });
    };

    @action
    create = () => {
        return Api.post('/event-type/', EventTypeStore.item);
    };

    @action
    update = () => {
        return Api.put('/event-type/' + EventTypeStore.item.id, EventTypeStore.item);
    };
}();
