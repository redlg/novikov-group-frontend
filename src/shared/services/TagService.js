import { action, history, lodash } from '!/app';
import { TagStore } from '!/stores';
import { Api } from '!/utils';

export default new class TagService {
    @action
    getItems = () => {
        const data = Object.assign({}, TagStore.filter);
        return Api.get('/tag',data).then((response) => {
            const items = Array();
            response.body.forEach((item) => {
                items.push(item);
            });
            TagStore.items = items;
        });
    };

    @action
    get = (id) => {
        return Api.get('/tag/' + id).then((response) => {
            lodash.merge(TagStore.item, response.body);
        });
    };

    @action
    create = () => {
        return Api.post('/tag/', TagStore.item);
    };

    @action
    update = () => {
        return Api.put('/tag/' + TagStore.item.id, TagStore.item);
    };
}();
