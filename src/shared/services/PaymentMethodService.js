import { action, history, lodash } from '!/app';
import {PaymentMethodStore } from '!/stores';
import { Api } from '!/utils';

export default new class PaymentMethodService {
    @action
    getItems = () => {
        return Api.get('/operation/filters',PaymentMethodService.filter).then((response) => {
            PaymentMethodService.items = response.body;
        });
    };

    @action
    get = (id) => {
        return Api.get('/operation/filters/' + id).then((response) => {
            lodash.merge(PaymentMethodService.item, response.body);
        });
    };

    @action
    getSelectItems = () => {
        Api.get('/operation/filters', { }).then((response) => {
            PaymentMethodStore.selectItems = response.body.payment_methods.map((payment_methods) => {
                return {
                    id: payment_methods.id,
                    name: payment_methods.name_ru
                };
            });
        });
    };

    @action
    create = () => {
        return Api.post('/operation/filters/', PaymentMethodService.item);
    };

    @action
    update = () => {
        return Api.put('/operation/filters/' + PaymentMethodService.item.id,PaymentMethodService.item);
    };
}();
