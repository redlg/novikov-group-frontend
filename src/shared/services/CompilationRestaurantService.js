import { action, history, lodash } from '!/app';
import { CompilationRestaurantStore } from '!/stores';
import { Api } from '!/utils';

export default new class CompilationRestaurantService {

    @action
    getItems = () => {
        return Api.get('/compilation/restaurants', CompilationRestaurantStore.filter  ).then((response) => {
            CompilationRestaurantStore.items = response.body.items;
            CompilationRestaurantStore.pagination = response.body.pagination;

        });
    };

    @action
    get = (id) => {
        return Api.get('/compilation/restaurants/' + id).then((response) => {
            lodash.merge(CompilationRestaurantStore.item, response.body);
            CompilationRestaurantStore.item.tag_ids = response.body.tags.map(item => item.id);
        });
    };

    @action
    create = ( ) => {
        return Api.post('/compilation/restaurants/', CompilationRestaurantStore.item);
    };

    @action
    update = () => {
        return Api.put('/compilation/restaurants/' + CompilationRestaurantStore.item.id, CompilationRestaurantStore.item);
    };
}();
