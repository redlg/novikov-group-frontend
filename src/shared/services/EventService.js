import { action, history, lodash } from '!/app';
import { EventStore } from '!/stores';
import { Api } from '!/utils';

export default new class EventService {
    @action
    getItems = () => {
        return Api.get('/event', EventStore.filter).then((response) => {
            EventStore.items = response.body.items;
            EventStore.pagination = response.body.pagination;
        });
    };

    @action
    get = (id) => {
        return Api.get('/event/' + id).then((response) => {
            lodash.merge(EventStore.item, response.body);
        });
    };

    @action
    create = () => {
        return Api.post('/event/', EventStore.item);
    };

    @action
    update = () => {
        return Api.put('/event/' + EventStore.item.id, EventStore.item);
    };
}();
