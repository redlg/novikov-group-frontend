import { action, history, lodash } from '!/app';
import { RestaurantChefStore } from '!/stores';
import { Api } from '!/utils';

export default new class  RestaurantChefService {

    @action
    getItems = () => {
        return Api.get('/chef').then((response) => {
            RestaurantChefStore.items = response.body.items;

        });
    };

    @action
    get = (id) => {
        return Api.get('/chef/' + id).then((response) => {
            lodash.merge(RestaurantChefStore.item, response.body);
        });
    };

    @action
    saveChef = (data) => {
        return Api.put('/chef/' + data.id, data);
    };

}
