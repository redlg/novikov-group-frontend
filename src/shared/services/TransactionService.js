import { action, history, lodash } from '!/app';
import { TransactionStore } from '!/stores';
import { Api } from '!/utils';

export default new class TransactionService {
    @action
    getItems = (options, clear = false) => {
        let data = {};
        if (!clear) {
            data = Object.assign({}, TransactionStore.filter);
            data['sum_from'] = data.sum[0];
            data['sum_to'] = data.sum[1];
            data['discount_sum_from'] = data.discount_sum[0];
            data['discount_sum_to'] = data.discount_sum[1];
        }
        lodash.merge(data, options);

        return Api.get('/transaction',data).then((response) => {
            TransactionStore.items = response.body.items;
            TransactionStore.pagination = response.body.pagination;
        });
    };

    @action
    getFilters = () => {
        return Api.get('/transaction/filters').then((response) => {
            TransactionStore.filters.card_input_methods = response.body.card_input_methods;
            TransactionStore.filters.payment_methods = response.body.payment_methods;
            TransactionStore.filters.transaction_types = response.body.transaction_types;
        });
    };

    @action
    get = (id) => {
        return Api.get('/transaction/' + id).then((response) => {
            lodash.merge(TransactionStore.item, response.body);
        });
    };



    @action
    create = () => {
        return Api.post('/transaction/', TransactionStore.item);
    };

    @action
    update = () => {
        return Api.put('/transaction/' + TransactionStore.item.id, TransactionStore.item);
    };
}();
