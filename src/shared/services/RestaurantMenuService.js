import { action, history } from '!/app';
import { RestaurantMenuStore } from '!/stores';
import { Api } from '!/utils';
import { RestaurantMenuCategory, RestaurantMenuDish } from '!/models';

export default new class RestaurantMenuService {
    @action
    getCategories = (restaurant_id) => {
        return Api.get('/menu-category/', { restaurant_id }).then((response) => {
            const categories = Array();
            response.body.map((item) => {
                categories.push(new RestaurantMenuCategory(item));
            });
            RestaurantMenuStore._categories = categories;
        });
    };

    @action
    getCategory = (id) => {
        return Api.get('/menu-category/' + id);
    };

    @action
    saveCategory = (category) => {
        return Api.put('/menu-category/' + category.id, category);
    };

    @action
    createCategory = (id) => {
        return Api.post('/menu-category', category);
    };

    @action
    getDishes = (restaurant_id) => {
        return Api.get('/menu-dish/', { restaurant_id }).then((response) => {
            const dishes = Array();
            response.body.map((item) => {
                dishes.push(new RestaurantMenuDish(item));
            });
            RestaurantMenuStore.dishes = dishes;
        });
    };

    @action
    getDish = (id) => {
        return Api.get('/menu-dish/' + id);
    };

    @action
    saveDish = (dish) => {
        return Api.put('/menu-dish/' + dish.id, dish);
    };
}();
