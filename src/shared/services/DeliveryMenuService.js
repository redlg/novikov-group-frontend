import { action, history } from '!/app';
import { DeliveryMenuStore } from '!/stores';
import { Api } from '!/utils';
import { DeliveryMenuCategory, DeliveryMenuDish } from '!/models';

export default new class DeliveryMenuService {
    @action
    getCategories = (restaurant_id) => {
        return Api.get('/delivery-order-category/', { restaurant_id }).then((response) => {
            const categories = Array();
            response.body.map((item) => {
                categories.push(new DeliveryMenuCategory(item));
            });
            DeliveryMenuStore.categories = categories;
        });
    };

    @action
    getDishes = (restaurant_id) => {
        return Api.get('/delivery-order-dish/', { restaurant_id }).then((response) => {
            const dishes = Array();
            response.body.map((item) => {
                dishes.push(new DeliveryMenuDish(item));
            });
            DeliveryMenuStore.dishes = dishes;
        });
    };

    @action
    getCategory = (id) => {
        return Api.get('/delivery-order-category/' + id);
    };

    @action
    saveCategory = (category) => {
        return Api.put('/delivery-order-category/' + category.id, category);
    };

    @action
    createCategory = (id) => {
        return Api.post('/delivery-order-category', category);
    };

    @action
    getDish = (id) => {
        return Api.get('/delivery-order-dish/' + id);
    };

    @action
    saveDish = (dish) => {
        return Api.put('/delivery-order-dish/' + dish.id, dish);
    };
}();
