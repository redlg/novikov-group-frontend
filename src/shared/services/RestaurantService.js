import { action, history, lodash } from '!/app';
import { RestaurantStore } from '!/stores';
import { Api } from '!/utils';

export default new class RestaurantService {
    @action
    getFilters = () => {
        Api.get('/restaurant/filters').then((response) => {
            RestaurantStore.filters.cities = response.body.cities;
        });
    };

    @action
    getItems = (options, clear = false) => {
        let data = {};
        if (!clear) {
            data = Object.assign({}, RestaurantStore.filter);
            data['seats_from'] = data.seats[0];
            data['seats_to'] = data.seats[1];
        }
        lodash.merge(data, options);

        return Api.get('/restaurant', data).then((response) => {
            RestaurantStore.items = response.body.items;
            RestaurantStore.pagination = response.body.pagination;
        });
    };

    @action
    getSelectItems = () => {
        Api.get('/restaurant/values').then((response) => {
            RestaurantStore.selectItems = response.body;
        });
    };

    @action
    getRestaurant = (id) => {
        return Api.get('/restaurant/' + id);
    };

    @action
    saveRestaurant = (data) => {
        return Api.put('/restaurant/' + data.id, data);
    };

    @action
    getChef = (id) => {
        return Api.get('/chef/' + id);
    };

    @action
    saveChef = (data) => {
        return Api.put('/chef/' + data.id, data);
    };

    @action
    getAddress = (id) => {
        return Api.get('/address/' + id);
    };

    @action
    getAddressTypes = (id) => {
        return Api.get('/address/filters');
    };

    @action
    saveAddress = (data) => {
        return Api.put('/address/' + data.id, data);
    };

    @action
    getLegalEntity = (id) => {
        return Api.get('/legal-entity/' + id);
    };

    @action
    getLegalEntityTypes = (id) => {
        return Api.get('/legal-entity/filters');
    };

    @action
    saveLegalEntity = (data) => {
        return Api.put('/legal-entity/' + data.id, data);
    };
}();
