import { action, history, lodash } from '!/app';
import { KitchenStore } from '!/stores';
import { Api } from '!/utils';

export default new class KitchenService {
    @action
    getItems = () => {
        const data = Object.assign({}, KitchenStore.filter);
        return Api.get('/kitchen',data).then((response) => {
            const items = Array();
            response.body.forEach((item) => {
                items.push(item);
            });
            KitchenStore.items = items;
            KitchenStore.pagination = items;
        });
    };

    @action
    get = (id) => {
        return Api.get('/kitchen/' + id).then((response) => {
            lodash.merge(KitchenStore.item, response.body);
        });
    };

    @action
    create = () => {
        return Api.post('/kitchen/', KitchenStore.item);
    };

    @action
    update = () => {
        return Api.put('/kitchen/' + KitchenStore.item.id, KitchenStore.item);
    };
}();
