import { action, history, lodash } from '!/app';
import { DiscountCardStore } from '!/stores';
import { Api } from '!/utils';

export default new class DiscountCardService {
    @action
    getFilters = () => {
        Api.get('/restaurant/filters').then((response) => {
            RestaurantStore.filters.cities = response.body.cities;
        });
    };

    @action
    getItems = () => {
        const data = Object.assign({}, DiscountCardStore.filter);

        return Api.get('/discount-card', data).then((response) => {
            DiscountCardStore.items = response.body.items;
            DiscountCardStore.pagination = response.body.pagination;
        });
    };

    @action
    getItem = (id) => {
        return Api.get('/discount-card/' + id).then((response) => {
            lodash.merge(DiscountCardStore.item, response.body);
        });
    };

    @action
    export = () => {
        const data = Object.assign({}, DiscountCardStore.filter);

        return Api.get('/discount-card/export', data).then((response) => {});
    };

    @action
    updateItem = () => {
        return Api.put('/discount-card/' + DiscountCardStore.item.id, DiscountCardStore.item);
    };

    @action
    unblock = (id, message) => {
        return Api.post('/discount-card/' + id + '/unblock', { message }).then((response) => {
            lodash.merge(DiscountCardStore.item, response.body);
        });
    };

    @action
    activate = (id, message) => {
        return Api.post('/discount-card/' + id + '/activate', { message }).then((response) => {
            lodash.merge(DiscountCardStore.item, response.body);
        });
    };

    @action
    block = (id, message) => {
        return Api.post('/discount-card/' + id + '/block', { message }).then((response) => {
            lodash.merge(DiscountCardStore.item, response.body);
        });
    };
}();
