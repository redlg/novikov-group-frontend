import { action, history } from '!/app';
import { CertificateTypeStore } from '!/stores';
import { Api } from '!/utils';

export default new class CertificateTypeService {
    @action
    getItems = () => {
        return Api.get('/certificate-type').then((response) => {
            const items = Array();
            response.body.forEach((item) => {
                items.push(item);
            });
            CertificateTypeStore.items = items;
        });
    };
}();
