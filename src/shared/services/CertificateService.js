import { action, history, lodash } from '!/app';
import { CertificateStore } from '!/stores';
import { Api } from '!/utils';

export default new class CertificateService {
    @action
    getFilters = () => {
        Api.get('/restaurant/filters').then((response) => {
            RestaurantStore.filters.cities = response.body.cities;
        });
    };

    @action
    getItems = () => {
        const data = Object.assign({}, CertificateStore.filter);

        Api.get('/certificate', data).then((response) => {
            CertificateStore.items = response.body.items;
            CertificateStore.pagination = response.body.pagination;
        });
    };

    @action
    getItem = (id) => {
        return Api.get('/certificate/' + id).then((response) => {
            lodash.merge(CertificateStore.item, response.body);
        });
    };

    @action
    updateItem = () => {
        return Api.put('/certificate/' + CertificateStore.item.id, CertificateStore.item);
    };

    @action
    export = () => {
        const data = Object.assign({}, CertificateStore.filter);

        Api.get('/certificate/export', data).then((response) => {});
    };

    @action
    unblock = (id, message) => {
        return Api.post('/certificate/' + id + '/unblock', { message }).then((response) => {
            lodash.merge(CertificateStore.item, response.body);
        });
    };

    @action
    activate = (id, message) => {
        return Api.post('/certificate/' + id + '/activate', { message }).then((response) => {
            lodash.merge(CertificateStore.item, response.body);
        });
    };

    @action
    block = (id, message) => {
        return Api.post('/certificate/' + id + '/block', { message }).then((response) => {
            lodash.merge(CertificateStore.item, response.body);
        });
    };
}();
