import { action, history, lodash } from '!/app';
import { OperationTypeStore } from '!/stores';
import { Api } from '!/utils';

export default new class OperationTypeService {
    @action
    getItems = () => {
        Api.get('/operation/filters').then((response) => {
            OperationTypeStore.items.operation_types = response.body.operation_types;
        });
    };
}