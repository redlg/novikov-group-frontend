import { observable, action, computed, lodash } from '!/app';

import { RestaurantMenuStore } from '!/stores';

export default class RestaurantMenuCategory {
    id = '';
    @observable name_ru = '';
    @observable name_en = '';
    @observable menu_category_id = '';
    @observable expanded = false;

    constructor(data = {}) {
        lodash.merge(this, data);
    }

    @computed
    get categories() {
        return RestaurantMenuStore._categories.filter((category) => category.menu_category_id === this.id);
    }

    @computed
    get dishes() {
        let items = RestaurantMenuStore.dishes;
        if (RestaurantMenuStore.search) {
            items = RestaurantMenuStore.dishes.filter((item) => {
                return item.name_ru.toLowerCase().indexOf(RestaurantMenuStore.search.toLowerCase()) > -1;
            });
        }
        items = items.filter((dish) => dish.menu_category_id === this.id);
        return items;
    }

    @computed
    get dishes_count() {
        let count = this.dishes.length;
        this.categories.forEach((category) => {
            count += category.dishes.length;
        });
        return count;
    }
}
