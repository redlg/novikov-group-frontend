import { observable, action, computed, lodash } from '!/app';

import { DeliveryMenuStore } from '!/stores';

export default class DeliveryMenuCategory {
    id = '';
    @observable expanded = true;
    @observable name_ru = '';
    @observable name_en = '';
    @observable expanded = false;

    constructor(data = {}) {
        lodash.merge(this, data);
    }

    @action
    toggle() {
        this.expanded = !this.expanded;
    }

    @computed
    get dishes() {
        let items = DeliveryMenuStore.dishes;
        if (DeliveryMenuStore.search) {
            items = DeliveryMenuStore.dishes.filter((item) => {
                return item.name_ru.toLowerCase().indexOf(DeliveryMenuStore.search.toLowerCase()) > -1;
            });
        }
        items = items.filter((dish) => dish.delivery_category_id === this.id);
        return items;
    }

    @computed
    get dishes_count() {
        return this.dishes.length;
    }
}
