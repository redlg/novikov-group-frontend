import { observable, action, computed, lodash } from '!/app';

export default class RestaurantMenuDish {
    id = '';
    @observable name_ru = '';
    @observable name_en = '';
    @observable menu_category_id = 0;

    constructor(data = {}) {
        lodash.merge(this, data);
    }
}
