export default new class Alert {
    _alert = '';

    init = (object) => {
        this._alert = object;
    };

    success = (message) => {
        this._alert.show(message, {
            time: 3000,
            type: 'success'
        });
    };

    error = (message) => {
        this._alert.show(message, {
            time: 3000,
            type: 'error'
        });
    };
}();
