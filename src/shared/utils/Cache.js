export default class Cache {
    prefix = '';

    constructor(prefix = 'App') {
        this.prefix = prefix + '.';
    }

    create = (prefix) => {
        return new this(prefix);
    };

    get = (id, defaultValue = false) => {
        let data = localStorage.getItem(this.prefix + id);
        if (data === null) {
            if (typeof defaultValue === 'function') {
                return defaultValue();
            }
            return defaultValue;
        } else {
            let timestamp = Math.floor(Date.now() / 1000);
            data = JSON.parse(data);

            //if cache is valid
            if (timestamp < data.expire) {
                return data.value;
            }

            this.delete(id);

            if (typeof defaultValue === 'function') {
                return defaultValue();
            }

            return defaultValue;
        }
    };

    delete = (id) => {
        localStorage.removeItem(this.prefix + id);
    };

    set = (id, value, time = 99999999) => {
        let timestamp = Math.floor(Date.now() / 1000);
        let data = {
            expire: timestamp + time,
            timestamp: timestamp,
            value: value
        };
        localStorage.setItem(this.prefix + id, JSON.stringify(data));
    };

    remove = (id) => {
        this.delete(id);
    };

    save = (id, value, time = 99999999) => {
        this.set(id, value, time);
    };
}
